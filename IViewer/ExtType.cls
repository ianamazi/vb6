VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ExtType"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Member0" ,"ExtType"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable(s) to hold property value(s)
Private mvarExtName As String 'local copy
Private mvarExtType As Integer 'local copy
Private mvarExtensions As String 'local copy
Public Property Let Extensions(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.Extensions = 5
    mvarExtensions = vData
End Property


Public Property Get Extensions() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.Extensions
    Extensions = mvarExtensions
End Property



Public Property Let ExtType(ByVal vData As Integer)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ExtType = 5
    mvarExtType = vData
End Property


Public Property Get ExtType() As Integer
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ExtType
    ExtType = mvarExtType
End Property



Public Property Let ExtName(ByVal vData As String)
'used when assigning a value to the property, on the left side of an assignment.
'Syntax: X.ExtName = 5
    mvarExtName = vData
End Property


Public Property Get ExtName() As String
'used when retrieving value of a property, on the right side of an assignment.
'Syntax: Debug.Print X.ExtName
    ExtName = mvarExtName
End Property



