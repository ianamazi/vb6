VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmTLib 
   Caption         =   "Lyrics & Time Library"
   ClientHeight    =   7770
   ClientLeft      =   60
   ClientTop       =   630
   ClientWidth     =   7890
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   7770
   ScaleWidth      =   7890
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList icoSmall 
      Left            =   5280
      Top             =   1200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   16777215
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTLib.frx":0000
            Key             =   "IVYnotfile"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTLib.frx":03B4
            Key             =   "IVYnofile"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTLib.frx":075C
            Key             =   "IVYnofol"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTLib.frx":0B15
            Key             =   "IVYfol"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTLib.frx":0EC4
            Key             =   "icoLyrics"
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imState 
      Left            =   4440
      Top             =   1200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   2
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTLib.frx":1333
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmTLib.frx":148D
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.ComboBox cmbPoss 
      Height          =   360
      Left            =   3240
      Sorted          =   -1  'True
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   360
      Visible         =   0   'False
      Width           =   4215
   End
   Begin MSComctlLib.StatusBar stbr 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   0
      Top             =   7395
      Width           =   7890
      _ExtentX        =   13917
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   2
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   1
            Object.Width           =   10848
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   2
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ListView lvw 
      Height          =   7335
      Left            =   30
      TabIndex        =   2
      Top             =   30
      Width           =   7815
      _ExtentX        =   13785
      _ExtentY        =   12938
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      OLEDropMode     =   1
      FullRowSelect   =   -1  'True
      _Version        =   393217
      SmallIcons      =   "imTvw"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Appearance      =   1
      OLEDropMode     =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Object.Width           =   1853
      EndProperty
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileRefresh 
         Caption         =   "&Refresh Library"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuOptPreview 
         Caption         =   "Preview Lyrics"
         Shortcut        =   ^L
      End
      Begin VB.Menu mnuOptPreviewPlay 
         Caption         =   "Play while previewing"
         Shortcut        =   ^P
      End
      Begin VB.Menu sf0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileShow 
         Caption         =   "Show Missing Files"
         Index           =   1
         Shortcut        =   ^{F1}
      End
      Begin VB.Menu mnuFileShow 
         Caption         =   "Show Missing Lyrics"
         Index           =   2
      End
      Begin VB.Menu mnuFileShow 
         Caption         =   "Show Removable Files"
         Index           =   3
         Shortcut        =   ^{F2}
      End
      Begin VB.Menu mnuFileShow 
         Caption         =   "Show All"
         Index           =   4
         Shortcut        =   ^{F3}
      End
      Begin VB.Menu mnuFileShow 
         Caption         =   "Show From Main Window"
         Index           =   5
      End
      Begin VB.Menu sf 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileClose 
         Caption         =   "Clos&e"
         Shortcut        =   ^E
      End
   End
   Begin VB.Menu mnuActions 
      Caption         =   "&Actions"
      Begin VB.Menu mnuActLocate 
         Caption         =   "Locate"
         Begin VB.Menu mnuActLoc 
            Caption         =   "&Recent"
            Index           =   1
            Shortcut        =   ^Q
         End
         Begin VB.Menu mnuActLoc 
            Caption         =   "Locate F&ile..."
            Index           =   2
         End
         Begin VB.Menu mnuActLoc 
            Caption         =   "Locate &Folder..."
            Index           =   3
         End
         Begin VB.Menu mnuActLoc 
            Caption         =   "Locate &Lyrics..."
            Index           =   4
         End
      End
      Begin VB.Menu mnuActClear 
         Caption         =   "Clear Lyrics"
         Index           =   1
      End
      Begin VB.Menu mnuActClear 
         Caption         =   "Clear Time"
         Index           =   2
         Shortcut        =   {DEL}
      End
      Begin VB.Menu mnuActFol 
         Caption         =   "&Rename"
         Index           =   3
         Shortcut        =   {F2}
      End
      Begin VB.Menu mnuActClean 
         Caption         =   "&Clean Folder"
         Enabled         =   0   'False
         Shortcut        =   ^C
      End
   End
End
Attribute VB_Name = "frmTLib"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Dim str() As Variant
Dim strRem As String 'removable drives
'        If m = vbYes And InStr(1, strRem, fso.GetDriveName(tmp(0)), vbTextCompare) > 0 Then ts.WriteLine lins(i)

Dim iMissing As Integer, iRemovable As Integer, iTotal As Integer, iFols As Integer

Private Sub Form_Load()
iMissing = 0: iRemovable = 0: iFols = 0

Set lvw.SmallIcons = icoSmall
Call ListView_SetImageList(lvw.hWnd, imState.hImageList, LVSIL_STATE)

Dim drv As Drive
For Each drv In fso.Drives
  If drv.DriveType <> Fixed And drv.DriveType <> UnknownType Then strRem = strRem & drv.DriveLetter & ":"
Next

Dim ts As TextStream, lins() As String, pos As Single, i As Integer
  Set ts = fso.OpenTextFile(apPath & "Time Library.ini", ForReading)
  'ts.ReadLine '
  lins = Split(ts.ReadAll, vbCrLf)
  ts.Close
  iTotal = UBound(lins)
  ReDim Preserve str(iTotal, 2)
  For i = 1 To iTotal '1 so that [section] gets outta the way...
    pos = InStr(lins(i), "=")
    If pos > 0 Then str(i, 1) = Mid(lins(i), pos + 1) Else pos = Len(lins(i)) + 1
    str(i, 0) = Mid(lins(i), 1, pos - 1)
    If Dir(str(i, 0)) = "" Then
      str(i, 2) = 0
      iMissing = iMissing + 1
      AddItem i
    Else
      str(i, 2) = 1
    End If
    'iRemovable
  Next
stbr.Panels(1) = iMissing & " missing in " & iFols & " Folders (of " & iTotal & " files)"
stbr.Panels(1).AutoSize = sbrSpring
End Sub

Sub AddItem(i As Integer)
Dim itm As ListItem, pFol As String, icn As Integer
pFol = fso.GetParentFolderName(str(i, 0))
On Error Resume Next
Set itm = lvw.ListItems(LCase(pFol))
If itm Is Nothing Then 'addfil
  If Dir(pFol, vbDirectory) = "" Then icn = 3 Else icn = 4
  Set itm = lvw.ListItems.Add(, LCase(pFol), pFol, , icn)
  iFols = iFols + 1
  setIndent lvw.hWnd, itm.Index, 0, lvisExpanded
End If
itm.SubItems(1) = Val(itm.SubItems(1)) + 1 'increase file count

If itm.SmallIcon = 4 And Dir(str(i, 0)) = "" Then icn = 1 Else icn = 2
Set itm = lvw.ListItems.Add(itm.Index + 1, LCase(str(i, 0)), fso.GetFileName(str(i, 0)), , icn)
setIndent lvw.hWnd, itm.Index, 1, lvisNoButton
itm.SubItems(1) = str(i, 1)
itm.ListSubItems(1).ReportIcon = "icoLyrics"
itm.Tag = 1 '= str(i,2) > means missing?
'tvw.Nodes.Add nod, 4, LCase(str(i, 0)), fso.GetFileName(str(i, 0)), icn
'tvw.Nodes(tvw.Nodes.Count).Tag = 1
End Sub

'Sub AddItem(i As Integer)
'Dim nod As Node, pFol As String, icn As Integer
'pFol = fso.GetParentFolderName(str(i, 0))
'On Error Resume Next
'Set nod = tvw.Nodes(LCase(pFol))
'If nod Is Nothing Then 'addfil
'  If Dir(pFol, vbDirectory) = "" Then icn = 3 Else icn = 4
'  Set nod = tvw.Nodes.Add(, 4, LCase(pFol), pFol, icn)
'  iFols = iFols + 1
'End If
'nod.Tag = Val(nod.Tag) + 1
'If nod.Image = 4 And Dir(str(i, 0)) = "" Then icn = 1 Else icn = 2
'tvw.Nodes.Add nod, 4, LCase(str(i, 0)), fso.GetFileName(str(i, 0)), icn
'tvw.Nodes(tvw.Nodes.Count).Tag = 1
'End Sub


Private Sub tvw_DblClick()
'if its a file look for it
'first find the starting parent...
If tvw.SelectedItem.Image < 3 Then
  Dim pFol As String, filt As String
  pFol = fso.GetParentFolderName(tvw.SelectedItem.Key)
  Do Until Dir(pFol, vbDirectory) <> ""
    pFol = fso.GetParentFolderName(pFol)
  Loop
  filt = tvw.SelectedItem.Text & Chr(0) & tvw.SelectedItem.Text & Chr(0)
  filt = filt & "Current Extension (mp3?)" & Chr(0) & fso.GetExtensionName(tvw.SelectedItem.Text)
  filt = filt & Chr(0) & "All Files (*.*)" & Chr(0) & "*.*"
  fil = ShowFileOpenSave(Me, True, "Locate File", pFol, filt, 1)
End If
End Sub

'Private Sub tvw_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
'Dim tst As Node
'Set tst = tvw.HitTest(X, Y)
'If tst Is Nothing Then Exit Sub
'If tst.Parent Is Nothing Then tvw.ToolTipText = tst.Tag & " Files." Else tvw.ToolTipText = ""
'End Sub

Private Sub Form_Resize()
If Me.WindowState = vbMinimized Then Exit Sub

lvw.Width = Me.ScaleWidth - 60
lvw.Height = Me.ScaleHeight - stbr.Height - 60
lvw.ColumnHeaders(1).Width = lvw.Width - 1400
End Sub

Private Sub mnuOptPreview_Click()
If mnuOptPreview.Checked Then Unload frmLyrics Else frmLyrics.Show
mnuOptPreview.Checked = Not mnuOptPreview.Checked
End Sub
