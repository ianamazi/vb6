VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmJump 
   Caption         =   "Jump to file..."
   ClientHeight    =   3660
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmJump.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3660
   ScaleWidth      =   4680
   Begin VB.OptionButton optAddWhere 
      Caption         =   "&3 App"
      Height          =   255
      Index           =   3
      Left            =   2670
      TabIndex        =   6
      ToolTipText     =   "Appends selection in Playlist"
      Top             =   120
      Width           =   735
   End
   Begin VB.OptionButton optAddWhere 
      Caption         =   "&2 List"
      Height          =   255
      Index           =   2
      Left            =   1935
      TabIndex        =   5
      ToolTipText     =   "Plays Selection To Playlist"
      Top             =   120
      Width           =   735
   End
   Begin VB.OptionButton optAddWhere 
      Caption         =   "&1 Exp"
      Height          =   255
      Index           =   1
      Left            =   1200
      TabIndex        =   4
      ToolTipText     =   "Plays Selection to Explorer List"
      Top             =   120
      Width           =   735
   End
   Begin VB.CheckBox chkShowFromLibrary 
      Caption         =   "&All Files"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   120
      Value           =   1  'Checked
      Width           =   855
   End
   Begin VB.TextBox txtFilter 
      Height          =   285
      Left            =   113
      TabIndex        =   0
      Top             =   480
      Width           =   4455
   End
   Begin MSComctlLib.ListView lvwJump 
      Height          =   2655
      Left            =   120
      TabIndex        =   1
      Top             =   840
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   4683
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   10072
      EndProperty
   End
   Begin VB.Label cmdCleanTimeLibrary 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Clean &Library"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   3488
      TabIndex        =   3
      Top             =   120
      Width           =   1080
   End
End
Attribute VB_Name = "frmJump"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private lvwCrl As ListView

Public Function DoJump(lvw As ListView)
If lvw.Index = 3 Then chkShowFromLibrary.Value = False: chkShowFromLibrary.Enabled = False
lvwJump.ListItems.Clear
Dim itm As ListItem
For Each itm In lvw.ListItems
  lvwJump.ListItems.Add lvwJump.ListItems.Count + 1, itm.Key, itm.Text
Next
Set lvwCrl = lvw
End Function

Private Sub cmdCleanTimeLibrary_Click()
If fso.FileExists(apPath & "Time Library.ini") = False Then MsgBox "Time Library Missing", vbCritical
Dim lin() As String, fil As String, i As Integer

Dim ts As TextStream, ts2 As TextStream
Set ts = fso.OpenTextFile(apPath & "Time Library.ini", ForReading)
ts.ReadLine
ReDim lin(0)
Do Until ts.AtEndOfStream
  ReDim Preserve lin(UBound(lin) + 1)
  lin(UBound(lin)) = ts.ReadLine
Loop

Set ts = fso.CreateTextFile(apPath & "Time Library.ini", True)
ts.WriteLine ("[Time Library]")
If fso.FileExists(apPath & "Time Library Missing.ini") Then
  Set ts2 = fso.OpenTextFile(apPath & "Time Library Missing.ini", ForAppending)
Else
  Set ts2 = fso.CreateTextFile(apPath & "Time Library Missing.ini", True)
  ts2.WriteLine ("[Time Library - Missing Files]")
End If

Dim remvd As Long
For i = 1 To UBound(lin)
  fil = Mid(lin(i), 1, InStr(lin(i), "=") - 1)
  If fso.FileExists(fil) Then
    ts.WriteLine (lin(i))
  Else
    ts2.WriteLine (lin(i))
    remvd = remvd + 1
  End If
Next

ts.Close
ts2.Close
MsgBox "Time Library Cleaned" & vbCrLf & "(" & remvd & " missing files)", vbInformation
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()
Me.Width = getINI("Positions", "frmJump.Width", "4800")
Me.Left = getINI("Positions", "frmJump.Left", (Screen.Width - Me.Width) / 2)
Me.Height = getINI("Positions", "frmJump.Height", "3840")
Me.Top = getINI("Positions", "frmJump.Top", (Screen.Height - Me.Height) / 2)
optAddWhere(CInt(getINI("Preferences", "Default Action in Jump Dialog", 3))).Value = True
End Sub

Private Sub Form_Resize()
If Me.Width < 4800 Then Me.Width = 4800: Exit Sub
'cmdCancel.Left = Me.Width - cmdCancel.Width - 240
txtFilter.Width = Me.Width - 360
cmdCleanTimeLibrary.Left = Me.Width - cmdCleanTimeLibrary.Width - 240
lvwJump.Width = Me.Width - 360
lvwJump.Height = Me.ScaleHeight - lvwJump.Top - 120
End Sub

Private Sub Form_Unload(Cancel As Integer)
putINI "Positions", "frmJump.Width", Me.Width
putINI "Positions", "frmJump.Left", Me.Left
putINI "Positions", "frmJump.Height", Me.Height
putINI "Positions", "frmJump.Top", Me.Top
End Sub

Private Sub lvwJump_DblClick()
Dim iSelected As Integer
For iSelected = 1 To 4
  If optAddWhere(iSelected).Value = True Then Exit For
Next
If iSelected = 4 Then MsgBox "Select an option first.", vbCritical: Exit Sub

If iSelected = 1 Then
  Vdplayer.LoadFol (fso.GetParentFolderName(lvwJump.SelectedItem.Key))
  Set lvwCrl = Vdplayer.lvw(1)
  Vdplayer.mnuView_Click 1
Else
  Vdplayer.AddItem 2, lvwJump.SelectedItem.Key 'has code to check if already exists
  Set lvwCrl = Vdplayer.lvw(2)
  Vdplayer.mnuView_Click 2
End If

If iSelected <> 3 Then
  Vdplayer.CrlClick , lvwJump.SelectedItem.Key
  lvwCrl.MultiSelect = False
  lvwCrl.ListItems(lvwJump.SelectedItem.Key).Selected = True
  lvwCrl.ListItems(lvwJump.SelectedItem.Key).EnsureVisible
  lvwCrl.MultiSelect = True
End If
Unload Me

putINI "Preferences", "Default Action in Jump Dialog", CStr(iSelected)

lvwCrl.SetFocus
End Sub

Private Sub lvwJump_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyReturn Then lvwJump_DblClick
End Sub

Private Sub optAddWhere_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyDown Then txtFilter.SetFocus
End Sub

Private Sub txtFilter_Change()
lvwJump.ListItems.Clear

Dim str As String
str = LCase(txtFilter)

'Whole chkShowFromLibrary Section Added 28th Oct 06
If Len(str) < 2 Then lvwJump.ListItems.Clear: Exit Sub
Static lin() As String, bolLoaded As Boolean

If chkShowFromLibrary Then
  If bolLoaded = False Then
  'Load time library
    Dim ts As TextStream
    Set ts = fso.OpenTextFile(apPath & "Time Library.ini", ForReading)
    ts.ReadLine
    Dim S As String
    ReDim lin(0)
    Do Until ts.AtEndOfStream
      ReDim Preserve lin(UBound(lin) + 1)
      S = ts.ReadLine
      S = Mid(S, 1, InStr(S, "=") - 1)
      lin(UBound(lin)) = S
    Loop
    ts.Close
    bolLoaded = True
  End If
  Dim i As Integer
  For i = 1 To UBound(lin)
  If InStr(LCase(lin(i)), str) > 0 Then
    lvwJump.ListItems.Add lvwJump.ListItems.Count + 1, lin(i), fso.GetFileName(lin(i))
    lvwJump.ListItems(lvwJump.ListItems.Count).ToolTipText = fso.GetParentFolderName(lin(i))
  End If
  Next
Else 'Old Code
  Dim itm As ListItem
  For Each itm In lvwCrl.ListItems
    If InStr(LCase(itm.Text), str) > 0 Then
      lvwJump.ListItems.Add lvwJump.ListItems.Count + 1, itm.Key, itm.Text
    End If
  Next
End If
End Sub

Private Sub txtFilter_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyUp Or KeyCode = vbKeyDown Then lvwJump.SetFocus
End Sub

Private Sub txtFilter_KeyPress(KeyAscii As Integer)
If KeyAscii = vbKeyUp Then
  If lvwJump.SelectedItem.Index > 1 Then lvwJump.ListItems(lvwJump.SelectedItem.Index - 1).Selected = True
ElseIf KeyAscii = vbKeyDown Then
  If lvwJump.SelectedItem.Index < lvwJump.ListItems.Count Then lvwJump.ListItems(lvwJump.SelectedItem.Index + 1).Selected = True
End If
End Sub
