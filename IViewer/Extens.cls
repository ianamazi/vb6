VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Extens"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Collection" ,"ExtType"
Attribute VB_Ext_KEY = "Member0" ,"ExtType"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'local variable to hold collection
Private mCol As Collection

Public Function Sort() 'puts "None" at top & sorts the rest alphabetically
'swap (n-1)*(n-1) times??
End Function

Public Function QueryExtn(Extension As String) As Integer
If InStr(1, Me.Item("Html").Extensions, Extension, vbTextCompare) Then
  If Vdplayer.viShowHtmlAsText Then QueryExtn = 4 Else QueryExtn = 1
Else
  For i = 1 To Me.Count
    If InStr(1, Me.Item(i).Extensions, Extension, vbTextCompare) Then
      QueryExtn = Me.Item(i).ExtType
      Exit Function
    End If
  Next
  QueryExtn = -1
End If
End Function

Public Sub saveToINI()
cIni.Section = "Extensions"
cIni.DeleteSection

For i = 1 To Me.Count
  cIni.Key = Me.Item(i).ExtName & "(" & Me.Item(i).ExtType & ")"
  cIni.Value = Me.Item(i).Extensions
Next
End Sub

Public Sub loadDefaults()
Do Until Me.Count = 0
  Me.Remove 1
Loop

Me.Add "None", 0, ""
Me.Add "Audio", 3, "*.mp3;*.wav;*.mid;*.m3u;*.wma"
Me.Add "Html", 1, "*.html;*.htm;*.php;*.asp"
Me.Add "Image", 2, "*.bmp;*.jpg;*.gif;*.jpeg"
Me.Add "Office", 1, "*.doc;*.xls"
Me.Add "Text", 4, "*.txt;*.log;*.dat;*.nfo;*.ini;*.bat"
Me.Add "Video", 3, "*.avi;*.mpg;*.mpeg;*.asf"
'pdfs
End Sub

Public Sub loadFromINI()
Do Until Me.Count = 0
  Me.Remove 1
Loop

Dim skey() As String, cnt As Long, i As Long
cIni.Section = "Extensions"
cIni.EnumerateCurrentSection skey, cnt

For i = 1 To cnt
  cIni.Key = skey(i) 'eg: Audio(3)
  Me.Add Mid(skey(i), 1, InStr(skey(i), "(") - 1), Mid(skey(i), InStr(skey(i), "(") + 1, 1), cIni.Value
Next
End Sub


Public Function Add(ExtName As String, ExtType As Integer, Extensions As String, Optional skey As String) As ExtType
    'create a new object
    Dim objNewMember As ExtType
    Set objNewMember = New ExtType
    
    skey = ExtName 'uses Name as Key

    'set the properties passed into the method
    objNewMember.ExtName = ExtName
    objNewMember.ExtType = ExtType
    objNewMember.Extensions = Extensions
    If Len(skey) = 0 Then
        mCol.Add objNewMember
    Else
        mCol.Add objNewMember, skey
    End If


    'return the object created
    Set Add = objNewMember
    Set objNewMember = Nothing


End Function

Public Property Get Item(vntIndexKey As Variant) As ExtType
Attribute Item.VB_UserMemId = 0
    'used when referencing an element in the collection
    'vntIndexKey contains either the Index or Key to the collection,
    'this is why it is declared as a Variant
    'Syntax: Set foo = x.Item(xyz) or Set foo = x.Item(5)
  Set Item = mCol(vntIndexKey)
End Property



Public Property Get Count() As Long
    'used when retrieving the number of elements in the
    'collection. Syntax: Debug.Print x.Count
    Count = mCol.Count
End Property


Public Sub Remove(vntIndexKey As Variant)
    'used when removing an element from the collection
    'vntIndexKey contains either the Index or Key, which is why
    'it is declared as a Variant
    'Syntax: x.Remove(xyz)


    mCol.Remove vntIndexKey
End Sub


Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    'this property allows you to enumerate
    'this collection with the For...Each syntax
    Set NewEnum = mCol.[_NewEnum]
End Property


Private Sub Class_Initialize()
    'creates the collection when this class is created
    Set mCol = New Collection
End Sub


Private Sub Class_Terminate()
    'destroys collection when this class is terminated
    Set mCol = Nothing
End Sub

