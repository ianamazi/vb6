VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmOptions 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "IViewer Options"
   ClientHeight    =   5415
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   5985
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmOptions.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5415
   ScaleWidth      =   5985
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox pic 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   4575
      Index           =   3
      Left            =   2160
      ScaleHeight     =   4545
      ScaleWidth      =   3585
      TabIndex        =   21
      TabStop         =   0   'False
      Tag             =   "Options"
      Top             =   120
      Visible         =   0   'False
      Width           =   3615
      Begin VB.TextBox txtTimer 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BorderStyle     =   0  'None
         Height          =   240
         Left            =   1995
         TabIndex        =   33
         Text            =   "999"
         Top             =   3240
         Width           =   435
      End
      Begin VB.Frame framMedia 
         Caption         =   "Media File Options"
         Height          =   1035
         Left            =   240
         TabIndex        =   26
         Top             =   1380
         Width           =   3135
         Begin VB.CheckBox chkMediaFrame 
            Caption         =   "Show &Frame Count"
            Height          =   255
            Left            =   240
            TabIndex        =   27
            Top             =   240
            Width           =   2175
         End
         Begin VB.CheckBox chkMediaBlock 
            Caption         =   "Block Scripts and &URL Popups"
            Height          =   255
            Left            =   240
            TabIndex        =   29
            Top             =   720
            Width           =   2415
         End
         Begin VB.CheckBox chkAudioSwitch 
            Caption         =   "S&witch to Horizontal to play Audio"
            Height          =   255
            Left            =   240
            TabIndex        =   28
            Top             =   480
            Width           =   2775
         End
      End
      Begin VB.Frame framTitleCaption 
         Caption         =   "View Titlebar Caption as:"
         Height          =   615
         Left            =   240
         TabIndex        =   35
         Top             =   3720
         Width           =   3135
         Begin VB.OptionButton optCaption 
            Caption         =   "File &Name"
            Height          =   255
            Index           =   3
            Left            =   1800
            TabIndex        =   38
            Top             =   240
            Width           =   1095
         End
         Begin VB.OptionButton optCaption 
            Caption         =   "F&ull"
            Height          =   255
            Index           =   1
            Left            =   240
            TabIndex        =   36
            Top             =   240
            Width           =   615
         End
         Begin VB.OptionButton optCaption 
            Caption         =   "&Path"
            Height          =   255
            Index           =   2
            Left            =   960
            TabIndex        =   37
            Top             =   240
            Width           =   735
         End
      End
      Begin VB.CheckBox chkTBarCaptions 
         Caption         =   "View Captions for &Toolbar"
         Height          =   255
         Left            =   480
         TabIndex        =   31
         Top             =   2820
         Width           =   2175
      End
      Begin VB.CheckBox chkSortHistory 
         Caption         =   "Sort Folder History by Time &Visited"
         Height          =   255
         Left            =   480
         TabIndex        =   30
         Top             =   2580
         Width           =   2775
      End
      Begin VB.Frame framStartup 
         Caption         =   "During Startup,"
         Height          =   1035
         Left            =   240
         TabIndex        =   22
         Top             =   120
         Width           =   3135
         Begin VB.CheckBox chkStartFilelists 
            Caption         =   "Load &Default Filelists"
            Height          =   255
            Left            =   240
            TabIndex        =   25
            Top             =   720
            Width           =   1815
         End
         Begin VB.CheckBox chkStartFolder 
            Caption         =   "Load Folder &History"
            Height          =   255
            Left            =   240
            TabIndex        =   24
            Top             =   480
            Width           =   1695
         End
         Begin VB.CheckBox chkStartSplash 
            Caption         =   "Show &Splash Screen"
            Height          =   255
            Left            =   240
            TabIndex        =   23
            Top             =   240
            Width           =   1815
         End
      End
      Begin VB.Label lblOpt 
         AutoSize        =   -1  'True
         Caption         =   "sec(s)"
         Height          =   195
         Index           =   0
         Left            =   2550
         TabIndex        =   34
         Top             =   3270
         Width           =   435
      End
      Begin VB.Label lblOpt 
         AutoSize        =   -1  'True
         Caption         =   "Change Slide &every"
         Height          =   195
         Index           =   1
         Left            =   480
         TabIndex        =   32
         Top             =   3270
         Width           =   1395
      End
   End
   Begin VB.PictureBox pic 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   4575
      Index           =   1
      Left            =   2160
      ScaleHeight     =   4545
      ScaleWidth      =   3585
      TabIndex        =   3
      TabStop         =   0   'False
      Tag             =   "Actions"
      Top             =   120
      Visible         =   0   'False
      Width           =   3615
      Begin VB.Frame framLifeTimer 
         Caption         =   "Application Life Timer"
         Height          =   1095
         Left            =   240
         TabIndex        =   6
         Top             =   3240
         Width           =   3135
         Begin VB.Label lblTimer 
            AutoSize        =   -1  'True
            Caption         =   "Current Usage"
            Height          =   195
            Index           =   2
            Left            =   240
            TabIndex        =   8
            Top             =   600
            Width           =   1050
         End
         Begin VB.Label lblTimer 
            AutoSize        =   -1  'True
            Caption         =   "Total Usage"
            Height          =   195
            Index           =   1
            Left            =   240
            TabIndex        =   7
            Top             =   360
            Width           =   855
         End
      End
      Begin VB.CommandButton cmdPerform 
         Caption         =   "Perform &Selected Actions"
         Default         =   -1  'True
         Height          =   300
         Left            =   720
         TabIndex        =   5
         Top             =   2580
         Width           =   2175
      End
      Begin MSComctlLib.ListView lvwActions 
         Height          =   2175
         Left            =   240
         TabIndex        =   4
         Top             =   240
         Width           =   3135
         _ExtentX        =   5530
         _ExtentY        =   3836
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         Checkboxes      =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Object.Width           =   5098
         EndProperty
      End
   End
   Begin VB.PictureBox pic 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   4575
      Index           =   2
      Left            =   2160
      ScaleHeight     =   4545
      ScaleWidth      =   3585
      TabIndex        =   9
      TabStop         =   0   'False
      Tag             =   "Favorites"
      Top             =   120
      Visible         =   0   'False
      Width           =   3615
      Begin VB.PictureBox picAdd 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   195
         Left            =   360
         ScaleHeight     =   195
         ScaleWidth      =   375
         TabIndex        =   93
         Top             =   3450
         Visible         =   0   'False
         Width           =   375
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Add:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   700
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   0
            TabIndex        =   94
            Top             =   0
            Width           =   375
         End
      End
      Begin VB.CommandButton cmdOptAdd 
         Caption         =   "&Desktop"
         Height          =   375
         Index           =   16
         Left            =   2520
         TabIndex        =   92
         Top             =   3360
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.CommandButton cmdOptAdd 
         Caption         =   "&My Docs"
         Height          =   375
         Index           =   5
         Left            =   1680
         TabIndex        =   91
         Top             =   3360
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.CommandButton cmdOptAdd 
         Caption         =   "&Folder"
         Height          =   375
         Index           =   1
         Left            =   840
         TabIndex        =   90
         Top             =   3360
         Visible         =   0   'False
         Width           =   735
      End
      Begin VB.CheckBox chkExp 
         Caption         =   "Show ""Des&ktop"""
         Height          =   255
         Index           =   2
         Left            =   840
         TabIndex        =   15
         Top             =   2760
         Visible         =   0   'False
         Width           =   1575
      End
      Begin VB.CheckBox chkExp 
         Caption         =   "Show ""My &Documents"""
         Height          =   255
         Index           =   1
         Left            =   840
         TabIndex        =   14
         Top             =   2400
         Visible         =   0   'False
         Width           =   1935
      End
      Begin MSComctlLib.ListView lvw 
         Height          =   2535
         Index           =   3
         Left            =   360
         TabIndex        =   13
         Top             =   720
         Visible         =   0   'False
         Width           =   2895
         _ExtentX        =   5106
         _ExtentY        =   4471
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Explorer Favorites"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ListView lvw 
         Height          =   3015
         Index           =   2
         Left            =   360
         TabIndex        =   12
         Top             =   720
         Visible         =   0   'False
         Width           =   2895
         _ExtentX        =   5106
         _ExtentY        =   5318
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Favorite Playlists"
            Object.Width           =   2540
         EndProperty
      End
      Begin MSComctlLib.ListView lvw 
         Height          =   3015
         Index           =   1
         Left            =   360
         TabIndex        =   11
         Top             =   720
         Width           =   2895
         _ExtentX        =   5106
         _ExtentY        =   5318
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   1
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Text            =   "Favorite Folders"
            Object.Width           =   2540
         EndProperty
      End
      Begin VB.CommandButton cmdOpt 
         Caption         =   "&Edit"
         Height          =   320
         Index           =   4
         Left            =   2115
         TabIndex        =   19
         ToolTipText     =   "Use [F2] to Edit"
         Top             =   4080
         Width           =   555
      End
      Begin VB.CommandButton cmdOpt 
         Caption         =   "&Sort"
         Height          =   320
         Index           =   1
         Left            =   360
         TabIndex        =   16
         ToolTipText     =   "Use [Alt + S] to Sort"
         Top             =   4080
         Width           =   555
      End
      Begin VB.CommandButton cmdOpt 
         Caption         =   "&Down"
         Height          =   320
         Index           =   3
         Left            =   1530
         TabIndex        =   18
         ToolTipText     =   "Use [F7] to Move Down"
         Top             =   4080
         Width           =   555
      End
      Begin VB.CommandButton cmdOpt 
         Caption         =   "&Up"
         Height          =   320
         Index           =   2
         Left            =   945
         TabIndex        =   17
         ToolTipText     =   "Use [F6] to Move Up"
         Top             =   4080
         Width           =   555
      End
      Begin VB.CommandButton cmdOpt 
         Caption         =   "De&l"
         Height          =   320
         Index           =   5
         Left            =   2700
         TabIndex        =   20
         ToolTipText     =   "Use [Del] to Delete"
         Top             =   4080
         Width           =   555
      End
      Begin MSComctlLib.TabStrip tStrip 
         Height          =   3735
         Left            =   240
         TabIndex        =   10
         Top             =   240
         Width           =   3135
         _ExtentX        =   5530
         _ExtentY        =   6588
         TabWidthStyle   =   1
         MultiRow        =   -1  'True
         Separators      =   -1  'True
         TabMinWidth     =   1411
         _Version        =   393216
         BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
            NumTabs         =   3
            BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "&Folders"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "Pla&ylists"
               ImageVarType    =   2
            EndProperty
            BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
               Caption         =   "E&xplorer"
               ImageVarType    =   2
            EndProperty
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.PictureBox pic 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   4575
      Index           =   4
      Left            =   2160
      ScaleHeight     =   4545
      ScaleWidth      =   3585
      TabIndex        =   39
      TabStop         =   0   'False
      Tag             =   "Appearance"
      Top             =   120
      Visible         =   0   'False
      Width           =   3615
      Begin VB.CommandButton cmdTextFont 
         Caption         =   "&Text Font"
         Height          =   300
         Left            =   2160
         TabIndex        =   46
         Top             =   840
         Width           =   975
      End
      Begin VB.CheckBox chkReOrder 
         Caption         =   "&Re-Order files When Shuffling"
         Height          =   255
         Left            =   480
         TabIndex        =   49
         Top             =   1920
         Width           =   2535
      End
      Begin VB.CheckBox chkShowTime 
         Caption         =   "&Show Time with Date"
         Height          =   255
         Left            =   480
         TabIndex        =   48
         Top             =   1680
         Width           =   2175
      End
      Begin VB.CheckBox chkBgd 
         Caption         =   "Use &Background Image"
         Height          =   255
         Left            =   1200
         TabIndex        =   52
         Top             =   2760
         Width           =   2055
      End
      Begin VB.TextBox txtBgd 
         Height          =   360
         Left            =   1200
         TabIndex        =   53
         Top             =   3120
         Width           =   2175
      End
      Begin VB.CommandButton cmdLvwFont 
         Caption         =   "Set &Font"
         Height          =   300
         Left            =   2420
         TabIndex        =   58
         Top             =   3967
         Width           =   975
      End
      Begin VB.CheckBox chkLvwGrid 
         Caption         =   "View &Gridlines for Filelists"
         Height          =   255
         Left            =   480
         TabIndex        =   50
         Top             =   2160
         Width           =   2175
      End
      Begin VB.CheckBox chkLvwExtn 
         Caption         =   "View only File &Extensions"
         Height          =   255
         Left            =   480
         TabIndex        =   51
         Top             =   2400
         Width           =   2175
      End
      Begin VB.Label lblFilelist 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         Caption         =   "File List Appearance"
         ForeColor       =   &H80000010&
         Height          =   195
         Left            =   1080
         TabIndex        =   47
         Top             =   1320
         Width           =   1455
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000010&
         BorderWidth     =   2
         X1              =   240
         X2              =   3360
         Y1              =   1560
         Y2              =   1560
      End
      Begin VB.Label lblColBgd 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   480
         TabIndex        =   40
         Top             =   240
         Width           =   255
      End
      Begin VB.Label lblOpt 
         AutoSize        =   -1  'True
         Caption         =   "Window Background Colour"
         Height          =   195
         Index           =   2
         Left            =   840
         TabIndex        =   41
         Top             =   270
         Width           =   1965
      End
      Begin VB.Label lblOpt 
         AutoSize        =   -1  'True
         Caption         =   "Full Screen Background Colour"
         Height          =   195
         Index           =   3
         Left            =   840
         TabIndex        =   43
         Top             =   570
         Width           =   2175
      End
      Begin VB.Label lblColFScrBgd 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   480
         TabIndex        =   42
         Top             =   540
         Width           =   255
      End
      Begin VB.Label lblColTextBack 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   480
         TabIndex        =   44
         Top             =   870
         Width           =   255
      End
      Begin VB.Label lblTxt 
         AutoSize        =   -1  'True
         Caption         =   "Text Background"
         Height          =   195
         Index           =   1
         Left            =   840
         TabIndex        =   45
         Top             =   893
         Width           =   1215
      End
      Begin VB.Shape Shp 
         Height          =   735
         Left            =   240
         Top             =   2760
         Width           =   735
      End
      Begin VB.Image imBgd 
         Enabled         =   0   'False
         Height          =   735
         Left            =   240
         Stretch         =   -1  'True
         Top             =   2760
         Width           =   735
      End
      Begin VB.Label lblLvw 
         AutoSize        =   -1  'True
         Caption         =   "Foreground Colour"
         Height          =   195
         Index           =   2
         Left            =   840
         TabIndex        =   57
         Top             =   4020
         Width           =   1350
      End
      Begin VB.Label lblColLvwFore 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   480
         TabIndex        =   56
         Top             =   3990
         Width           =   255
      End
      Begin VB.Label lblLvw 
         AutoSize        =   -1  'True
         Caption         =   "Background Colour"
         Height          =   195
         Index           =   1
         Left            =   840
         TabIndex        =   55
         Top             =   3705
         Width           =   1350
      End
      Begin VB.Label lblColLvwBack 
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BorderStyle     =   1  'Fixed Single
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   255
         Left            =   480
         TabIndex        =   54
         Top             =   3675
         Width           =   255
      End
   End
   Begin VB.PictureBox pic 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   4575
      Index           =   5
      Left            =   2160
      ScaleHeight     =   4545
      ScaleWidth      =   3585
      TabIndex        =   59
      TabStop         =   0   'False
      Tag             =   "Extensions"
      Top             =   120
      Visible         =   0   'False
      Width           =   3615
      Begin VB.CommandButton cmdExtAdd 
         Caption         =   "A&dd"
         Height          =   300
         Left            =   360
         TabIndex        =   67
         ToolTipText     =   "Shortcut: [Insert]"
         Top             =   4170
         Width           =   855
      End
      Begin VB.CommandButton cmdExtRemove 
         Caption         =   "&Remove"
         Height          =   300
         Left            =   1380
         TabIndex        =   68
         ToolTipText     =   "Shortcut: [Delete]"
         Top             =   4170
         Width           =   855
      End
      Begin VB.CommandButton cmdExtDefaults 
         Caption         =   "&Defaults"
         Height          =   300
         Left            =   2400
         TabIndex        =   69
         Top             =   4170
         Width           =   855
      End
      Begin VB.CommandButton cmdExtEdit 
         Caption         =   "Edit &Extension"
         Height          =   300
         Index           =   0
         Left            =   360
         TabIndex        =   63
         ToolTipText     =   "Shortcut: [F2]"
         Top             =   960
         Width           =   1335
      End
      Begin VB.CommandButton cmdExtEdit 
         Caption         =   "Edit &Type"
         Height          =   300
         Index           =   1
         Left            =   1920
         TabIndex        =   64
         ToolTipText     =   "Shortcut: [Shft+F2]"
         Top             =   960
         Width           =   1335
      End
      Begin VB.CheckBox chkHtmlAsText 
         Caption         =   "Show &HTML files as Text (view source)"
         Height          =   255
         Left            =   240
         TabIndex        =   62
         Top             =   480
         Width           =   3135
      End
      Begin VB.ComboBox cmbExtension 
         Height          =   315
         Left            =   1560
         Style           =   2  'Dropdown List
         TabIndex        =   61
         Top             =   120
         Width           =   1815
      End
      Begin MSComctlLib.ImageCombo imCombo 
         Height          =   390
         Left            =   1680
         TabIndex        =   66
         Top             =   2055
         Visible         =   0   'False
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   688
         _Version        =   393216
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Text            =   "ImageCombo1"
         ImageList       =   "imList"
      End
      Begin MSComctlLib.TreeView tvw 
         Height          =   2775
         Left            =   240
         TabIndex        =   65
         Top             =   1320
         Width           =   3135
         _ExtentX        =   5530
         _ExtentY        =   4895
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   459
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         ImageList       =   "imList"
         Appearance      =   1
         OLEDragMode     =   1
         OLEDropMode     =   1
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000010&
         BorderWidth     =   2
         X1              =   120
         X2              =   3360
         Y1              =   780
         Y2              =   780
      End
      Begin VB.Label lblUnk 
         AutoSize        =   -1  'True
         Caption         =   "&Unknown files are"
         Height          =   195
         Left            =   240
         TabIndex        =   60
         Top             =   180
         Width           =   1275
      End
   End
   Begin VB.PictureBox pic 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   4575
      Index           =   6
      Left            =   2160
      ScaleHeight     =   4545
      ScaleWidth      =   3585
      TabIndex        =   70
      TabStop         =   0   'False
      Tag             =   "Deleting"
      Top             =   120
      Visible         =   0   'False
      Width           =   3615
      Begin VB.OptionButton optDel 
         Caption         =   "Delete to our '&Trash Can'"
         Height          =   255
         Index           =   3
         Left            =   480
         TabIndex        =   76
         Top             =   2160
         Width           =   2175
      End
      Begin VB.OptionButton optDel 
         Caption         =   "Delete to Windows &Recycle Bin"
         Height          =   255
         Index           =   2
         Left            =   480
         TabIndex        =   75
         Top             =   1840
         Value           =   -1  'True
         Width           =   2535
      End
      Begin VB.OptionButton optDel 
         Caption         =   "Delete &Without Recovery"
         Height          =   255
         Index           =   1
         Left            =   480
         TabIndex        =   74
         Top             =   1520
         Width           =   2175
      End
      Begin VB.OptionButton optDel 
         Caption         =   "&Disable Deleting in IViewer"
         Height          =   255
         Index           =   0
         Left            =   480
         TabIndex        =   73
         Top             =   1200
         Width           =   2295
      End
      Begin VB.CheckBox chkTrashConfirm 
         Caption         =   "Di&splay Delete Confirmation Dialog"
         Height          =   195
         Left            =   480
         TabIndex        =   71
         Top             =   360
         Width           =   2895
      End
      Begin VB.Frame framTrash 
         Caption         =   "Trash Can &Location"
         Height          =   1815
         Left            =   240
         TabIndex        =   77
         Top             =   2520
         Width           =   3135
         Begin VB.CommandButton cmdTrashClear 
            Caption         =   "C&lear"
            Height          =   320
            Left            =   1680
            TabIndex        =   82
            Top             =   1320
            Width           =   975
         End
         Begin VB.TextBox txtTrashLoc 
            Height          =   285
            Left            =   240
            Locked          =   -1  'True
            TabIndex        =   78
            Top             =   360
            Width           =   2655
         End
         Begin VB.CommandButton cmdTrashSet 
            Caption         =   "C&hange"
            Height          =   320
            Left            =   480
            TabIndex        =   79
            Top             =   840
            Width           =   855
         End
         Begin VB.CommandButton cmdTrashView 
            Caption         =   "&View"
            Height          =   320
            Left            =   1680
            TabIndex        =   80
            Top             =   840
            Width           =   975
         End
         Begin VB.Label lblTrashFiles 
            AutoSize        =   -1  'True
            Caption         =   "Files:"
            Height          =   195
            Left            =   240
            TabIndex        =   81
            Top             =   1383
            Width           =   375
         End
      End
      Begin VB.Label lblDelete 
         AutoSize        =   -1  'True
         Caption         =   "Delete Files"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   765
         TabIndex        =   72
         Top             =   840
         Width           =   975
      End
   End
   Begin MSComctlLib.ImageList imList 
      Left            =   1200
      Top             =   3000
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   20
      ImageHeight     =   20
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   5
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOptions.frx":000C
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOptions.frx":0648
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOptions.frx":0BA4
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOptions.frx":1100
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOptions.frx":165C
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imN 
      Left            =   1200
      Top             =   3720
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   16777215
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOptions.frx":1CA0
            Key             =   ""
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOptions.frx":257C
            Key             =   ""
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOptions.frx":3258
            Key             =   ""
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOptions.frx":3EAC
            Key             =   ""
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOptions.frx":4248
            Key             =   ""
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmOptions.frx":4E9C
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdOk 
      Caption         =   "&OK"
      Height          =   375
      Left            =   2400
      TabIndex        =   83
      Top             =   4800
      Width           =   975
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      Height          =   375
      Left            =   3500
      TabIndex        =   84
      Top             =   4800
      Width           =   975
   End
   Begin VB.TextBox txtRtb 
      Height          =   375
      IMEMode         =   3  'DISABLE
      Left            =   360
      TabIndex        =   86
      TabStop         =   0   'False
      Text            =   "Text"
      Top             =   3600
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.PictureBox picImg 
      BorderStyle     =   0  'None
      Height          =   495
      Left            =   360
      ScaleHeight     =   495
      ScaleWidth      =   735
      TabIndex        =   88
      TabStop         =   0   'False
      Top             =   3000
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.TextBox txtLvwFont 
      Height          =   375
      Left            =   360
      TabIndex        =   87
      TabStop         =   0   'False
      Text            =   "Lvws"
      Top             =   4080
      Visible         =   0   'False
      Width           =   735
   End
   Begin VB.CommandButton cmdApply 
      Caption         =   "&Apply"
      Height          =   375
      Left            =   4600
      TabIndex        =   85
      Top             =   4800
      Width           =   975
   End
   Begin MSComctlLib.ListView lvwN 
      Height          =   3375
      Left            =   300
      TabIndex        =   2
      Top             =   960
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   5953
      View            =   2
      Arrange         =   1
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      _Version        =   393217
      SmallIcons      =   "imN"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      Appearance      =   0
      NumItems        =   0
   End
   Begin VB.PictureBox picOut 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      ForeColor       =   &H80000008&
      Height          =   5055
      Left            =   120
      ScaleHeight     =   5025
      ScaleWidth      =   1785
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   120
      Width           =   1815
      Begin VB.Line Line3 
         X1              =   240
         X2              =   1560
         Y1              =   495
         Y2              =   495
      End
      Begin VB.Label lblVer 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "ver"
         ForeColor       =   &H00C0C0C0&
         Height          =   195
         Left            =   765
         TabIndex        =   89
         Top             =   4560
         Width           =   255
      End
      Begin VB.Label lblOpt 
         Alignment       =   2  'Center
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "O&ptions"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00808080&
         Height          =   240
         Index           =   4
         Left            =   510
         TabIndex        =   1
         Top             =   240
         Width           =   765
      End
   End
End
Attribute VB_Name = "frmOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim clr As Long

Dim Ctrl As ListView  'for favorites' lvw control

Private Sub chkAudioSwitch_Click()
If chkAudioSwitch.Value = 1 Then chkAudioSwitch.Tag = "True" Else chkAudioSwitch.Tag = "False"
cmdApply.Enabled = True
End Sub

Private Sub chkBgd_Click()
If chkBgd.Value = 1 Then
  imBgd.Enabled = True: chkBgd.Tag = "True": txtBgd.Enabled = True
  txtBgd.Tag = txtBgd.Text
  If txtBgd.Text = "" And Me.Tag = "" Then imBgd_Click
Else
  chkBgd.Tag = "False": imBgd.Enabled = False: txtBgd.Enabled = False
  txtBgd.Tag = ""
End If
cmdApply.Enabled = True
End Sub

Private Sub chkHtmlAsText_Click()
If chkHtmlAsText.Value = 1 Then chkHtmlAsText.Tag = "True" Else chkHtmlAsText.Tag = "False"
cmdApply.Enabled = True
End Sub

Private Sub chkLvwExtn_Click()
If chkLvwExtn.Value = 1 Then chkLvwExtn.Tag = "True" Else chkLvwExtn.Tag = "False"
cmdApply.Enabled = True
End Sub

Private Sub chkLvwGrid_Click()
If chkLvwGrid.Value = 1 Then chkLvwGrid.Tag = "True" Else chkLvwGrid.Tag = "False"
cmdApply.Enabled = True
End Sub

Private Sub chkMediaBlock_Click()
If chkMediaBlock.Value = 1 Then chkMediaBlock.Tag = "True" Else chkMediaBlock.Tag = "False"
cmdApply.Enabled = True
End Sub

Private Sub chkMediaFrame_Click()
If chkMediaFrame.Value = 1 Then chkMediaFrame.Tag = "True" Else chkMediaFrame.Tag = "False"
cmdApply.Enabled = True
End Sub

Private Sub chkReOrder_Click()
If chkReOrder.Value = 1 Then chkReOrder.Tag = "True" Else chkReOrder.Tag = "False"
cmdApply.Enabled = True
End Sub

Private Sub chkShowTime_Click()
If chkShowTime.Value = 1 Then chkShowTime.Tag = "True" Else chkShowTime.Tag = "False"
cmdApply.Enabled = True
End Sub

Private Sub chkSortHistory_Click()
If chkSortHistory.Value = 1 Then chkSortHistory.Tag = "True" Else chkSortHistory.Tag = "False"
  'If chkSortHistory.Value = 0 Then
    'MsgBox "History will be sorted alphabetically properly from the next time the program runs."
cmdApply.Enabled = True
End Sub

Private Sub chkStartFilelists_Click()
If chkStartFilelists.Value = 1 Then chkStartFilelists.Tag = "True" Else chkStartFilelists.Tag = "False"
cmdApply.Enabled = True
End Sub

Private Sub chkStartFolder_Click()
If chkStartFolder.Value = 1 Then chkStartFolder.Tag = "True" Else chkStartFolder.Tag = "False"
cmdApply.Enabled = True
End Sub

Private Sub chkStartSplash_Click()
If chkStartSplash.Value = 1 Then chkStartSplash.Tag = "True" Else chkStartSplash.Tag = "False"
cmdApply.Enabled = True
End Sub

Private Sub chkTBarCaptions_Click()
If chkTBarCaptions.Value = 1 Then chkTBarCaptions.Tag = "True" Else chkTBarCaptions.Tag = "False"
cmdApply.Enabled = True
End Sub

Private Sub chkTrashConfirm_Click()
If chkTrashConfirm.Value = 1 Then chkTrashConfirm.Tag = "True" Else chkTrashConfirm.Tag = "False"
cmdApply.Enabled = True
End Sub

Private Sub cmbExtension_Click()
cmbExtension.Tag = cmbExtension.ListIndex
cmdApply.Enabled = True
End Sub

Private Sub cmdApply_Click()
'no need to do anything for actions but for all other options
'pane by pane seeing if all controls on it's tag <> "" then saving and or updating change

Dim i As Integer

'action options - 1
If chkSortHistory.Tag <> "" Then Vdplayer.viViewHistoryByTime = chkSortHistory.Tag: Vdplayer.LoadHistory
  putINI "Preferences", "Sort History By Viewed Order", Vdplayer.viViewHistoryByTime



'favorites options - 2
If pic(2).Tag = "Changed" Then
  cIni.Section = "Favorite Folders": cIni.DeleteSection
  For i = 1 To lvw(1).ListItems.Count
    putINI "Favorite Folders", lvw(1).ListItems.Item(i).Text, CStr(i)
  Next
  
  cIni.Section = "Favorite Files": cIni.DeleteSection
  For i = 1 To lvw(2).ListItems.Count
    putINI "Favorite Files", lvw(2).ListItems.Item(i).Text, CStr(i)
  Next
  Vdplayer.LoadFaves
  pic(2).Tag = ""
End If

'Explorer Favorites (2)
If lvw(3).Tag = "changed" Then
  cIni.Section = "Explorer Nodes": cIni.DeleteSection
  For i = 1 To lvw(3).ListItems.Count
    putINI "Explorer Nodes", lvw(3).ListItems.Item(i).Text, lvw(3).ListItems.Item(i).SmallIcon
  Next
  'Vdplayer.Tvw_Refresh
  lvw(3).Tag = ""
End If

'Options - 3
  If chkStartSplash.Tag <> "" Then putINI "Application Start", "Splash", chkStartSplash.Tag
  If chkStartFolder.Tag <> "" Then putINI "Application Start", "Remember Folder", chkStartFolder.Tag
  If chkStartFilelists.Tag <> "" Then putINI "Application Start", "Remember Filelist", chkStartFilelists.Tag
  If chkTBarCaptions.Tag <> "" Then putINI "Preferences", "Toolbar Caption", chkTBarCaptions.Tag: Vdplayer.doTBarCaption
  
    'title bar options
  For i = 1 To 3
    If optCaption(i).Value = True Then Vdplayer.viTitle = i
  Next
  Vdplayer.setCaption
  putINI "Preferences", "Scroll What", Vdplayer.viTitle

  If txtTimer.Tag <> "" Then Vdplayer.Tmr.Interval = Val(txtTimer.Tag) * 1000: putINI "Preferences", "Timer Interval", Vdplayer.Tmr.Interval
  
  'media options
  If chkMediaBlock.Tag <> "" Then Vdplayer.ani.InvokeURLs = chkMediaBlock.Tag: _
    putINI "Preferences", "Block Media URLs", chkMediaBlock.Tag
  If chkMediaFrame.Tag <> "" Then putINI "Preferences", "Media ViewFrames", chkMediaFrame.Tag: _
    If chkMediaFrame.Value = 0 Then Vdplayer.ani.DisplayMode = mpTime Else Vdplayer.ani.DisplayMode = mpFrames
  If chkAudioSwitch.Tag <> "" Then Vdplayer.viChangeHorz = chkAudioSwitch.Tag: _
    putINI "Preferences", "Switch Horizontal for Audio", chkAudioSwitch.Tag



'Appearance - 4
  'lvws background images
  putINI "File List", "Background Image", txtBgd.Tag
  Vdplayer.icoLarge.BackColor = lblColLvwBack.BackColor: Vdplayer.icoSmall.BackColor = lblColLvwBack.BackColor
  If chkReOrder.Tag <> "" Then Vdplayer.viReorderAsShuffle = chkReOrder.Tag: _
    putINI "Preferences", "Reorder with Shuffle", Vdplayer.viReorderAsShuffle

  'lvws font (from txtLvwFont)
  Dim dLvw
  If fso.FileExists(txtBgd.Tag) = False Then txtBgd.Text = ""
  For Each dLvw In Vdplayer.lvw
    dLvw.Font.Name = txtLvwFont.Font.Name
    dLvw.Font.Size = txtLvwFont.Font.Size
    dLvw.Font.Bold = txtLvwFont.Font.Bold
    dLvw.Font.Italic = txtLvwFont.Font.Italic
    dLvw.Font.Underline = txtLvwFont.Font.Underline
    dLvw.ForeColor = lblColLvwFore.BackColor
    dLvw.BackColor = lblColLvwBack.BackColor
    If chkLvwGrid.Tag <> "" Then dLvw.GridLines = chkLvwGrid.Tag
    Set dLvw.Picture = LoadPicture("")
    dLvw.BackColor = lblColLvwBack.BackColor
    Set dLvw.Picture = LoadPicture(txtBgd.Tag)
    DoEvents
    dLvw.Refresh
  Next
  
  If chkLvwGrid.Tag <> "" Then putINI "File List", "Gridlines", chkLvwGrid.Tag
  If chkLvwExtn.Tag <> "" Then Vdplayer.viLvwExtension = chkLvwExtn.Tag: doLvwExt: putINI "File List", "Extension", chkLvwExtn.Tag
  If chkShowTime.Tag <> "" Then Vdplayer.viShowTime = chkShowTime.Tag: doLvwTim: putINI "Preferences", "Show Time", chkShowTime.Tag
  If chkBgd.Tag <> "" Then putINI "File List", "View Background Image", chkBgd.Tag
  putINI "File List", "BackColor", lblColLvwBack.BackColor
  putINI "File List", "ForeColor", lblColLvwFore.BackColor
  putINI "File List", "Font", txtLvwFont.Font
  putINI "File List", "FontSize", txtLvwFont.Font.Size

  If lblColBgd.Tag <> "" Then Vdplayer.conViewer.BackColor = lblColBgd.BackColor:   putINI "Appearance", "Background Color", lblColBgd.BackColor
  If lblColFScrBgd.Tag <> "" Then putINI "Appearance", "Full Screen Background Color", lblColFScrBgd.BackColor

  'text (rtb) options
  With Vdplayer.rtb
    putINI "Text", "Font.Name", txtRtb.Font.Name
      .Font.Name = txtRtb.Font.Name
    putINI "Text", "Font.Size", txtRtb.Font.Size
      .Font.Size = txtRtb.Font.Size
    putINI "Text", "Font.Bold", txtRtb.Font.Bold
      .Font.Bold = txtRtb.Font.Bold
    putINI "Text", "Font.Italic", txtRtb.Font.Italic
      .Font.Italic = txtRtb.Font.Italic
    putINI "Text", "Font.Underline", txtRtb.Font.Underline
      .Font.Underline = txtRtb.Font.Underline
    putINI "Text", "Font.Strikethrough", txtRtb.Font.Strikethrough
      .Font.Strikethrough = txtRtb.Font.Strikethrough
    putINI "Text", "BackColor", lblColTextBack.BackColor
      .BackColor = lblColTextBack.BackColor
  End With



'extensions options - 5
  'Default Extension's options
  If cmbExtension.Tag <> "" Then Vdplayer.viDefExtn = cmbExtension.ListIndex - 1
  cmbExtension.Tag = ""
  putINI "Preferences", "Default Extn", Vdplayer.viDefExtn
  
  If chkHtmlAsText.Tag <> "" Then
    Vdplayer.viShowHtmlAsText = chkHtmlAsText.Tag
    putINI "Preferences", "Show Html As Text", Vdplayer.viShowHtmlAsText
    If Vdplayer.CurFile <> "" And InStr(extns.Item("Html").Extensions, fso.GetExtensionName(Vdplayer.CurFile)) <> 0 Then Vdplayer.CrlClick
  End If
  
  'extensions
  If pic(5).Tag <> "" Then
    Dim nod As Node, nod2 As Node
    'clear the extensions
    For i = 1 To extns.Count: extns(i).Extensions = "": Next
    
    Set nod = tvw.Nodes(1)
    Do Until nod Is Nothing
      Set nod2 = nod.Child
      Do Until nod2 Is Nothing
        If extns(nod.Text).Extensions <> "" Then extns(nod.Text).Extensions = extns(nod.Text).Extensions & ";"
        extns(nod.Text).Extensions = extns(nod.Text).Extensions & "*." & nod2.Text
        Set nod2 = nod2.Next
      Loop
      Set nod = nod.Next
    Loop
    
    extns.saveToINI
    Vdplayer.loadExtensions
    
    pic(5).Tag = ""
  End If



'trash options - 6
  If lblDelete.Tag <> "" Then
    Vdplayer.viTrashOpt = lblDelete.Tag
    putINI "Trash", "Trash Option", Vdplayer.viTrashOpt
    lblDelete.Tag = ""
  End If
  If chkTrashConfirm.Tag <> "" Then putINI "Trash", "Trash Confirm", chkTrashConfirm.Tag
  
  If txtTrashLoc.Tag <> "" Then Vdplayer.viTrashLoc = txtTrashLoc: putINI "Trash", "Trash Location", txtTrashLoc
  




cmdApply.Enabled = False
'MsgBox "Changes took " & DateDiff("s", tim, Now) & " secs to apply."
End Sub

Private Sub doLvwExt()
On Error Resume Next
Dim strCap As String
If Vdplayer.viLvwExtension = False Then strCap = "Type" Else: strCap = "Ext"

Dim dLvw, itm As ListItem, fil As File
For Each dLvw In Vdplayer.lvw
  dLvw.ColumnHeaders(3).Text = strCap
  
  For Each itm In dLvw.ListItems
  If fso.FileExists(itm.Key) Then
    Set fil = fso.GetFile(itm.Key)
    If Vdplayer.viLvwExtension Then
      itm.SubItems(2) = fso.GetExtensionName(fil.Path)
    Else
      If fil.Type <> "File" Then itm.SubItems(2) = fil.Type
    End If
  Else
    itm.SubItems(2) = fso.GetExtensionName(itm.Key)
  End If
  Next
Next
End Sub

Private Sub doLvwTim()
Dim dLvw, itm As ListItem, fil As File
For Each dLvw In Vdplayer.lvw
  For Each itm In dLvw.ListItems
  If fso.FileExists(itm.Key) Then
    Set fil = fso.GetFile(itm.Key)
    If Vdplayer.viShowTime Then
      itm.SubItems(3) = fil.DateLastModified
    Else
      itm.SubItems(3) = Format(fil.DateLastModified, "dd mmm yy")
    End If
  End If
  Next
Next
End Sub

Private Sub cmdCancel_Click()
If imCombo.Visible Then imCombo.Visible = False: Exit Sub

If cmdApply.Enabled = True Then
  Dim m As VbMsgBoxResult
    m = MsgBox("Are you sure you want to exit the options dialog" & vbCrLf & "without updating all your option changes?", vbQuestion + vbYesNo, "Closing Options Dialog")
    If m = vbYes Then Unload Me
Else
  Unload Me
End If
End Sub


Private Sub cmdExtAdd_Click()
tvw.Nodes.Add tvw.Nodes("None"), 4, , "", 1
tvw.Nodes(tvw.Nodes.Count).Selected = True
tvw.Nodes(tvw.Nodes.Count).Tag = "new"
tvw.SelectedItem.EnsureVisible
tvw.SetFocus
tvw.StartLabelEdit

pic(5).Tag = "changed"
cmdApply.Enabled = True

End Sub

Private Sub cmdExtDefaults_Click()
Dim m As VbMsgBoxResult
m = MsgBox("Certain you want to restore default file asociations?", vbQuestion + vbYesNo, "Confirm set Defaults")
If m = vbNo Then Exit Sub
tvw.Nodes.Clear: imCombo.ComboItems.Clear
extns.loadDefaults
Form_Load

pic(5).Tag = "changed"
cmdApply.Enabled = True

End Sub

Private Sub cmdExtEdit_Click(Index As Integer)
tvw_KeyDown vbKeyF2, Index

pic(5).Tag = "changed"
cmdApply.Enabled = True

End Sub

Private Sub cmdExtRemove_Click()
If Not tvw.SelectedItem Is Nothing Then
  If Not tvw.SelectedItem.Parent Is Nothing Then tvw.Nodes.Remove tvw.SelectedItem.Index
End If

pic(5).Tag = "changed"
cmdApply.Enabled = True

End Sub

Private Sub cmdLvwFont_Click()
  ShowFont Me, txtLvwFont
  'lblColLvwFore.BackColor = txtLvwFont.ForeColor  'not sure thisll work i think clr dlg can have only intrinsic vb colors
cmdApply.Enabled = True
End Sub

Private Sub cmdOK_Click()
  If cmdApply.Enabled = True Then cmdApply_Click
  Unload Me
End Sub

Private Sub cmdOpt_Click(Index As Integer)

On Error Resume Next
Dim r As Integer, tmp As String, i As Integer
r = Ctrl.SelectedItem.Index

Select Case Index
Case 1 '"&Sort"
  Ctrl.Sorted = True
  Ctrl.Sorted = False
Case 2 '"&Up"
  If r = 1 Then Exit Sub
  tmp = Ctrl.SelectedItem.Text
  Ctrl.SelectedItem.Text = Ctrl.ListItems(r - 1).Text
  Ctrl.ListItems(r - 1).Text = tmp
    tmp = Ctrl.SelectedItem.SmallIcon
    Ctrl.SelectedItem.SmallIcon = Ctrl.ListItems(r - 1).SmallIcon
    Ctrl.ListItems(r - 1).SmallIcon = CInt(tmp)
  Ctrl.ListItems(r - 1).Selected = True
Case 3 '"&Down"
  If r = Ctrl.ListItems.Count Then Exit Sub
  tmp = Ctrl.SelectedItem.Text
  Ctrl.SelectedItem.Text = Ctrl.ListItems(r + 1).Text
  Ctrl.ListItems(r + 1).Text = tmp
    tmp = Ctrl.SelectedItem.SmallIcon
    Ctrl.SelectedItem.SmallIcon = Ctrl.ListItems(r + 1).SmallIcon
    Ctrl.ListItems(r + 1).SmallIcon = CInt(tmp)
  Ctrl.ListItems(r + 1).Selected = True
Case 4 '"&Edit"
  Ctrl.SetFocus
  Ctrl.StartLabelEdit
Case 5 '"&Del"
  If Ctrl.ListItems.Count >= 1 Then Ctrl.ListItems.Remove (Ctrl.SelectedItem.Index) ': If r = lvw(Inde).ListCount Then lvw(Inde).ListIndex = r - 1 Else lvw(Inde).ListIndex = r
Case Else
  MsgBox cmdOpt(Index).Caption
End Select
If Ctrl.Index = 3 Then lvw(3).Tag = "changed" Else pic(2).Tag = "Changed"
cmdApply.Enabled = True
If Me.Visible And Index <> 4 Then Ctrl.SetFocus

End Sub

Private Sub cmdOptAdd_Click(Index As Integer)
If Index = 1 Then
  lvw(3).ListItems.Add lvw(3).ListItems.Count + 1, , Vdplayer.fList.Path, , 10
Else
  Dim strSplFol As String  ' receives the path of My Documents
  strSplFol = Space(260)
    SHGetSpecialFolderPath Me.hWnd, strSplFol, Index, 0
    'Index: 5=CSIDL_PERSONAL (my docs); 16=CSIDL_DESKTOPDIRECTORY (dtop)
    strSplFol = Left(strSplFol, InStr(strSplFol, vbNullChar) - 1)
  If Index = 16 Then Index = 9 Else Index = 11
  lvw(3).ListItems.Add lvw(3).ListItems.Count + 1, , strSplFol, , Index
End If

lvw(3).Tag = "changed"
cmdApply.Enabled = True
End Sub


Private Sub cmdPerform_Click()
Dim m As VbMsgBoxResult, i As Integer

m = MsgBox("Are you certain you wish to perform the selected action(s)." _
  & vbCrLf & "None of them can be undone.", vbQuestion + vbYesNo, "Confirm Perform Actions")
If m = vbNo Then Exit Sub

Dim drv As Drive, strRem As String
If lvwActions.ListItems(3).Checked = True Or lvwActions.ListItems(6).Checked = True Then
  For Each drv In fso.Drives
    If drv.DriveType <> Fixed And drv.DriveType <> UnknownType Then strRem = strRem & drv.DriveLetter & ":"
  Next
End If

'cmdClearFolderHistory_Click()
If lvwActions.ListItems(1).Checked Then
  cIni.Section = "Folder History": cIni.DeleteSection
  Vdplayer.cmbHistory.ComboItems.Clear
  Vdplayer.lstSort.Clear
  putINI "Folder History", Mid(Vdplayer.lvw(1).Tag, 1, Len(Vdplayer.lvw(1).Tag) - 1), Now
  Vdplayer.SetHistoryFolder cIni.Key
End If


'cmdClearSearchFolHistory_Click()
If lvwActions.ListItems(2).Checked Then
  cIni.Section = "Search Folders": cIni.DeleteSection
  With Vdplayer.cmbSearchIn
    If .SelectedItem.Index > .ComboItems("Browse").Index Then .ComboItems("Hard Drives").Selected = True
    Do Until .ComboItems.Count = .ComboItems("Browse").Index
      .ComboItems.Remove (.ComboItems("Browse").Index + 1)
    Loop
  End With
End If


'cmdRemoveInvalidFolders_Click()
If lvwActions.ListItems(3).Checked Then
  m = MsgBox("Would You like to clear Folders on removable drives as well?", vbYesNo + vbQuestion, "Remove Invalid Folders")
  
    i = 1
  With Vdplayer.cmbHistory
    Do Until i > .ComboItems.Count
      If m = vbNo And InStr(strRem, fso.GetDriveName(.ComboItems(i).Key)) <> 0 Then i = i + 1: GoTo Nxt
      If fso.FolderExists(.ComboItems(i).Key) = False Then .ComboItems.Remove (i) Else i = i + 1
Nxt:
    Loop
    
    Vdplayer.lstSort.Clear
    cIni.Section = "Folder History": cIni.DeleteSection
    Dim cmbItm As ComboItem
    For Each cmbItm In .ComboItems
      Vdplayer.lstSort.AddItem cmbItm.Key
      putINI "Folder History", cmbItm.Key, ""
    Next
    If Vdplayer.lstSort.ListCount <> .ComboItems.Count Then _
      MsgBox "IViewer created a disparity in Folder History. This may manifest as missing items or improper sorting.", vbInformation
  End With
End If

'cmdClearFilterHistory_Click()
If lvwActions.ListItems(4).Checked Then
  cIni.Section = "Filter History": cIni.DeleteSection
  Do Until Vdplayer.cmbFilter.ListCount = extns.Count
    Vdplayer.cmbFilter.RemoveItem Vdplayer.cmbFilter.ListCount - 1
  Loop
End If


'cmdClearSearchHistory_Click()
If lvwActions.ListItems(5).Checked Then
  cIni.Section = "Search History": cIni.DeleteSection
  Do Until Vdplayer.cmbSearch.ListCount = extns.Count
    Vdplayer.cmbSearch.RemoveItem Vdplayer.cmbSearch.ListCount - 1
  Loop
End If

'Dim ts As TextStream, lins() As String, tmp() As String
'
''cmdClearTimeLibrary_Click()
'If lvwActions.ListItems(6).Checked Then
'  m = MsgBox("Sure you want to clear ALL ENTRIES in Time Library?" & vbCrLf & _
'  "'No' will clear records of files no longer present" & vbCrLf & vbCrLf & _
'  "The Time Library keeps track of the duration of media files", vbYesNoCancel + vbQuestion, "Clear Time Library")
'
'  If m = vbCancel Then
'    Exit Sub
'  ElseIf m = vbYes Then
'    fso.DeleteFile apPath & "Time Library.ini", True
'  ElseIf m = vbNo Then
'
'    m = MsgBox("Retain files on removable drives?", vbYesNoCancel, "Clean Up Time Library")
'    If m = vbCancel Then Exit Sub
'
'    Set ts = fso.OpenTextFile(apPath & "Time Library.ini", ForReading)
'
'    lins = Split(ts.ReadAll, vbCrLf)
'    ts.Close
'
'    Set ts = fso.CreateTextFile(apPath & "Time Library.ini", True)
'    ts.WriteLine "[Time Library]"
'    On Error Resume Next
'    For i = 1 To UBound(lins)
'      tmp = Split(lins(i), "=")
'      If fso.FileExists(tmp(0)) = True Then
'        ts.WriteLine lins(i)
'      Else
'        If m = vbYes And InStr(1, strRem, fso.GetDriveName(tmp(0)), vbTextCompare) > 0 Then ts.WriteLine lins(i)
'      End If
'    Next i
'
'    ts.Close
'  End If
'End If

If lvwActions.ListItems(7).Checked Then
  If Vdplayer.lvw(2).ListItems.Count > 0 Then
    m = MsgBox("To load Time Library, We Must first Clear the Current Playlist" & vbCrLf & _
     "Do You wish to proceed?" & vbCrLf & vbCrLf & _
     "The Time Library keeps track of the duration of media files", vbYesNo + vbQuestion, "Clear Current Playlist")
    Vdplayer.lvw(2).ListItems.Clear
  End If
  
  If m = vbYes Then
    m = MsgBox("Load files on removable drives?", vbYesNoCancel, "Load Unavailable Files?")
    If m = vbCancel Then Exit Sub
    
    Set ts = fso.OpenTextFile(apPath & "Time Library.ini", ForReading)
   
    lins = Split(ts.ReadAll, vbCrLf)
    ts.Close
    
    For i = 1 To UBound(lins)
      If InStr(lins(i), "=") > 0 Then
        tmp = Split(lins(i), "=")
        If m = vbYes Or fso.FileExists(tmp(0)) = True Then
          Vdplayer.AddItem 2, tmp(0)
        End If
      End If
    Next i
  End If
End If

If lvwActions.ListItems(8).Checked Then
  If Vdplayer.lvw(2).ListItems.Count > 0 Then
    m = MsgBox("To load files with lyrics, We must first Clear the Current Playlist" & vbCrLf & _
     "Do You wish to proceed?", vbYesNo + vbQuestion, "Clear Current Playlist")
    Vdplayer.lvw(2).ListItems.Clear
  End If
  
  If m = vbYes Then
    Dim lin As String
    On Error Resume Next
    Set ts = fso.OpenTextFile(GetSetting(apComp, "Lyriks", "Lyrics Library File"))
    Do Until ts.AtEndOfStream
      lin = ts.ReadLine
      lin = Left(lin, InStr(lin, "=") - 1)
      If fso.FileExists(lin) Then Vdplayer.AddItem 2, lin
    Loop
    ts.Close
  End If
End If

cmdPerform.Enabled = False
End Sub

Private Sub imCombo_Click()
If Not tvw.SelectedItem Is Nothing Then
  If tvw.SelectedItem.Parent <> imCombo.SelectedItem.Text Then
    tvw.Nodes.Add tvw.Nodes(imCombo.SelectedItem.Text), 4, , tvw.SelectedItem.Text, imCombo.SelectedItem.Image
    tvw.Nodes.Remove tvw.SelectedItem.Index
    tvw.Nodes(tvw.Nodes.Count).Selected = True
    tvw.SelectedItem.EnsureVisible
  End If
  
  imCombo.Visible = False
  If tvw.Tag <> "" And tvw.SelectedItem.Text = tvw.Tag Then Vdplayer.conViewer.Tag = imCombo.SelectedItem.Image - 1
  pic(5).Tag = "changed"
  cmdApply.Enabled = True
End If
End Sub

Private Sub lvwActions_ItemCheck(ByVal Item As MSComctlLib.ListItem)
If Item.Checked = True Then cmdPerform.Enabled = True
End Sub

Private Sub cmdTextFont_Click()
ShowFont Me, txtRtb
cmdApply.Enabled = True
End Sub

Private Sub cmdTrashClear_Click()
Dim fldr As Folder, m As VbMsgBoxResult
m = MsgBox("Sure you wanna empty trash can?" & vbCrLf & Vdplayer.viTrashLoc & vbCrLf & "This action is ireversible.", vbQuestion + vbYesNo)
If fso.FolderExists(Vdplayer.viTrashLoc) Then Else MsgBox "'" & Vdplayer.viTrashLoc & "' does not exist. Click OK to continue.": Exit Sub
If m = 6 Then
  Set fldr = fso.GetFolder(Vdplayer.viTrashLoc)
  fldr.Delete
  fso.CreateFolder (Vdplayer.viTrashLoc)
  lblTrashFiles = "Files: 0  Size: 0b"
End If
End Sub

Private Sub cmdTrashSet_Click()
Dim fl As String
fl = ShowFolder(Me, "Select folder for Trash.", Vdplayer.viTrashLoc)
If fl = "" Then Exit Sub
Vdplayer.TrashChk (fl)
txtTrashLoc = fl: txtTrashLoc.ToolTipText = fl: txtTrashLoc.Tag = fl
cmdApply.Enabled = True
End Sub

Private Sub cmdTrashView_Click()
Vdplayer.TrashChk Vdplayer.viTrashLoc
Shell "explorer.exe /e," & getINI("Trash", "Trash Location", apPath & "Trash"), vbMaximizedFocus
End Sub

Private Sub Form_Load()
lblVer = "Version: " & App.Major & "." & App.Minor & vbCrLf & "Build: " & App.Revision
lvwN.ListItems.Add 1, , " Actions", , 1

  With lvwActions.ListItems
    .Add 1, , "Clear History of Folders Visited"
    .Add 2, , "Clear History of Searched Folders"
    .Add 3, , "Remove Invalid Folders (Visited)"
    .Add 4, , "Clear File Filters Text History"
    .Add 5, , "Clear File Search Text History"
    .Add 6, , "Clear Time Library"
    .Add 7, , "Load Time Library Into Playlist"
    If fso.FileExists(GetSetting(apComp, "Lyriks", "Lyrics Library File")) = True Then
      .Add 8, , "Load Songs With Lyrics"
      .Item(8).ToolTipText = "Clears the Playlist and loads songs with lyrics available"
    End If
    
    .Item(1).ToolTipText = "This will clear history of all folders visited"
    .Item(2).ToolTipText = "This clears the history of locations in which searches have been conducted"
    .Item(3).ToolTipText = "You may also choose to retain folders on removable media"
    .Item(4).ToolTipText = "Clears the Drop-Down text for File Filters"
    .Item(5).ToolTipText = "Clears the Drop-Down text for File Search"
    .Item(6).ToolTipText = "The Time Library tells IViewer the times of all played media"
    .Item(7).ToolTipText = "Clears the Playlist and adds entire contents of Time Library"
    'Time Library stores durations of all media files played
    Dim str As String, i As Integer
    str = getINI("Preferences", "Selected Actions", "")
    For i = 1 To 6
      If InStr(str, i) > 0 Then .Item(i).Checked = True
    Next
  End With
  
  Dim mn As Long
  mn = getINI("Application Start", "Life Timer", 0)
  lblTimer(2) = "Current Usage: " & doMinutes(DateDiff("n", Vdplayer.timStart, Now))
  lblTimer(1) = "Total Usage: " & doMinutes(mn + DateDiff("n", Vdplayer.timStart, Now))

lvwN.ListItems.Add 2, , " Favorites", , 2
  Set Ctrl = lvw(1)
  lvw(1).SmallIcons = Vdplayer.imTvw
  lvw(2).SmallIcons = Vdplayer.imTvw
  lvw(3).SmallIcons = Vdplayer.imTvw

lvwN.ListItems.Add 3, , " Options", , 3
  If True = getINI("Application Start", "Splash", True) Then chkStartSplash.Value = 1
  If True = getINI("Application Start", "Remember Filelist", True) Then chkStartFilelists.Value = 1
  If True = getINI("Application Start", "Remember Folder", True) Then chkStartFolder.Value = 1

  optCaption(Vdplayer.viTitle).Value = True
  
  If True = getINI("Preferences", "Toolbar Caption", "True") Then chkTBarCaptions.Value = 1
  If Vdplayer.viViewHistoryByTime = True Then chkSortHistory.Value = 1
  
  txtTimer = getINI("Preferences", "Timer Interval", 1000) / 1000
  
  If True = getINI("Preferences", "Block Media URLs", True) Then chkMediaBlock.Value = 1
  If False = getINI("Preferences", "Media ViewFrames", False) Then chkMediaFrame.Value = 1
  If True = Vdplayer.viChangeHorz Then chkAudioSwitch.Value = 1



lvwN.ListItems.Add 4, , " Appearance", , 4
  If Vdplayer.viReorderAsShuffle Then chkReOrder.Value = 1
  If True = getINI("File List", "Extension", True) Then chkLvwExtn.Value = 1
  If True = getINI("File List", "Gridlines", False) Then chkLvwGrid.Value = 1
  If Vdplayer.viShowTime Then chkShowTime.Value = 1
  Me.Tag = "loading"
  If True = getINI("File List", "View Background Image", False) Then chkBgd.Value = 1 'chkBgd_Click
  Me.Tag = ""
  If fso.FileExists(getINI("File List", "Background Image")) Then
    BgdSet cIni.Value
  Else
    txtBgd = cIni.Value
    'txtBgd.Tag = txtBgd
  End If
  lblColLvwBack.BackColor = Vdplayer.lvw(1).BackColor
  lblColLvwFore.BackColor = Vdplayer.lvw(1).ForeColor
  txtLvwFont.ForeColor = lblColLvwFore.BackColor
  txtLvwFont.Font.Name = Vdplayer.lvw(1).Font.Name
  txtLvwFont.Font.Size = Vdplayer.lvw(1).Font.Size

  Vdplayer.loadRtbFont txtRtb
  
  lblColBgd.BackColor = Vdplayer.conViewer.BackColor
  lblColFScrBgd.BackColor = getINI("Appearance", "Full Screen Background Color", 14737632)
  lblColTextBack.BackColor = Vdplayer.rtb.BackColor


lvwN.ListItems.Add 5, , " Extensions", , 5
  cmbExtension.Clear
  cmbExtension.AddItem "Prompt each time"
  cmbExtension.AddItem "same as previous"
  cmbExtension.AddItem "IE (htm/office/pdf)"
  cmbExtension.AddItem "Image"
  cmbExtension.AddItem "Media (Audio/Video)"
  cmbExtension.AddItem "Text"
  cmbExtension.ListIndex = Vdplayer.viDefExtn + 1

  If Vdplayer.viShowHtmlAsText Then chkHtmlAsText.Value = 1

  loadExtns

lvwN.ListItems.Add 6, , " Deleting", , 6
  optDel(Vdplayer.viTrashOpt).Value = True
  If True = getINI("Trash", "Trash Confirm", True) Then chkTrashConfirm.Value = 1
  txtTrashLoc = Vdplayer.viTrashLoc
  If fso.FolderExists(Vdplayer.viTrashLoc) Then
    Dim fl As Folder
    Set fl = fso.GetFolder(Vdplayer.viTrashLoc)
    lblTrashFiles = fl.Files.Count & " file(s) [" & StrSiz(fl.Size) & "]"
  Else
    lblTrashFiles = "folder unavailable": lblTrashFiles.Enabled = False
  End If


cmdApply.Enabled = False
End Sub

Public Sub loadExtns()
Dim ext() As String, typ As String
Dim i As Integer, j As Integer

For i = 1 To extns.Count
  imCombo.ComboItems.Add i, extns.Item(i).ExtName, extns.Item(i).ExtName, extns.Item(i).ExtType + 1
  ext = Split(extns.Item(i).Extensions, ";")
  
  tvw.Nodes.Add , 4, extns(i).ExtName, extns(i).ExtName, extns(i).ExtType + 1
  tvw.Nodes(tvw.Nodes.Count).Sorted = True
  For j = LBound(ext) To UBound(ext)
    tvw.Nodes.Add extns(i).ExtName, 4, , Mid(ext(j), 3), extns(i).ExtType + 1
  Next
Next
End Sub


Private Sub Form_Resize()
lvwN.ListItems(CInt(getINI("Preferences", "Options Tab Index", 1))).Selected = True
lvwN_ItemClick lvwN.SelectedItem

lvw(1).ColumnHeaders(1).Width = lvw(1).Width - 300
lvw(2).ColumnHeaders(1).Width = lvw(2).Width - 300
lvw(3).ColumnHeaders(1).Width = lvw(3).Width - 300

If tvw.Tag <> "" Then 'auto add (coz vdplayer detecteed something and the user wants to define it here)
  tvw.Nodes.Add tvw.Nodes("None"), 4, , tvw.Tag, 1
  tvw.Nodes(tvw.Nodes.Count).Selected = True
  tvw.SelectedItem.EnsureVisible
  
  tvw_KeyDown vbKeyF2, 1
End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
putINI "Preferences", "Options Tab Index", lvwN.SelectedItem.Index

Dim i As Integer, str As String
For i = 1 To 6
  If lvwActions.ListItems(i).Checked Then str = str & i
Next
putINI "Preferences", "Selected Actions", str

End Sub

Private Sub imBgd_Click()
On Error Resume Next
Dim fn As String, slst As String
'must be able to tell user if selected file is an image or not
  
  If fso.FileExists(getINI("File List", "Background Image")) Then slst = getINI("File List", "Background Image") Else slst = apPath
  fn = ShowFileOpenSave(Me, True, Shp.Tag, slst, "Images (" & extns.Item("Image").Extensions & ")" & Chr(0) & extns.Item("Image").Extensions & Chr(0) & "All files(*.*)" & Chr(0) & "*.*", , , OpenSaveFlag.OFN_HIDEREADONLY)
  If fso.FileExists(fn) Then BgdSet fn
End Sub

Private Sub BgdSet(fil As String)
On Error GoTo Serr

imBgd.Picture = LoadPicture(fil)

If imBgd.Picture <> 0 Then
  If imBgd.Picture.Height > imBgd.Picture.Width Then
    imBgd.Top = Shp.Top: imBgd.Height = 735
    imBgd.Width = 735 * imBgd.Picture.Width / imBgd.Picture.Height
    imBgd.Left = Shp.Left + (735 - imBgd.Width) / 2
  Else
    imBgd.Left = Shp.Left
    imBgd.Height = 735 * imBgd.Picture.Height / imBgd.Picture.Width
    imBgd.Top = Shp.Top + (735 - imBgd.Height) / 2
  End If
End If

txtBgd = fil
txtBgd.Tag = fil

cmdApply.Enabled = True

Exit Sub
Serr:
  MsgBox "File is not a valid Image File.", vbInformation
On Error Resume Next
  imBgd(1).Picture = LoadPicture(txtBgd)
End Sub

Private Sub lblColBgd_Click()
  clr = ShowColor(Me, lblColBgd.BackColor, 1)
  If clr <> -1 Then lblColBgd.BackColor = clr: lblColBgd.Tag = clr: cmdApply.Enabled = True
End Sub

Private Sub lblColFScrBgd_Click()
  clr = ShowColor(Me, lblColFScrBgd.BackColor, 1)
  If clr <> -1 Then lblColFScrBgd.BackColor = clr: lblColFScrBgd.Tag = clr: cmdApply.Enabled = True
End Sub

Private Sub lblColLvwBack_Click()
  clr = ShowColor(Me, lblColLvwBack.BackColor, 1)
  If clr <> -1 Then lblColLvwBack.BackColor = clr: lblColLvwBack.Tag = clr: cmdApply.Enabled = True
End Sub

Private Sub lblColLvwFore_Click()
  clr = ShowColor(Me, lblColLvwFore.BackColor, 1)
  If clr <> -1 Then lblColLvwFore.BackColor = clr: txtLvwFont.ForeColor = clr: lblColLvwFore.Tag = clr: cmdApply.Enabled = True
End Sub

Private Sub lblColTextBack_Click()
  clr = ShowColor(Me, lblColTextBack.BackColor, 1)
  If clr <> -1 Then lblColTextBack.BackColor = clr: lblColTextBack.ForeColor = clr: lblColTextBack.Tag = clr: cmdApply.Enabled = True
End Sub

Private Sub lvw_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyS And Shift = 2 Then cmdOpt_Click 1
If KeyCode = vbKeyF6 Then cmdOpt_Click 2
If KeyCode = vbKeyF7 Then cmdOpt_Click 3
If KeyCode = vbKeyF2 Then cmdOpt_Click 4
If KeyCode = vbKeyDelete Then cmdOpt_Click 5
End Sub

Private Sub lvwN_ItemClick(ByVal Item As MSComctlLib.ListItem)
Dim pBox As PictureBox
For Each pBox In pic
  If pBox.Index = Item.Index Then pBox.Visible = True Else pBox.Visible = False
Next

If Item.Text = " Favorites" And pic(2).Tag <> "Loaded" Then LoadFaves
End Sub

Private Sub optCaption_Click(Index As Integer)
cmdApply.Enabled = True
End Sub

Private Sub LoadFaves()
Dim i As Integer, cnt As Long, strS() As String
lvw(1).ListItems.Clear: lvw(2).ListItems.Clear

cIni.Section = "Favorite Folders": cIni.EnumerateCurrentSection strS, cnt
For i = 1 To cnt
  If fso.FolderExists(strS(i)) Then
    lvw(1).ListItems.Add i, , strS(i), , 6
  Else
    lvw(1).ListItems.Add i, , strS(i), , 8
  End If
Next

cIni.Section = "Favorite Files": cIni.EnumerateCurrentSection strS, cnt
For i = 1 To cnt
  lvw(2).ListItems.Add i, , strS(i), , 12
Next

cIni.Section = "Explorer Nodes": cIni.EnumerateCurrentSection strS, cnt
For i = 1 To cnt
  cIni.Key = strS(i)
  If fso.FolderExists(strS(i)) Then
    lvw(3).ListItems.Add lvw(3).ListItems.Count + 1, , strS(i), , CInt(cIni.Value)
  Else
    lvw(3).ListItems.Add lvw(3).ListItems.Count + 1, , strS(i), , 8
  End If
Next

pic(2).Tag = "Loaded"
End Sub

Private Sub optDel_Click(Index As Integer)
lblDelete.Tag = Index
cmdApply.Enabled = True
End Sub

Private Sub tStrip_Click()
Dim i As Integer
Set Ctrl = lvw(tStrip.SelectedItem.Index)
For i = 1 To 3
If i = Ctrl.Index Then lvw(i).Visible = True Else lvw(i).Visible = False
Next
If Ctrl.Index = 3 Then picAdd.Visible = True: cmdOptAdd(1).Visible = True: cmdOptAdd(5).Visible = True: cmdOptAdd(16).Visible = True Else _
  picAdd.Visible = False: cmdOptAdd(1).Visible = False: cmdOptAdd(5).Visible = False: cmdOptAdd(16).Visible = False
End Sub

Private Sub tvw_AfterLabelEdit(Cancel As Integer, NewString As String)
If tvw.SelectedItem.Tag = "new" Then 'just added
  If tvw.SelectedItem.Text = "" Then
    tvw.Nodes.Remove tvw.SelectedItem.Index
  Else
    tvw_KeyDown vbKeyF2, 1
    tvw.SelectedItem.Tag = ""
  End If
End If
End Sub

Private Sub tvw_KeyDown(KeyCode As Integer, Shift As Integer)
If tvw.SelectedItem Is Nothing Then Exit Sub
If KeyCode = vbKeyF2 And Not tvw.SelectedItem.Parent Is Nothing Then
  If Shift = 1 Then
    imCombo.ComboItems(tvw.SelectedItem.Parent.Text).Selected = True
    imCombo.Top = tvw.Top - 100
    imCombo.Visible = True
    imCombo.SetFocus
  Else
    tvw.StartLabelEdit
    pic(5).Tag = "changed"
    cmdApply.Enabled = True
  End If
ElseIf KeyCode = vbKeyInsert Then
  cmdExtAdd_Click
ElseIf KeyCode = vbKeyDelete Then
  cmdExtRemove_Click
End If
End Sub

Private Sub tvw_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
If Button = 2 Then
  If tvw.HitTest(x, y) Is Nothing Then imCombo.Visible = False: Exit Sub
  tvw.SelectedItem = tvw.HitTest(x, y)
  If tvw.SelectedItem.Parent Is Nothing Then imCombo.Visible = False: Exit Sub
  imCombo.Top = y + tvw.Top - 150
  imCombo.Visible = True
  imCombo.ComboItems(tvw.SelectedItem.Parent.Key).Selected = True
ElseIf imCombo.Visible Then
  imCombo.Visible = False
End If
End Sub

Private Sub tvw_OLECompleteDrag(Effect As Long)
Set tvw.DropHighlight = Nothing
End Sub

Private Sub tvw_OLEDragDrop(Data As MSComctlLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single)
Set tvw.DropHighlight = tvw.HitTest(x, y)

If tvw.DropHighlight Is Nothing Then Exit Sub

If tvw.DropHighlight.Parent Is Nothing Then
  tvw.Nodes.Add tvw.DropHighlight, 4, , tvw.SelectedItem.Text, tvw.DropHighlight.Image
Else
  tvw.Nodes.Add tvw.DropHighlight.Parent, 4, , tvw.SelectedItem.Text, tvw.DropHighlight.Image
End If

tvw.Nodes.Remove tvw.SelectedItem.Index
tvw.Nodes(tvw.Nodes.Count).Selected = True
tvw.SelectedItem.EnsureVisible

Set tvw.DropHighlight = Nothing

pic(5).Tag = "changed"
cmdApply.Enabled = True

End Sub

Private Sub tvw_OLEDragOver(Data As MSComctlLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single, state As Integer)
Set tvw.DropHighlight = tvw.HitTest(x, y)

If Not tvw.DropHighlight Is Nothing Then
  If tvw.DropHighlight.Parent Is Nothing Then
    If tvw.DropHighlight.Text = tvw.SelectedItem.Parent.Text Then Effect = 0    'cant drop on same type
  Else
    If tvw.DropHighlight.Parent.Text = tvw.SelectedItem.Parent.Text Then Effect = 0    'cant drop on same type
  End If
End If

End Sub

Private Sub tvw_OLEStartDrag(Data As MSComctlLib.DataObject, AllowedEffects As Long)
Set tvw.DropHighlight = Nothing
If tvw.SelectedItem.Parent Is Nothing Then AllowedEffects = 0
End Sub


Private Sub txtBgd_Change()
If fso.FileExists(txtBgd) And imBgd.Tag <> txtBgd Then BgdSet txtBgd
End Sub

Private Sub txtTimer_Change()
If txtTimer = "" Then Exit Sub
If Val(txtTimer) <= 0 Or Val(txtTimer) > 999 Then MsgBox "Must pick a value between 1 and 999.", vbInformation: Exit Sub
txtTimer.Tag = txtTimer
cmdApply.Enabled = True
End Sub

