VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "shdocvw.dll"
Object = "{22D6F304-B0F6-11D0-94AB-0080C74C7E95}#1.0#0"; "msdxm.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form Vdplayer 
   AutoRedraw      =   -1  'True
   Caption         =   "VdPlayer"
   ClientHeight    =   8310
   ClientLeft      =   60
   ClientTop       =   -1170
   ClientWidth     =   8880
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "video.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   8310
   ScaleWidth      =   8880
   StartUpPosition =   2  'CenterScreen
   Tag             =   "loading"
   WindowState     =   2  'Maximized
   Begin VB.Timer tmrRev 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   5040
      Top             =   5760
   End
   Begin VB.Timer tmrAni 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   5040
      Top             =   3840
   End
   Begin VB.PictureBox vlin 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      DrawStyle       =   5  'Transparent
      FillColor       =   &H00008000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6105
      Left            =   2400
      MousePointer    =   9  'Size W E
      ScaleHeight     =   6105
      ScaleWidth      =   60
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   600
      Width           =   60
   End
   Begin VB.PictureBox hhlin 
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      DrawStyle       =   5  'Transparent
      FillColor       =   &H00008000&
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   60
      Left            =   0
      MousePointer    =   7  'Size N S
      ScaleHeight     =   60
      ScaleWidth      =   6780
      TabIndex        =   5
      TabStop         =   0   'False
      Tag             =   "2000"
      Top             =   2040
      Visible         =   0   'False
      Width           =   6780
   End
   Begin VB.PictureBox conViewer 
      AutoRedraw      =   -1  'True
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      Height          =   5535
      Left            =   5640
      OLEDropMode     =   1  'Manual
      ScaleHeight     =   5535
      ScaleWidth      =   6135
      TabIndex        =   7
      TabStop         =   0   'False
      Top             =   360
      Visible         =   0   'False
      Width           =   6135
      Begin RichTextLib.RichTextBox rtb 
         Height          =   1815
         Left            =   0
         TabIndex        =   34
         Top             =   0
         Visible         =   0   'False
         Width           =   1815
         _ExtentX        =   3201
         _ExtentY        =   3201
         _Version        =   393217
         HideSelection   =   0   'False
         ReadOnly        =   -1  'True
         ScrollBars      =   2
         AutoVerbMenu    =   -1  'True
         OLEDragMode     =   0
         OLEDropMode     =   0
         TextRTF         =   $"video.frx":08CA
      End
      Begin VB.PictureBox picSmall 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   240
         Left            =   1920
         ScaleHeight     =   240
         ScaleWidth      =   240
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   4200
         Visible         =   0   'False
         Width           =   240
      End
      Begin VB.PictureBox picLarge 
         Appearance      =   0  'Flat
         AutoRedraw      =   -1  'True
         AutoSize        =   -1  'True
         BackColor       =   &H00C0C0C0&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   704
         Left            =   1080
         ScaleHeight     =   705
         ScaleWidth      =   705
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   3840
         Visible         =   0   'False
         Width           =   704
      End
      Begin VB.VScrollBar vs 
         Height          =   1215
         LargeChange     =   5
         Left            =   3960
         Max             =   1000
         TabIndex        =   9
         Top             =   0
         Value           =   1
         Visible         =   0   'False
         Width           =   255
      End
      Begin VB.HScrollBar hs 
         Height          =   255
         LargeChange     =   5
         Left            =   0
         Max             =   1000
         TabIndex        =   8
         Top             =   4590
         Value           =   1
         Visible         =   0   'False
         Width           =   3575
      End
      Begin SHDocVwCtl.WebBrowser browser 
         Height          =   2835
         Left            =   0
         TabIndex        =   35
         Top             =   0
         Width           =   3135
         ExtentX         =   5530
         ExtentY         =   5001
         ViewMode        =   1
         Offline         =   0
         Silent          =   0
         RegisterAsBrowser=   0
         RegisterAsDropTarget=   0
         AutoArrange     =   0   'False
         NoClientEdge    =   0   'False
         AlignLeft       =   0   'False
         NoWebView       =   0   'False
         HideFileNames   =   0   'False
         SingleClick     =   0   'False
         SingleSelection =   0   'False
         NoFolders       =   0   'False
         Transparent     =   0   'False
         ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
         Location        =   "http:///"
      End
      Begin VB.Image img 
         Height          =   5250
         Left            =   720
         Stretch         =   -1  'True
         Top             =   0
         Visible         =   0   'False
         Width           =   4875
      End
      Begin VB.Label iv 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000040C0&
         Height          =   255
         Left            =   3960
         TabIndex        =   11
         Top             =   4560
         Visible         =   0   'False
         Width           =   255
      End
      Begin MediaPlayerCtl.MediaPlayer ani 
         Height          =   3765
         Left            =   210
         TabIndex        =   10
         Tag             =   "1080"
         Top             =   0
         Visible         =   0   'False
         Width           =   4290
         AudioStream     =   -1
         AutoSize        =   -1  'True
         AutoStart       =   -1  'True
         AnimationAtStart=   0   'False
         AllowScan       =   -1  'True
         AllowChangeDisplaySize=   -1  'True
         AutoRewind      =   -1  'True
         Balance         =   0
         BaseURL         =   ""
         BufferingTime   =   5
         CaptioningID    =   ""
         ClickToPlay     =   -1  'True
         CursorType      =   0
         CurrentPosition =   -1
         CurrentMarker   =   0
         DefaultFrame    =   ""
         DisplayBackColor=   0
         DisplayForeColor=   16777215
         DisplayMode     =   0
         DisplaySize     =   0
         Enabled         =   -1  'True
         EnableContextMenu=   -1  'True
         EnablePositionControls=   -1  'True
         EnableFullScreenControls=   -1  'True
         EnableTracker   =   -1  'True
         Filename        =   ""
         InvokeURLs      =   -1  'True
         Language        =   -1
         Mute            =   0   'False
         PlayCount       =   0
         PreviewMode     =   0   'False
         Rate            =   1
         SAMILang        =   ""
         SAMIStyle       =   ""
         SAMIFileName    =   ""
         SelectionStart  =   -1
         SelectionEnd    =   -1
         SendOpenStateChangeEvents=   0   'False
         SendWarningEvents=   0   'False
         SendErrorEvents =   0   'False
         SendKeyboardEvents=   -1  'True
         SendMouseClickEvents=   0   'False
         SendMouseMoveEvents=   0   'False
         SendPlayStateChangeEvents=   -1  'True
         ShowCaptioning  =   0   'False
         ShowControls    =   -1  'True
         ShowAudioControls=   -1  'True
         ShowDisplay     =   0   'False
         ShowGotoBar     =   0   'False
         ShowPositionControls=   -1  'True
         ShowStatusBar   =   -1  'True
         ShowTracker     =   -1  'True
         TransparentAtStart=   -1  'True
         VideoBorderWidth=   0
         VideoBorderColor=   16744703
         VideoBorder3D   =   0   'False
         Volume          =   0
         WindowlessVideo =   0   'False
      End
   End
   Begin VB.Timer tmrTransient 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   5040
      Top             =   3240
   End
   Begin VB.PictureBox conNavigator 
      BorderStyle     =   0  'None
      Height          =   7575
      Left            =   0
      ScaleHeight     =   7575
      ScaleWidth      =   4785
      TabIndex        =   3
      TabStop         =   0   'False
      Top             =   1920
      Visible         =   0   'False
      Width           =   4785
      Begin MSComctlLib.StatusBar stbr 
         Height          =   255
         Left            =   0
         TabIndex        =   6
         Top             =   5760
         Width           =   2655
         _ExtentX        =   4683
         _ExtentY        =   450
         _Version        =   393216
         BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
            NumPanels       =   2
            BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
               AutoSize        =   1
               Object.Width           =   2046
               MinWidth        =   882
            EndProperty
            BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
               AutoSize        =   1
               Object.Width           =   2575
               MinWidth        =   1411
            EndProperty
         EndProperty
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.PictureBox conSearch 
         BorderStyle     =   0  'None
         Height          =   2415
         Left            =   0
         ScaleHeight     =   2415
         ScaleWidth      =   2895
         TabIndex        =   22
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         Width           =   2895
         Begin VB.ComboBox cmbSearch 
            Height          =   315
            Left            =   720
            TabIndex        =   33
            Text            =   "Combo1"
            Top             =   0
            Width           =   1455
         End
         Begin VB.ComboBox cmbSrchSize 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   660
            Style           =   2  'Dropdown List
            TabIndex        =   26
            Top             =   735
            Width           =   1260
         End
         Begin VB.CheckBox chkSrchSize 
            Caption         =   "Size"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   255
            Left            =   60
            TabIndex        =   25
            Top             =   788
            Width           =   735
         End
         Begin VB.TextBox txtSrchSize 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1920
            TabIndex        =   27
            Top             =   735
            Width           =   375
         End
         Begin VB.ComboBox cmbSrchUnit 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   2280
            Style           =   2  'Dropdown List
            TabIndex        =   28
            Top             =   735
            Width           =   615
         End
         Begin VB.CommandButton cmdSearchGo 
            Caption         =   "Search"
            Default         =   -1  'True
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Left            =   2160
            TabIndex        =   23
            Top             =   5
            Width           =   735
         End
         Begin MSComctlLib.ListView lvw 
            Height          =   1245
            Index           =   3
            Left            =   0
            TabIndex        =   29
            Top             =   1080
            Width           =   2895
            _ExtentX        =   5106
            _ExtentY        =   2196
            View            =   3
            Arrange         =   2
            LabelEdit       =   1
            MultiSelect     =   -1  'True
            LabelWrap       =   -1  'True
            HideSelection   =   0   'False
            AllowReorder    =   -1  'True
            PictureAlignment=   5
            _Version        =   393217
            Icons           =   "icoLarge"
            SmallIcons      =   "icoSmall"
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            Appearance      =   1
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            NumItems        =   7
            BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               Key             =   "name"
               Text            =   "File Name"
               Object.Width           =   3351
            EndProperty
            BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   1
               Key             =   "size"
               Object.Tag             =   "bytes"
               Text            =   "Size"
               Object.Width           =   1764
            EndProperty
            BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   2
               Key             =   "type"
               Text            =   "Type"
               Object.Width           =   1058
            EndProperty
            BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   3
               Text            =   "Date Modified"
               Object.Width           =   4586
            EndProperty
            BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   4
               Text            =   "Time"
               Object.Width           =   2469
            EndProperty
            BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   5
               Text            =   "In Folder"
               Object.Width           =   3881
            EndProperty
            BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
               SubItemIndex    =   6
               Text            =   "Bytes"
               Object.Width           =   0
            EndProperty
         End
         Begin MSComctlLib.ImageCombo cmbSearchIn 
            Height          =   330
            Left            =   240
            TabIndex        =   24
            Top             =   360
            Width           =   1575
            _ExtentX        =   2778
            _ExtentY        =   582
            _Version        =   393216
            ForeColor       =   -2147483640
            BackColor       =   -2147483643
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Text            =   "ImageCombo1"
            ImageList       =   "imTvw"
         End
         Begin VB.Label Label1 
            AutoSize        =   -1  'True
            Caption         =   "Look for:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   60
            TabIndex        =   31
            Top             =   60
            Width           =   645
         End
         Begin VB.Label Label2 
            AutoSize        =   -1  'True
            Caption         =   "in:"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   195
            Left            =   60
            TabIndex        =   30
            Top             =   480
            Width           =   180
         End
      End
      Begin VB.PictureBox vvlin 
         AutoRedraw      =   -1  'True
         BorderStyle     =   0  'None
         DrawStyle       =   5  'Transparent
         FillColor       =   &H00008000&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   178
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5025
         Left            =   3120
         MousePointer    =   9  'Size W E
         ScaleHeight     =   5025
         ScaleWidth      =   60
         TabIndex        =   14
         TabStop         =   0   'False
         Top             =   0
         Visible         =   0   'False
         Width           =   60
      End
      Begin MSComctlLib.ListView lvw 
         Height          =   1905
         Index           =   2
         Left            =   0
         TabIndex        =   0
         Top             =   0
         Visible         =   0   'False
         Width           =   3420
         _ExtentX        =   6033
         _ExtentY        =   3360
         SortKey         =   3
         View            =   3
         Arrange         =   2
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         OLEDropMode     =   1
         AllowReorder    =   -1  'True
         FullRowSelect   =   -1  'True
         PictureAlignment=   5
         _Version        =   393217
         Icons           =   "icoLarge"
         SmallIcons      =   "icoSmall"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         OLEDropMode     =   1
         NumItems        =   7
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Key             =   "nam"
            Text            =   "File Name"
            Object.Width           =   2822
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Key             =   "siz"
            Text            =   "Size"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Key             =   "ext"
            Text            =   "Type"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Date Modified"
            Object.Width           =   4586
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Time"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Key             =   "loc"
            Text            =   "In Folder"
            Object.Width           =   3881
         EndProperty
         BeginProperty ColumnHeader(7) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   6
            Text            =   "Bytes"
            Object.Width           =   0
         EndProperty
      End
      Begin MSComctlLib.ImageList icoSmall 
         Left            =   3720
         Top             =   1800
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   2
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "video.frx":0954
               Key             =   "IVYnotfile"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "video.frx":0D08
               Key             =   "IVYnofile"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.ImageList icoLarge 
         Left            =   3720
         Top             =   1200
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   32
         ImageHeight     =   32
         MaskColor       =   12632256
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   2
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "video.frx":10B0
               Key             =   "IVYnotfile"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "video.frx":1534
               Key             =   "IVYnofile"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.ImageList imTvw 
         Left            =   4200
         Top             =   360
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   20
         ImageHeight     =   20
         MaskColor       =   16777215
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   12
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "video.frx":1998
               Key             =   ""
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "video.frx":1DEC
               Key             =   ""
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "video.frx":2240
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "video.frx":2694
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "video.frx":2AE8
               Key             =   "current"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "video.frx":2E9C
               Key             =   "folder"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "video.frx":3250
               Key             =   ""
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "video.frx":36A4
               Key             =   "miss"
            EndProperty
            BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "video.frx":3A6C
               Key             =   ""
            EndProperty
            BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "video.frx":46C0
               Key             =   ""
            EndProperty
            BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "video.frx":4A78
               Key             =   ""
            EndProperty
            BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "video.frx":4E34
               Key             =   ""
            EndProperty
         EndProperty
      End
      Begin VB.PictureBox hlin 
         BorderStyle     =   0  'None
         DrawStyle       =   5  'Transparent
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   178
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   150
         Left            =   0
         MousePointer    =   7  'Size N S
         ScaleHeight     =   150
         ScaleWidth      =   3645
         TabIndex        =   15
         TabStop         =   0   'False
         Top             =   2400
         Width           =   3645
         Begin VB.Label hd 
            Alignment       =   2  'Center
            BackColor       =   &H80000010&
            Caption         =   "u"
            BeginProperty Font 
               Name            =   "Marlett"
               Size            =   8.25
               Charset         =   2
               Weight          =   500
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   150
            Left            =   2880
            MousePointer    =   1  'Arrow
            TabIndex        =   17
            ToolTipText     =   "Roll Down"
            Top             =   0
            Width           =   735
         End
         Begin VB.Label hu 
            Alignment       =   2  'Center
            BackColor       =   &H80000010&
            Caption         =   "5"
            BeginProperty Font 
               Name            =   "Marlett"
               Size            =   8.25
               Charset         =   2
               Weight          =   500
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            ForeColor       =   &H00FFFFFF&
            Height          =   150
            Left            =   0
            MousePointer    =   1  'Arrow
            TabIndex        =   16
            ToolTipText     =   "Roll Up"
            Top             =   0
            Width           =   735
         End
      End
      Begin VB.ComboBox cmbFilter 
         Height          =   315
         Left            =   0
         TabIndex        =   20
         Text            =   "Combo1"
         Top             =   2760
         Width           =   3855
      End
      Begin MSComctlLib.ListView lvw 
         Height          =   1485
         Index           =   1
         Left            =   0
         TabIndex        =   21
         Top             =   3240
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   2619
         View            =   3
         Arrange         =   2
         LabelEdit       =   1
         Sorted          =   -1  'True
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   0   'False
         OLEDropMode     =   1
         AllowReorder    =   -1  'True
         PictureAlignment=   5
         _Version        =   393217
         Icons           =   "icoLarge"
         SmallIcons      =   "icoSmall"
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         OLEDropMode     =   1
         NumItems        =   6
         BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            Key             =   "name"
            Text            =   "File Name"
            Object.Width           =   3528
         EndProperty
         BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   1
            Key             =   "size"
            Object.Tag             =   "bytes"
            Text            =   "Size"
            Object.Width           =   1764
         EndProperty
         BeginProperty ColumnHeader(3) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   2
            Key             =   "type"
            Text            =   "Type"
            Object.Width           =   1058
         EndProperty
         BeginProperty ColumnHeader(4) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   3
            Text            =   "Date Modified"
            Object.Width           =   4586
         EndProperty
         BeginProperty ColumnHeader(5) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   4
            Text            =   "Time"
            Object.Width           =   2469
         EndProperty
         BeginProperty ColumnHeader(6) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
            SubItemIndex    =   5
            Text            =   "Bytes"
            Object.Width           =   0
         EndProperty
      End
      Begin MSComctlLib.TreeView tvw 
         Height          =   1695
         Left            =   0
         TabIndex        =   19
         Top             =   390
         Width           =   3615
         _ExtentX        =   6376
         _ExtentY        =   2990
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   450
         LabelEdit       =   1
         LineStyle       =   1
         Sorted          =   -1  'True
         Style           =   7
         FullRowSelect   =   -1  'True
         ImageList       =   "imTvw"
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSComctlLib.ImageCombo cmbHistory 
         Height          =   375
         Left            =   0
         TabIndex        =   18
         Top             =   0
         Width           =   4335
         _ExtentX        =   7646
         _ExtentY        =   661
         _Version        =   393216
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ImageList       =   "imTvw"
      End
   End
   Begin VB.ListBox lstSort 
      Height          =   255
      Left            =   4440
      Sorted          =   -1  'True
      TabIndex        =   32
      TabStop         =   0   'False
      Top             =   600
      Visible         =   0   'False
      Width           =   1095
   End
   Begin MSComctlLib.Toolbar TBar 
      Align           =   1  'Align Top
      Height          =   330
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Visible         =   0   'False
      Width           =   8880
      _ExtentX        =   15663
      _ExtentY        =   582
      ButtonWidth     =   609
      ButtonHeight    =   582
      AllowCustomize  =   0   'False
      Wrappable       =   0   'False
      Style           =   1
      _Version        =   393216
      BeginProperty Buttons {66833FE8-8583-11D1-B16A-00C0F0283628} 
         NumButtons      =   20
         BeginProperty Button1 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "explore"
            Object.ToolTipText     =   "Explorer Pane"
            Object.Tag             =   "Folders"
            Style           =   1
            Value           =   1
         EndProperty
         BeginProperty Button2 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "play"
            Object.ToolTipText     =   "Playlist Pane"
            Object.Tag             =   "Playlist"
            Style           =   1
         EndProperty
         BeginProperty Button3 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "search"
            Object.ToolTipText     =   "Search Pane"
            Object.Tag             =   "Search"
            Style           =   1
         EndProperty
         BeginProperty Button4 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button5 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "up"
            Object.ToolTipText     =   "Open Parent Folder"
            Object.Tag             =   "Up"
         EndProperty
         BeginProperty Button6 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "open"
            Object.ToolTipText     =   "Open/Add file(s)"
            Object.Tag             =   "Open"
         EndProperty
         BeginProperty Button7 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "list"
            Object.ToolTipText     =   "Open Playlist"
            Object.Tag             =   "Load"
         EndProperty
         BeginProperty Button8 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "save"
            Object.ToolTipText     =   "Save Playlist"
            Object.Tag             =   "Save"
         EndProperty
         BeginProperty Button9 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button10 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "goh"
            Object.ToolTipText     =   "Toggle Horizontal View"
            Object.Tag             =   "Split"
         EndProperty
         BeginProperty Button11 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button12 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "next"
            Object.ToolTipText     =   "View/Play Next File"
            Object.Tag             =   "Next"
         EndProperty
         BeginProperty Button13 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "slides"
            Object.ToolTipText     =   "View Slide Show"
            Object.Tag             =   "Slide"
            Style           =   1
         EndProperty
         BeginProperty Button14 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Key             =   "image"
            Object.ToolTipText     =   "Image Size"
            Object.Tag             =   "Image"
            Style           =   5
         EndProperty
         BeginProperty Button15 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Key             =   "dec"
            Object.ToolTipText     =   "Decrease Size"
            Object.Tag             =   "Size -"
         EndProperty
         BeginProperty Button16 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Key             =   "inc"
            Object.ToolTipText     =   "Increase Size"
            Object.Tag             =   "Size +"
         EndProperty
         BeginProperty Button17 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Key             =   "reset"
            Object.ToolTipText     =   "Reset Size"
            Object.Tag             =   "Reset"
         EndProperty
         BeginProperty Button18 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Object.Visible         =   0   'False
            Key             =   "repeat"
            Object.ToolTipText     =   "Keep Repeating Media"
            Object.Tag             =   "Loop"
            Style           =   1
         EndProperty
         BeginProperty Button19 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Style           =   3
         EndProperty
         BeginProperty Button20 {66833FEA-8583-11D1-B16A-00C0F0283628} 
            Key             =   "icon"
            Object.ToolTipText     =   "Icon Views"
            Object.Tag             =   "Views"
            Style           =   5
         EndProperty
      EndProperty
   End
   Begin MSComctlLib.ImageList imTBar 
      Left            =   3720
      Top             =   840
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483633
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   16777215
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   20
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":5308
            Key             =   "image"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":5784
            Key             =   "slides"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":5D50
            Key             =   "explore"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":6304
            Key             =   "play"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":67AC
            Key             =   "list"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":6C00
            Key             =   "search"
         EndProperty
         BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":7038
            Key             =   "gov"
         EndProperty
         BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":74D0
            Key             =   "icon"
         EndProperty
         BeginProperty ListImage9 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":7DAC
            Key             =   "goh"
         EndProperty
         BeginProperty ListImage10 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":8200
            Key             =   "open"
         EndProperty
         BeginProperty ListImage11 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":86D4
            Key             =   "save"
         EndProperty
         BeginProperty ListImage12 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":8AE8
            Key             =   "up"
         EndProperty
         BeginProperty ListImage13 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":8F84
            Key             =   "dec"
         EndProperty
         BeginProperty ListImage14 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":9380
            Key             =   "repeat"
         EndProperty
         BeginProperty ListImage15 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":97A4
            Key             =   "inc"
         EndProperty
         BeginProperty ListImage16 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":9BA0
            Key             =   "reset"
         EndProperty
         BeginProperty ListImage17 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":9FB4
            Key             =   "incrate"
         EndProperty
         BeginProperty ListImage18 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":A390
            Key             =   "decrate"
         EndProperty
         BeginProperty ListImage19 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":A768
            Key             =   "next"
         EndProperty
         BeginProperty ListImage20 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "video.frx":AB80
            Key             =   "resetrate"
         EndProperty
      EndProperty
   End
   Begin VB.Timer Tmr 
      Enabled         =   0   'False
      Interval        =   1000
      Left            =   5040
      Top             =   2640
   End
   Begin VB.FileListBox fList 
      Height          =   1845
      Left            =   0
      TabIndex        =   1
      TabStop         =   0   'False
      Top             =   360
      Visible         =   0   'False
      Width           =   3330
   End
   Begin VB.Menu menuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFileOpen 
         Caption         =   "&Open/Add file(s)..."
         Shortcut        =   ^O
      End
      Begin VB.Menu sf0 
         Caption         =   "-"
      End
      Begin VB.Menu mnuPlaylistNew 
         Caption         =   "&New Playlist"
         Shortcut        =   ^N
      End
      Begin VB.Menu mnuPlaylistLoad 
         Caption         =   "&Load Playlist..."
         Shortcut        =   ^L
      End
      Begin VB.Menu mnuPlaylistSave 
         Caption         =   "&Save List..."
         Shortcut        =   ^S
      End
      Begin VB.Menu sf2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFileLaunch 
         Caption         =   "Launch F&ile..."
      End
      Begin VB.Menu mnuFileNotepad 
         Caption         =   "Open With Not&epad..."
      End
      Begin VB.Menu mnuFileExplore 
         Caption         =   "Explore &Folder..."
      End
      Begin VB.Menu sf3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuExit 
         Caption         =   "E&xit"
         Shortcut        =   ^Q
      End
   End
   Begin VB.Menu menuView 
      Caption         =   "&View"
      Begin VB.Menu mnuClear 
         Caption         =   "&Clear Screen"
         Shortcut        =   {F3}
      End
      Begin VB.Menu mnuCall 
         Caption         =   "C&ontext Menu"
         Shortcut        =   {F4}
      End
      Begin VB.Menu sv1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuView 
         Caption         =   "&Explorer"
         Index           =   1
         Shortcut        =   ^{F1}
      End
      Begin VB.Menu mnuView 
         Caption         =   "&Playlist"
         Index           =   2
         Shortcut        =   ^{F2}
      End
      Begin VB.Menu mnuView 
         Caption         =   "&Search"
         Index           =   3
         Shortcut        =   ^{F3}
      End
      Begin VB.Menu sv2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuImage 
         Caption         =   "&Image"
         Tag             =   "4"
         Begin VB.Menu mnuFullScreen 
            Caption         =   "&Full Screen"
            Shortcut        =   {F11}
         End
         Begin VB.Menu si1 
            Caption         =   "-"
         End
         Begin VB.Menu mnuImg 
            Caption         =   "&Variable"
            Index           =   1
         End
         Begin VB.Menu mnuImg 
            Caption         =   "Fit &Height"
            Index           =   2
         End
         Begin VB.Menu mnuImg 
            Caption         =   "Fit &Width"
            Index           =   3
         End
         Begin VB.Menu mnuImg 
            Caption         =   "&Fit Both"
            Checked         =   -1  'True
            Index           =   4
         End
         Begin VB.Menu mnuImg 
            Caption         =   "&Stretch Fit"
            Index           =   5
         End
         Begin VB.Menu mnuImg 
            Caption         =   "&Tiled"
            Index           =   6
         End
         Begin VB.Menu mnuImg 
            Caption         =   "In &IE"
            Index           =   7
         End
      End
      Begin VB.Menu mnuMedia 
         Caption         =   "&Media"
         Begin VB.Menu mnuMedSize 
            Caption         =   "Si&ze"
            Begin VB.Menu mnuMed 
               Caption         =   "&Default Size"
               Index           =   0
            End
            Begin VB.Menu mnuMed 
               Caption         =   "&Half Size"
               Index           =   1
            End
            Begin VB.Menu mnuMed 
               Caption         =   "D&ouble Size"
               Index           =   2
            End
            Begin VB.Menu mnuMed 
               Caption         =   "&Full Screen"
               Index           =   3
            End
            Begin VB.Menu mnuMed 
               Caption         =   "Fit &to Size"
               Index           =   4
            End
            Begin VB.Menu mnuMed 
               Caption         =   "1/&16 Screen"
               Index           =   5
            End
            Begin VB.Menu mnuMed 
               Caption         =   "1/&4 Screen"
               Index           =   6
            End
            Begin VB.Menu mnuMed 
               Caption         =   "1/&2 Screen"
               Index           =   7
            End
         End
         Begin VB.Menu mnuMedMove 
            Caption         =   "Mo&ve"
            Begin VB.Menu mnuMedMoveBack 
               Caption         =   "&Back [Numpad 1]"
            End
            Begin VB.Menu mnuMedMoveFwd 
               Caption         =   "&Forward [Numpad 2]"
            End
            Begin VB.Menu mnuMedMoveTo 
               Caption         =   "&To [Numpad 0]"
            End
            Begin VB.Menu mnuMedMoveStep 
               Caption         =   "Set &Step [Numpad 3]"
            End
         End
         Begin VB.Menu svm 
            Caption         =   "-"
         End
         Begin VB.Menu mnuMediaVolInc 
            Caption         =   "&Increase Voume [+]"
         End
         Begin VB.Menu mnuMediaVolDec 
            Caption         =   "&Decrease Volume [-]"
         End
         Begin VB.Menu svm2 
            Caption         =   "-"
         End
         Begin VB.Menu mnuMediaReplay 
            Caption         =   "&Replay Media"
            Shortcut        =   ^R
         End
         Begin VB.Menu mnuMediaMute 
            Caption         =   "&Mute"
            Shortcut        =   ^M
         End
         Begin VB.Menu mnuMediaPause 
            Caption         =   "&Pause"
            Shortcut        =   ^P
         End
         Begin VB.Menu mnuMediaStop 
            Caption         =   "&Stop"
         End
         Begin VB.Menu mnuMediaRepeat 
            Caption         =   "&Loop"
         End
      End
      Begin VB.Menu mnuText 
         Caption         =   "Text"
         Begin VB.Menu mnuTextSearch 
            Caption         =   "&Find..."
            Shortcut        =   ^F
         End
         Begin VB.Menu st1 
            Caption         =   "-"
         End
         Begin VB.Menu mnuTxtSize 
            Caption         =   "Set &Font Size..."
            Tag             =   "Enter Size"
         End
      End
      Begin VB.Menu sv3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuViewSlideShow 
         Caption         =   "Slide Sho&w"
         Shortcut        =   ^W
      End
      Begin VB.Menu sv4 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHorz 
         Caption         =   "Split &Horizontally"
         Shortcut        =   ^H
      End
   End
   Begin VB.Menu mnuRZ 
      Caption         =   "&Rate"
      Begin VB.Menu mnuF6 
         Caption         =   "&Decrease"
         Shortcut        =   {F6}
      End
      Begin VB.Menu mnuF6c 
         Caption         =   "&Fine Decrease"
         Shortcut        =   ^{F6}
      End
      Begin VB.Menu mnuF7 
         Caption         =   "&Increase"
         Shortcut        =   {F7}
      End
      Begin VB.Menu mnuF7c 
         Caption         =   "&Fine Increase"
         Shortcut        =   ^{F7}
      End
      Begin VB.Menu mnuF8 
         Caption         =   "R&eset"
         Shortcut        =   {F8}
      End
   End
   Begin VB.Menu menuFileList 
      Caption         =   "File &List"
      NegotiatePosition=   2  'Middle
      Begin VB.Menu menuLvwView 
         Caption         =   "&View"
         Begin VB.Menu mnuLvwView 
            Caption         =   "Large &Icons"
            Index           =   0
            Shortcut        =   +{F1}
         End
         Begin VB.Menu mnuLvwView 
            Caption         =   "S&mall Icons"
            Index           =   1
            Shortcut        =   +{F2}
         End
         Begin VB.Menu mnuLvwView 
            Caption         =   "&List"
            Index           =   2
            Shortcut        =   +{F3}
         End
         Begin VB.Menu mnuLvwView 
            Caption         =   "&Details"
            Checked         =   -1  'True
            Index           =   3
            Shortcut        =   +{F4}
         End
      End
      Begin VB.Menu menuLvwArrange 
         Caption         =   "Arrange &Icons"
         Begin VB.Menu mnuLvwShuffle 
            Caption         =   "Shuffle"
         End
         Begin VB.Menu sai 
            Caption         =   "-"
         End
         Begin VB.Menu mnuLvwArrange 
            Caption         =   "by Name"
            Index           =   1
         End
         Begin VB.Menu mnuLvwArrange 
            Caption         =   "by Size"
            Index           =   2
         End
         Begin VB.Menu mnuLvwArrange 
            Caption         =   "by Type"
            Index           =   3
         End
         Begin VB.Menu mnuLvwArrange 
            Caption         =   "by Folder"
            Index           =   4
         End
         Begin VB.Menu mnuLvwArrange 
            Caption         =   "by Time"
            Index           =   5
         End
         Begin VB.Menu mnuLvwArrange 
            Caption         =   ""
            Index           =   6
         End
      End
      Begin VB.Menu menuLvwColumn 
         Caption         =   "Column &Headers"
         Begin VB.Menu mnuLvwColumn 
            Caption         =   "File Name"
            Checked         =   -1  'True
            Index           =   1
         End
         Begin VB.Menu mnuLvwColumn 
            Caption         =   "Size"
            Checked         =   -1  'True
            Index           =   2
         End
         Begin VB.Menu mnuLvwColumn 
            Caption         =   "Extension"
            Checked         =   -1  'True
            Index           =   3
         End
         Begin VB.Menu mnuLvwColumn 
            Caption         =   "Location"
            Checked         =   -1  'True
            Index           =   4
         End
         Begin VB.Menu mnuLvwColumn 
            Caption         =   "Time"
            Checked         =   -1  'True
            Index           =   5
         End
         Begin VB.Menu mnuLvwColumn 
            Caption         =   ""
            Index           =   6
         End
      End
      Begin VB.Menu sl1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuListAdd 
         Caption         =   "&Add File(s) to Playlist [Ins]"
      End
      Begin VB.Menu mnuListRename 
         Caption         =   "&Rename [F2]"
      End
      Begin VB.Menu mnuListDelete 
         Caption         =   "&Delete file(s) [Del]"
      End
      Begin VB.Menu mnuPlaylistMoveUp 
         Caption         =   "Move &Up [Shft + PgUp]"
      End
      Begin VB.Menu mnuPlaylistMoveDown 
         Caption         =   "Move &Down [Shft + PgDown]"
      End
      Begin VB.Menu mnuPlaylistRemove 
         Caption         =   "Remove &Selected [Del]"
      End
      Begin VB.Menu mnuPlaylistCrop 
         Caption         =   "Remove U&nselected [Shft + Del]"
      End
      Begin VB.Menu mnuPlaylistRemoveMissing 
         Caption         =   "Remove &Missing Files"
      End
      Begin VB.Menu sl2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuListPlayRev 
         Caption         =   "Play in Re&verse"
         Shortcut        =   ^V
      End
      Begin VB.Menu mnuListPlayNext 
         Caption         =   "Play Selected File(s) &Next"
      End
      Begin VB.Menu mnuListReturn 
         Caption         =   "Return Selection to &Current File"
      End
      Begin VB.Menu mnuJump 
         Caption         =   "&Jump to File..."
         Shortcut        =   ^J
      End
      Begin VB.Menu mnuJumpFolder 
         Caption         =   "Jump to &Folder..."
      End
      Begin VB.Menu mnuNext 
         Caption         =   "Goto Nex&t File"
         Shortcut        =   ^T
      End
      Begin VB.Menu mnuShuffle 
         Caption         =   "&Shuffle and Play / View"
      End
   End
   Begin VB.Menu mnuFavorites 
      Caption         =   "F&avorites"
      Begin VB.Menu mnuFaveNode 
         Caption         =   "Add Folder as Explorer Node"
      End
      Begin VB.Menu mnuFaveFolder 
         Caption         =   "Add Folder to favorites"
      End
      Begin VB.Menu mnuFavePlaylist 
         Caption         =   "Add Playlist to Favorites"
      End
      Begin VB.Menu mnuFaveOrganize 
         Caption         =   "Organize Favorites..."
      End
      Begin VB.Menu sh1 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFaveF 
         Caption         =   ""
         Index           =   0
         Visible         =   0   'False
      End
      Begin VB.Menu sh2 
         Caption         =   "-"
         Visible         =   0   'False
      End
      Begin VB.Menu mnuFaveP 
         Caption         =   ""
         Index           =   0
         Visible         =   0   'False
      End
   End
   Begin VB.Menu mnuOptions 
      Caption         =   "&Options"
      Begin VB.Menu mnuRefresh 
         Caption         =   "&Refresh"
         Shortcut        =   {F5}
      End
      Begin VB.Menu mnuOptionsClipboard 
         Caption         =   "Copy Files to &Clipboard"
      End
      Begin VB.Menu mnuOptionsGenerate 
         Caption         =   "&Generate Playlist"
      End
      Begin VB.Menu so1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuOptionsRunLyriks 
         Caption         =   "Run &Lyriks..."
      End
      Begin VB.Menu mnuOptionsShowLyrics 
         Caption         =   "S&how Lyrics"
      End
      Begin VB.Menu mnuOptionsLyricsTrain 
         Caption         =   "&Train Lyrics"
      End
      Begin VB.Menu mnuOptionsLyricsMatch 
         Caption         =   "Match L&yrics"
         Enabled         =   0   'False
         Shortcut        =   ^Y
      End
      Begin VB.Menu so2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuStatusBar 
         Caption         =   "View Status &Bar"
         Checked         =   -1  'True
      End
      Begin VB.Menu mnuToolbarView 
         Caption         =   "View &Toolbar"
      End
      Begin VB.Menu so3 
         Caption         =   "-"
      End
      Begin VB.Menu mnuOptionsTLib 
         Caption         =   "Clean &up Library..."
      End
      Begin VB.Menu mnuOptExtns 
         Caption         =   "Set File &Associations..."
      End
      Begin VB.Menu mnuSettings 
         Caption         =   "&Settings..."
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "&Help"
      Begin VB.Menu mnuHelpTips 
         Caption         =   "Program T&ips..."
         Shortcut        =   ^I
      End
      Begin VB.Menu sh 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelpAbout 
         Caption         =   "&About"
         Shortcut        =   {F1}
      End
   End
End
Attribute VB_Name = "Vdplayer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

'for img zoom & speed
Dim z As Integer, y As Integer, zMax As Integer 'y =magn, z =ani.rate

Public CurFile As String 'a =path of current file

Public timStart As Date 'For life timer

Dim strSearch As String, lSrchSiz As Long, extnn As String, bolDoSearch As Boolean, _
  bolSearchExt As Boolean, bolSearchAll As Boolean, iSearchSize As Integer  'in search procedures

'pane sizing Booleans
Dim hMoving As Boolean, vMoving As Boolean, _
  hhMoving As Boolean, vvMoving As Boolean

Dim bolHasMedia(3) As Boolean
'Dim isSelecting As Boolean, isShift As Boolean

Dim mesg As String 'replacing sub main
Dim ind As Byte, IndView As Byte
Dim RollStatus As Integer, strList As String
Dim MStep As Integer

Private fldr As Folder, fil As File, filCom As File, ts As TextStream

Private itm As ListItem

'publically declare all variables reqd for options dialog
'or make do with registry setings if necessary
Public viTitle As Integer 'How to display app title
Public viViewHistoryByTime As Boolean
Public viShowHtmlAsText As Boolean 'used in extens.QueryType
Public viLvwExtension As Boolean
Public viDefExtn As Integer '-1 = prompt, 0 = same as previous
Public viReorderAsShuffle As Boolean
Public viChangeHorz As Boolean
Public viShowTime As Boolean
Public viTrashLoc As String
Public viTrashOpt As Integer '0=NO, 1=Delete, 2=Recycle Bin, 3=Our Trash Can

'icon loading declares
Private Const BASIC_SHGFI_FLAGS = &H400 _
   Or &H4 Or &H4000 _
   Or &H200 Or &H2000

Private Type SHFILEINFO
    hIcon As Long
    iIcon As Long
    dwAttributes As Long
    szDisplayName As String * 260
    szTypeName As String * 80
End Type

Private Declare Function SHGetFileInfo Lib "Shell32.dll" Alias "SHGetFileInfoA" _
   (ByVal pszPath As String, _
    ByVal dwFileAttributes As Long, _
    psfi As SHFILEINFO, _
    ByVal cbSizeFileInfo As Long, _
    ByVal uFlags As Long) As Long

Private Declare Function ImageList_Draw Lib "comctl32.dll" _
   (ByVal himl&, ByVal i&, ByVal hDCDest&, _
    ByVal x&, ByVal y&, ByVal Flags&) As Long

Private shinfo As SHFILEINFO

Private Sub ani_Center()
'anis min height = 1080. without even the status bar at tis bottom its height is 690 (imagesourceheight=0)
If ani.DisplaySize = mpFullScreen Then
  Exit Sub
ElseIf ani.DisplaySize = mpFitToSize Then
  ani.Left = 0: ani.Width = conViewer.ScaleWidth
  ani.Top = 0
  If ani.ImageSourceHeight = 0 Then
    If ani.FileName = "" Then
      ani.Height = 690
    Else
      ani.Height = 1080
      'ani.Top = conViewer.Height - ani.Height
    End If
  Else
    ani.Height = conViewer.ScaleHeight
  End If
Else
  If ani.Width < conViewer.Width Then ani.Left = (conViewer.Width - ani.Width) / 2 Else ani.Left = 0
  If ani.ImageSourceHeight = 0 Then
    'must check the line immediately after this commented line
    If ani.Height > conViewer.Height Then
      ani.Top = 0
    Else
      If ani.Height < 1080 Then
        ani.Top = (conViewer.ScaleHeight - 1080) / 2
      Else
        ani.Top = (conViewer.ScaleHeight - ani.Height) / 2
      End If
    End If
  Else
    If ani.Height + ani.ImageSourceHeight < conViewer.Height Then ani.Top = (conViewer.Height - 1080) / 2 Else ani.Top = 0
  End If
End If
End Sub

Private Sub ani_EndOfStream(ByVal result As Long)
If ind <> 3 Then ani.Stop: Exit Sub
If mnuMediaRepeat.Checked = False Then CrlNext Else ani.Play
End Sub

Private Sub ani_NewStream()
If mnuOptionsShowLyrics.Checked Then
  Static cLyrIni As New cInifile
  If cLyrIni.Path = "" Then cLyrIni.Path = apPath & "Lyrics Library.ini": cLyrIni.Section = "Lyrics Library"
    'GetSetting(apComp, "Lyriks", "Lyrics Library File", "")
  cLyrIni.Default = ""
  cLyrIni.Key = CurFile
  If cLyrIni.Value = "" Then 'hide rtb if not hidden
    If rtb.Visible Then
      rtb.Visible = False
      rtb.Tag = ""
      rtb.Top = 0
      rtb.Height = conViewer.Height
    End If
  Else 'make rtb visible if hidden
    If mnuHorz.Checked = True And conViewer.Height - ani.Height < 200 Then mnuHorz_Click
    If rtb.Visible = False Then
      rtb.Text = ""
      rtb.Top = ani.Height
      rtb.Height = conViewer.Height - rtb.Top
      rtb.Visible = True
    End If
    loadSong cLyrIni.Value
  End If
End If
If mnuMedia.Tag = mpFullScreen And ani.ImageSourceHeight = 0 Then ani.DisplaySize = mpFitToSize Else ani.DisplaySize = mnuMedia.Tag
tmrAni.Enabled = True
ani.Visible = True
End Sub

'Private Sub ani_ScriptCommand(ByVal scType As String, ByVal Param As String)
'If fso.FileExists("MediaScripts.log") = False Then fso.CreateTextFile "MediaScripts.log"
'Set ts = fso.OpenTextFile("MediaScripts.log", ForAppending, True)
'ts.WriteLine Now & Chr(9) & scType & Chr(9) & Param
'ts.Close
  'Debug.Print Now, Not ani.InvokeURLs, scType, Param
'End Sub

Private Sub loadSong(strSong As String)
'consider doing this in the web browser control
Dim ts As TextStream, sng As String, a As String, sFile As String

If InStr(strSong, "#") = 0 Or rtb.Tag = strSong Then Exit Sub


sFile = (Mid(strSong, 1, InStr(strSong, "#") - 1))
If fso.FileExists(sFile) = False Then
  'rtb.TextRTF = ""
  rtb.Text = "The File " & sFile & " is no longer available"
  Exit Sub
End If

Set ts = fso.OpenTextFile(sFile, ForReading)

sng = "<title>" & Mid(strSong, 1 + InStr(strSong, "#")) & "</title>"

'lvw.ListItems.Clear
'rtb.TextRTF = ""


rtb.Text = "Lyric File: " & Mid(strSong, 1, InStr(strSong, "#") - 1)

Do
  If ts.AtEndOfStream Then MsgBox "Song not found": Exit Sub
  a = ts.ReadLine
  If Mid(a, 1, 8) = "<source>" Then
    rtb.Text = rtb.Text & vbCrLf & "Source: " & Mid(a, 9, Len(a) - 17) '& Chr(9) & Chr(9)
  ElseIf Mid(a, 1, 8) = "<artist>" Then
    rtb.Text = rtb.Text & vbCrLf & "Artist: " & Mid(a, 9, Len(a) - 17)
  ElseIf Mid(a, 1, 7) = "<album>" Then
    rtb.Text = rtb.Text & vbCrLf & "Album: " & Mid(a, 8, Len(a) - 15)
  ElseIf Mid(a, 1, 7) = "<addnl>" Then
    rtb.Text = rtb.Text & vbCrLf & "Addnl: " & Mid(a, 8, Len(a) - 15)
  ElseIf Mid(a, 1, 8) = "<agenre>" Then
    rtb.Text = rtb.Text & vbCrLf & "Album Genre: " & Mid(a, 9, Len(a) - 17)
  End If
  
Loop Until a = sng

'rtb.Text = rtb.Text & vbCrLf

a = ts.ReadLine
sng = ""
Do Until a = "<song>"

  If Mid(a, 1, 8) = "<sgenre>" Then
    rtb.Text = rtb.Text & vbCrLf & "Song Genre: " & Mid(a, 9, Len(a) - 17)
  ElseIf Mid(a, 1, 7) = "<music>" Then
    rtb.Text = rtb.Text & vbCrLf & "Music By: " & Mid(a, 8, Len(a) - 15)
  ElseIf Mid(a, 1, 6) = "<misc>" Then
    rtb.Text = rtb.Text & vbCrLf & "Misc Info: " & Mid(a, 7, Len(a) - 13)
  ElseIf Mid(a, 1, 6) = "<file>" Then
    sFile = Mid(a, 7, Len(a) - 13)
    rtb.Text = rtb.Text & vbCrLf & "File: " & sFile
  End If

a = ts.ReadLine

Loop


ani.Tag = sFile

rtb.Text = rtb.Text & vbCrLf & vbCrLf & "Lyrics>" & vbCrLf & vbCrLf
a = ts.ReadLine
Do Until a = "</song>"
rtb.Text = rtb.Text & a & vbCrLf
a = ts.ReadLine
Loop

ts.Close

rtb.Tag = strSong

Exit Sub
Serr:
MsgBox Err.Description, vbCritical
End Sub

Private Sub ChangeMode()
'first clear all things
browser.Visible = False: rtb.Visible = False
ani.Visible = False: ani.Left = Me.ScaleWidth: ani.Top = Me.ScaleHeight
img.Visible = False
sv3.Visible = True: mnuRZ.Visible = False: mnuF6c.Visible = False: mnuF7c.Visible = False
  TBar.Buttons("inc").Visible = False: TBar.Buttons("dec").Visible = False: TBar.Buttons("reset").Visible = False
mnuImage.Visible = False: mnuMedia.Visible = False: mnuText.Visible = False 'make context menus diappear
mnuCall.Enabled = True: mnuFullScreen.Visible = False
TBar.Buttons("repeat").Visible = False: TBar.Buttons("image").Visible = False
mnuViewSlideShow.Enabled = True

Select Case ind

Case 1 'Html
browser.Visible = True:  mnuCall.Enabled = False: sv3.Visible = False
stbr.Panels(1) = ""

Case 2 'Image
mnuImage.Visible = True
img.Visible = True: mnuImage.Visible = True: mnuFullScreen.Visible = True
If mnuImage.Tag = 1 Then mnuRZ.Visible = True: mnuRZ.Caption = "&Magnification"
stbr.Panels(1) = "Zoom:"
TBar.Buttons("image").Visible = True
If CurFile <> "" Then CrlClick

Case 3 'Media
ani.Visible = True: ani_Center: mnuMedia.Visible = True: iv.Visible = False

mnuRZ.Visible = True: mnuRZ.Caption = "&Rate": mnuF6c.Visible = True: mnuF7c.Visible = True
stbr.Panels(1) = "Rate: " & ani.Rate
TBar.Buttons("repeat").Visible = True
TBar.Buttons("inc").Visible = True: TBar.Buttons("dec").Visible = True: TBar.Buttons("reset").Visible = True
  TBar.Buttons("inc").Tag = "Rate +": TBar.Buttons("inc").ToolTipText = "Increase Rate"
  TBar.Buttons("dec").Tag = "Rate -": TBar.Buttons("dec").ToolTipText = "Decrease Rate"
  TBar.Buttons("reset").Tag = "Rate 1.0": TBar.Buttons("reset").ToolTipText = "Reset Rate"
If TBar.Buttons(1).Caption <> "" Then
  TBar.Buttons("inc").Caption = "Rate +": TBar.Buttons("dec").Caption = "Rate -": TBar.Buttons("reset").Caption = "Rate 1.0"
End If
TBar.Buttons("inc").Image = imTBar.ListImages("incrate").Index
TBar.Buttons("dec").Image = imTBar.ListImages("decrate").Index
TBar.Buttons("reset").Image = imTBar.ListImages("resetrate").Index
mnuViewSlideShow.Enabled = False

Case 4 'Text
rtb.Top = 0: rtb.Height = conViewer.Height
rtb.Visible = True: mnuText.Visible = True
stbr.Panels(1) = ""
End Select

If mnuViewSlideShow.Enabled Then TBar.Buttons("slides").Visible = True Else TBar.Buttons("slides").Visible = False
End Sub

Private Sub conNavigator_Resiz()
If conNavigator.Height < 300 Or conNavigator.Width < 300 Then Exit Sub
On Error Resume Next
'width related stuff
stbr.Top = conNavigator.ScaleHeight - stbr.Height

  stbr.Width = conNavigator.Width

If mnuHorz.Checked = False Then
  cmbHistory.Width = conNavigator.ScaleWidth
  hlin.Width = conNavigator.ScaleWidth - 30
  cmbFilter.Width = hlin.ScaleWidth
  lvw(1).Width = conNavigator.ScaleWidth
  tvw.Width = conNavigator.ScaleWidth
  
  'height & other stuff
  If mnuView(2).Checked = False Then Hsiz:  Roll
Else
  If mnuStatusBar.Checked Then vvlin.Height = conNavigator.Height - stbr.Height Else vvlin.Height = conNavigator.Height
  vVsiz
End If

'size lst to match conNavigator
  lvw(2).Width = conNavigator.Width
  If mnuStatusBar.Checked Then lvw(2).Height = stbr.Top Else lvw(2).Height = conNavigator.Height

'resize search pane
conSearch.Height = lvw(2).Height
conSearch.Width = conNavigator.Width
conSearch_Resiz

'resizing columns
  lvwSizCol lvw(1), 1 'filelist
  lvwSizCol lvw(3), 6 'search
  lvwSizCol lvw(2), 6
End Sub

Public Sub lvwSizCol(sentLvw As ListView, colIndex As Integer)
Dim totColWid As Integer, col As ColumnHeader
totColWid = 0
For Each col In sentLvw.ColumnHeaders
  If col.Index <> colIndex Then totColWid = totColWid + col.Width
Next
If sentLvw.Width - totColWid > 360 Then _
  sentLvw.ColumnHeaders(colIndex).Width = sentLvw.Width - totColWid - 360
End Sub

Private Sub chkSrchSize_Click()
If chkSrchSize.Value = 1 Then
  cmbSrchSize.Enabled = True: txtSrchSize.Enabled = True: cmbSrchUnit.Enabled = True
Else
  cmbSrchSize.Enabled = False: txtSrchSize.Enabled = False: cmbSrchUnit.Enabled = False
End If
End Sub

Private Sub cmbFilter_Click()

If cmbFilter.ListIndex > extns.Count Then
  If InStr(cmbFilter.Text, "*") = 0 And InStr(cmbFilter.Text, "?") = 0 Then
    fList.Pattern = "*" & cmbFilter.Text & "*"
  Else
    fList.Pattern = cmbFilter.Text
  End If
Else
  cmbFilter.Tag = Mid(cmbFilter.Text, InStr(cmbFilter.Text, "(") + 1)
  fList.Pattern = Mid(cmbFilter.Tag, 1, Len(cmbFilter.Tag) - 1)
End If

If Me.Tag <> "loading" Then lvwLoad

'putINI "Preferences", "Explorer Filter", cmbFilter.ListIndex
End Sub

Private Sub cmbFilter_GotFocus()
ApplyTransient "Files in the folder currently being viewed", , 1
End Sub

Private Sub cmbFilter_KeyDown(KeyCode As Integer, Shift As Integer)

If KeyCode <> vbKeyReturn Then Exit Sub
  
  'first see if text already exists
  If cmbFilter.Text = "*" Then cmbFilter.ListIndex = 0: Exit Sub
  Dim i As Integer
  For i = 0 To cmbFilter.ListIndex
    If cmbFilter.List(i) = cmbFilter.Text Then cmbFilter.ListIndex = i: GoTo Nxt
  Next
  cmbFilter.AddItem cmbFilter.Text
  cmbFilter.ListIndex = cmbFilter.ListCount - 1
  
Nxt:
If cmbFilter.ListCount > 9 Then
  cIni.Section = "Filter History": cIni.DeleteSection
  For i = 9 To cmbFilter.ListCount - 1
    putINI "Filter History", cmbFilter.List(i), ""
  Next
End If
End Sub

Private Sub cmbHistory_Click()
On Error Resume Next
'Static lFol As String
If fso.FolderExists(cmbHistory.Text) = False Then Exit Sub
tvw_OpenPath cmbHistory.Text
fList.Path = cmbHistory.Text
lvwLoad
'If lvw.ListItems.Count > 0 Then lvw.ListItems(1).Selected = True: CrlClick

If viViewHistoryByTime = True Then
  Dim tmp As String
  tmp = cmbHistory.ComboItems(LCase(cmbHistory.Text)).Text
  cmbHistory.ComboItems.Remove (cmbHistory.ComboItems(LCase(cmbHistory.Text)).Index)
  SetHistoryFolder tmp
  'putINI "Folder History", Path, Now
End If

If fso.FolderExists(cmbHistory.Text) Then
  cmbHistory.SelectedItem.Image = 6
Else
  cmbHistory.SelectedItem.Image = 8
  'cmbHistory.ComboItems(lFol).Selected = True
End If

End Sub

Private Sub cmbHistory_GotFocus()
ApplyTransient "History of folders visited", , 1
End Sub

Private Sub cmbHistory_KeyDown(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyReturn And fso.FolderExists(cmbHistory.Text) Then SetHistoryFolder cmbHistory.Text ' cmbHistory_Click
  If KeyCode = vbKeyEscape Then SetHistoryFolder fList.Path
End Sub

Private Sub cmbHistory_LostFocus()
  If cmbHistory.Text <> fList.Path Then SetHistoryFolder fList.Path
End Sub

Private Sub cmbSearch_KeyDown(KeyCode As Integer, Shift As Integer)

If KeyCode <> vbKeyReturn Then Exit Sub

  'first see if text already exists
  Dim i As Integer
  For i = 0 To cmbSearch.ListIndex
    If cmbSearch.List(i) = cmbSearch.Text Then cmbSearch.ListIndex = i: GoTo Nxt
  Next
  cmbSearch.AddItem cmbSearch.Text
  cmbSearch.ListIndex = cmbSearch.ListCount - 1

Nxt:
  cmdSearchGo_Click
  
  cIni.Section = "Search History": cIni.DeleteSection
  For i = extns.Count To cmbSearch.ListCount - 1
    putINI "Search History", cmbSearch.List(i), ""
  Next

End Sub
Private Sub cmbSearchIn_Click()
On Error Resume Next
If cmbSearchIn.SelectedItem.Key <> "Browse" Then Exit Sub
  Dim fol As String, i As Integer
fol = ShowFolder(Me, "Choose a Folder to Search in", "My Computer")
If fso.FolderExists(fol) = False Then Exit Sub
Set fldr = fso.GetFolder(fol)
cmbSearchIn.ComboItems.Add , fldr.Path, fldr.Path, 5, , 2
cmbSearchIn.ComboItems(fldr.Path).Selected = True

'save search folder history
cIni.Section = "Search Folders": cIni.DeleteSection
For i = cmbSearchIn.ComboItems("Browse").Index + 1 To cmbSearchIn.ComboItems.Count
  putINI "Search Folders", i - cmbSearchIn.ComboItems("Browse").Index, cmbSearchIn.ComboItems(i).Key
Next
End Sub

Private Sub cmbSearchIn_KeyDown(KeyCode As Integer, Shift As Integer)
On Error GoTo SSErr
Static lastInd As Integer
If cmbSearchIn.SelectedItem Is Nothing Then Exit Sub
lastInd = cmbSearchIn.SelectedItem.Index
Exit Sub

SSErr:
If KeyCode = vbKeyReturn Or KeyCode = vbKeyEscape Then cmbSearchIn.ComboItems(lastInd).Selected = True
End Sub

Private Sub cmdSearchGo_Click()

If cmdSearchGo.Caption = "Search" Then
  stbr.Panels(1).Visible = False
  stbr.Panels(2).Alignment = sbrRight
  stbr.Panels(2).Text = "Starting Search..."
  'Dim tm As Single
  'tm = Timer
  
  If chkSrchSize.Value = 1 Then
    If cmbSrchSize.Text = "more than" Then iSearchSize = 1
    If cmbSrchSize.Text = "less than" Then iSearchSize = 2
  Else
    iSearchSize = 0
  End If
  
  If cmbSearch.ListIndex < 9 And cmbSearch.ListIndex <> -1 Then
    strSearch = Mid(Mid(cmbSearch.Text, InStr(cmbSearch.Text, "(") + 1), 1, Len(cmbSearch.Text) - InStr(cmbSearch.Text, "(") - 1)
    bolSearchExt = True
  ElseIf InStr(cmbSearch, ".") <> 0 Then
    strSearch = Mid(cmbSearch, InStr(cmbSearch, ".") + 1): bolSearchExt = True
  Else
    strSearch = LCase(cmbSearch): bolSearchExt = False
  End If
  
  If strSearch = "*" Or strSearch = "*.*" Or strSearch = "" Then bolSearchAll = True Else bolSearchAll = False
  bolDoSearch = True: cmdSearchGo.Caption = "Stop": DoEvents
  
  If cmbSrchUnit = "KB" Then lSrchSiz = Val(txtSrchSize) * 1024 Else lSrchSiz = Val(txtSrchSize) * 1024 * 1024
  
  ClearLvw 3
  doSearchInFolder
  If bolDoSearch = False Then
    stbr.Panels(2).Text = "Search Stopped. " & lvw(3).ListItems.Count & " files found."
    cmdSearchGo.Caption = "Search"
  Else
    stbr.Panels(2).Text = "Search Completed. " & lvw(3).ListItems.Count & " files found."
    bolDoSearch = False: cmdSearchGo.Caption = "Search"
  End If
ElseIf cmdSearchGo.Caption = "Stop" Then
  bolDoSearch = False
  cmdSearchGo.Caption = "Search"
End If
  stbr.Panels(2).Alignment = sbrCenter
  stbr.Panels(1).Visible = True
'Debug.Print Timer() - tm
'MsgBox "Took " & tm - Timer() & " secs"'
End Sub

Sub doSearchInFolder()
'conduct search based on option in cmbSearchIn
On Error GoTo sortErr
'cmbSearchIn.ComboItems(cmbSearchIn.Text).Selected = True
If fso.FolderExists(cmbSearchIn.Text) Then
  doMatchFiles fso.GetFolder(cmbSearchIn.Text)
ElseIf cmbSearchIn.SelectedItem.Key = "My Computer" Or cmbSearchIn.SelectedItem.Key = "Hard Drives" Then
  Dim drv As Drive
  For Each drv In fso.Drives
    'Debug.Print Drive.DriveType
    If cmbSearchIn.SelectedItem.Key = "Hard Drives" And drv.DriveType <> 2 Then GoTo Nxt
    If drv.IsReady = False Then GoTo Nxt
      doMatchFiles fso.GetFolder(drv.DriveLetter & ":\")
Nxt:
  Next
ElseIf cmbSearchIn.SelectedItem.Key = "Browse" Then
  MsgBox "You have not yet selected a location in which to conduct this search." & vbCrLf & "Please select one and try again."
Else
  If fso.FolderExists(cmbSearchIn.SelectedItem.Key) Then
    doMatchFiles fso.GetFolder(cmbSearchIn.SelectedItem.Key)
  Else
    MsgBox "The search folder ('" & cmbSearchIn.SelectedItem.Key & "') doesn't seem to exist anymore." & vbCrLf & vbCrLf & "If it is on a CD, please re-insert it and try again." & vbCrLf & "If it is a network resource, make sure you are still connected to it."
  End If
End If

  Exit Sub

sortErr:
If Err.Number = 35601 Or Err.Number = 91 Then
  If fso.FolderExists(cmbSearchIn.Text) = False Then MsgBox "The location you have entered doesnt appear to be a valid folder," & vbCrLf & "or refers to a folder that is currently available."
Else
  MsgBox Err.Description
End If
Err.Clear
End Sub
 

Public Sub doMatchFiles(fol As Folder)

stbr.Panels(2).Text = "Searching in: " & fol.Path
DoEvents

If bolDoSearch = False Then Exit Sub

For Each fil In fol.Files
  'try and disqualify file
  
  '1st disqualify based on size
  If iSearchSize <> 0 Then
    If (iSearchSize = 1 And lSrchSiz < fil.Size) Or _
      (iSearchSize = 2 And lSrchSiz > fil.Size) Then GoTo Nxt
  End If
  
  If bolSearchAll = False Then 'disqualification possible only if this aint met
    'if extension mentioned _
      no extension matching and strsearch is in filename _
      searching for ext but none for file
    If (bolSearchExt = True And InStr(1, strSearch, fso.GetExtensionName(fil.Name), vbTextCompare) = 0) Or _
      (bolSearchExt = False And InStr(1, fil.Name, strSearch, vbTextCompare) = 0) Or _
      (bolSearchExt And fso.GetExtensionName(fil.Name) = "") Then GoTo Nxt
  End If
  
  'since not disqualified, add file
    AddItem 3, fil.Path
Nxt:
Next

Dim fldr As Folder
For Each fldr In fol.SubFolders
  doMatchFiles fldr
Next
End Sub

Private Sub conSearch_Resiz()
  lvw(3).Width = conSearch.Width
  cmbSearchIn.Width = conSearch.Width - cmbSearchIn.Left
  
  If conSearch.Width > 2745 Then
    txtSrchSize.Width = conSearch.Width - txtSrchSize.Left - cmbSrchUnit.Width
    cmbSrchUnit.Left = conSearch.Width - cmbSrchUnit.Width
  End If
  If conSearch.Width > 1815 Then
    cmbSearch.Width = conSearch.Width - cmbSearch.Left - cmdSearchGo.Width
    cmdSearchGo.Left = conSearch.Width - cmdSearchGo.Width
  End If
  If conSearch.Height > lvw(3).Top Then lvw(3).Height = conSearch.Height - lvw(3).Top
    '^ if stmt is to let hhsiz roll down with no more than 1000 twips margin
End Sub

Private Sub conviewer_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
If Button = 2 Then
  If mnuRZ.Visible = True And Shift = 1 Then PopupMenu mnuRZ Else mnuCall_Click
End If
End Sub

Private Sub conViewer_OLEDragDrop(Data As DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single)
'this code must be identical with the one in lvw_DragDrop [under index=1]
If fso.FileExists(Data.Files(1)) Then
  LoadFol fso.GetParentFolderName(Data.Files(1))
  lvw(1).ListItems(Data.Files(1)).Selected = True
  CrlClick , Data.Files(1)
ElseIf fso.FolderExists(Data.Files(1)) Then
  LoadFol Data.Files(1)
End If
End Sub

Private Sub conviewer_Resize()
If conViewer.Width < 300 Or conViewer.Height < 300 Then Exit Sub
browser.Height = conViewer.Height: browser.Width = conViewer.Width
rtb.Width = conViewer.Width: rtb.Height = conViewer.Height - rtb.Top

hs.Top = conViewer.ScaleHeight - hs.Height
vs.Left = conViewer.ScaleWidth - vs.Width
iv.Top = hs.Top: iv.Left = vs.Left
If ind = 2 And img.Picture <> 0 Then img.Visible = False: imgClick
If ind = 3 Then ani_Center ': wmp.Width = conViewer.Width: wmp.Height = conViewer.Height
'If ani.Visible = True Then
  'If ani.ImageSourceHeight = 0 Then 'And ani.Width > conViewer.Width Then
  '  ani.Left = 0
  '  ani.Width = conViewer.Width
  '  ani.Top = conViewer.Height - ani.Height
  'Else
    'ani_Center
  'End If
'End If
End Sub

Private Sub Form_Activate()
On Error Resume Next
If mnuSettings.Checked = True Then frmOptions.SetFocus
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If ani.Visible And ani.FileName <> "" Then '(Shift = 2 Or Shift = 3)
  If KeyCode = vbKeyNumpad1 Then mnuMedMoveBack_Click: KeyCode = 0
  If KeyCode = vbKeyNumpad2 Then mnuMedMoveFwd_Click: KeyCode = 0
  If KeyCode = vbKeyNumpad0 Then mnuMedMoveTo_Click: KeyCode = 0
  If KeyCode = vbKeyNumpad3 Then mnuMedMoveStep_Click: KeyCode = 0
  If KeyCode = vbKeyAdd Then mnuMediaVolInc_Click: KeyCode = 0
  If KeyCode = vbKeySubtract Then mnuMediaVolDec_Click: KeyCode = 0
End If

If bolDoSearch = True And KeyCode = vbKeyEscape Then bolDoSearch = False: cmdSearchGo.Caption = "Search"
End Sub

Private Sub Form_Load()
'Dim tm As Single
'tm = Timer(): Debug.Print: Debug.Print Now
On Error Resume Next

Dim bStart As Boolean, i As Integer

timStart = Now


If True = getINI("Application Start", "Splash") Then
  bStart = True
  frmSplash.isIvyLoading = True
End If

'set variables
If bStart Then frmSplash.lblStatus = "Loading Program Variables.": DoEvents: 'Debug.Print Timer() - tm & " " & frmSplash.lblStatus

z = 1: y = 100 'zoom & rate

'these menus should only turn vis when applicable...
mnuRZ.Visible = False
mnuMedia.Visible = False
mnuText.Visible = False
mnuImage.Visible = False
  sv3.Visible = False
If fso.FileExists(GetSetting(apComp, "Lyriks", "Lyrics Library File", "")) = False Then mnuOptionsShowLyrics.Enabled = False
If fso.FileExists(GetSetting(apComp, "Lyriks", "Lyriks EXE File", "")) = False Then mnuOptionsRunLyriks.Enabled = False
If True = getINI("Preferences", "Show Lyrics", False) Then mnuOptionsShowLyrics.Checked = True

'saving program location so softopen can run it
SaveSetting apComp, "Installed", apProd, apPath & apProd & ".exe"

'set recognised file types
If bStart Then frmSplash.lblStatus = "Loading Recognised File Extensions.": DoEvents
extns.loadFromINI
viDefExtn = getINI("Preferences", "Default Extn", 0)

loadExtensions

'load icons in toolbar
If bStart Then frmSplash.lblStatus = "Loading Toolbar Icons.": DoEvents: 'Debug.Print Timer() - tm & " " & frmSplash.lblStatus

TBar.ImageList = imTBar 'otherwise, cant modify imagelist
For i = 1 To TBar.Buttons.Count
If TBar.Buttons.Item(i).Key <> "" Then TBar.Buttons.Item(i).Image = imTBar.ListImages(TBar.Buttons.Item(i).Key).Index
Next i
doTBarCaption

'recall menu options
If bStart Then frmSplash.lblStatus = "Loading Stored Options.": DoEvents: 'Debug.Print Timer() - tm & " " & frmSplash.lblStatus
mnuMed_Click getINI("Preferences", "Media Size", 4)
viShowHtmlAsText = getINI("Preferences", "Show Html As Text", True)
ani.InvokeURLs = getINI("Preferences", "Block Media URLs", False)
viReorderAsShuffle = getINI("Preferences", "Reorder with Shuffle", True)
If False = getINI("Preferences", "Media ViewFrames", False) Then ani.DisplayMode = mpTime Else ani.DisplayMode = mpFrames

viViewHistoryByTime = getINI("Preferences", "Sort History By Viewed Order", False)
viChangeHorz = getINI("Preferences", "Switch Horizontal for Audio", True)

'set timer interval
Tmr.Interval = getINI("Preferences", "Timer Interval", 1000)
MStep = getINI("Preferences", "Media Move Step", 2)

'caption, trash & clipboard options
viTitle = getINI("Preferences", "Scroll What", 1)
viShowTime = getINI("Preferences", "Show Time", True)

Set fldr = fso.GetFolder(getINI("Trash", "Trash Location", apPath & "Trash"))
viTrashLoc = fldr.Path
viTrashOpt = getINI("Trash", "Trash Option", 2) 'default - recycle bin

'recall width and space options for lvw & lst view
  If bStart Then frmSplash.lblStatus = "Recalling Filelist Column Views.": DoEvents: 'Debug.Print Timer() - tm & " " & frmSplash.lblStatus
  
  'this must be done before recalling width options
    viLvwExtension = getINI("File List", "Extension", True)
  
  Dim sv As String, sw As String, dList 'As ListView
  For Each dList In lvw
    If Me.viLvwExtension = True Then dList.ColumnHeaders(3).Text = "Ext"

    For i = 1 To dList.ColumnHeaders.Count - 1
    sv = "lvw" & dList.Index & " visible " & dList.ColumnHeaders(i).Text
    sw = "lvw" & dList.Index & " width " & dList.ColumnHeaders(i).Text
    If getINI("File List Columns", sv, True) = True Then
    dList.ColumnHeaders(i).Width = getINI("File List Columns", sw, dList.ColumnHeaders(i).Width)
    Else
    dList.ColumnHeaders(i).Tag = getINI("File List Columns", sw, dList.ColumnHeaders(i).Width)
    dList.ColumnHeaders(i).Width = 0
    End If
    Next i
  Next

'recall lvw view options
If bStart Then frmSplash.lblStatus = "Loading Interface Appearance.": DoEvents: 'Debug.Print Timer() - tm & " " & frmSplash.lblStatus

Dim bGrid As Boolean, bCol As Long, fCol As Long, fFace As String, iSiz As Integer
bGrid = getINI("File List", "Gridlines", False)
bCol = getINI("File List", "BackColor", vbWhite)
fCol = getINI("File List", "ForeColor", vbBlack)
fFace = getINI("File List", "Font", "Verdana")
iSiz = getINI("File List", "FontSize", 12)

For Each dList In lvw
  dList.BackColor = bCol
  dList.ForeColor = fCol
  dList.Font.Name = fFace
  dList.Font.Size = iSiz
  dList.GridLines = bGrid
Next
mnuLvwView_Click getINI("File List", "View", 3)  'default is view details

conViewer.BackColor = getINI("Appearance", "Background Color", conViewer.BackColor)

icoLarge.BackColor = lvw(1).BackColor: icoSmall.BackColor = lvw(1).BackColor

If fso.FileExists(getINI("File List", "Background Image")) Then
  For Each dList In lvw
    Set dList.Picture = LoadPicture(getINI("File List", "Background Image"))
  Next
End If

'text (rtb) options
loadRtbFont rtb
rtb.BackColor = Val(getINI("Text", "BackColor", &H80000005)) 'default is window bgd
mnuTxtSize.Caption = mnuTxtSize.Tag & " (" & rtb.Font.Size & ")"


If bStart Then frmSplash.lblStatus = "Loading Favorites": DoEvents: 'Debug.Print Timer() - tm & " " & frmSplash.lblStatus
LoadFaves

'hlin & vlin properties
If bStart Then frmSplash.lblStatus = "Loading Environment Settings": DoEvents: 'Debug.Print Timer() - tm & " " & frmSplash.lblStatus
vlin.Left = getINI("Appearance", "vlin", 3800): Vsiz
RollStatus = getINI("Appearance", "hlin.pos", 0)
hlin.Tag = getINI("Appearance", "hlin.top", hlin.Top)
hhlin.Top = getINI("Appearance", "hhlin", hhlin.Top)
vvlin.Left = getINI("Appearance", "vvlin", vvlin.Left)
  'make it roll down if a file/folder is passed & roll up if list passed
  ' therefore ROLLing is done only in form_resize


'load filelist
Dim fl As String
If True = getINI("Application Start", "Remember Filelist", True) Then 'SRIRAMS IDEA TO NOT LOAD LSTFILES ON REQUEST
If bStart Then frmSplash.lblStatus = "Loading Filelists.": DoEvents: 'Debug.Print Timer() - tm & " " & frmSplash.lblStatus
  strList = apPath & "iviewer.lst"
  If fso.FileExists(strList) Then lstLoad
End If


'see if file or folder to open was passed
'must do before add history items... in order to change 'path' in registry
Dim strCommand As String
  strCommand = Command() '"/3/D:\imufol\vb\IViewer\allonHDD.m3u"
  'strCommand = "/o/D:\imufol\vb\IViewer\songs.lst"
  'strCommand = "D:\Daddy\AddinG&SSite\from shop"

If fso.FolderExists(strCommand) Then
  putINI "Application Start", "Explore Path", strCommand
  
End If
If fso.FileExists(strCommand) Then putINI "Application Start", "Explore Path", fso.GetParentFolderName(strCommand)

'load folders (must have tvw nodes before loading history causes tvw_Openpath
If bStart Then frmSplash.lblStatus = "Loading Drives and Folders": DoEvents: 'Debug.Print Timer() - tm & " " & frmSplash.lblStatus
Tvw_Refresh

'add history items
If True = getINI("Application Start", "Remember Folder", True) Then      'SRIRAMS IDEA TO NOT LOAD LSTFILES ON REQUEST
  If bStart Then frmSplash.lblStatus = "Loading Folder History": DoEvents: 'Debug.Print Timer() - tm & " " & frmSplash.lblStatus
  
  LoadHistory
  
  SetHistoryFolder getINI("Application Start", "Explore Path", apPath)
Else
  SetHistoryFolder apPath
End If

'load folders
If bStart Then frmSplash.lblStatus = "Opening Folder '" & cmbHistory.Text & "'": DoEvents: 'Debug.Print Timer() - tm & " " & frmSplash.lblStatus

'this was in Tvw_Refresh but moved to (mnu_Refresh & here) to avoid loading trouble
If fso.FolderExists(cmbHistory.Text) Then fList.Path = cmbHistory.Text Else SetHistoryFolder fList.Path
tvw_OpenPath cmbHistory.Text: lvwLoad

'now that we've loaded in folder of file that was passed,
If fso.FileExists(strCommand) Then
  If bStart Then frmSplash.lblStatus = "Opening File - " & strCommand: DoEvents: 'Debug.Print Timer() - tm & " " & frmSplash.lblStatus
  Set filCom = fso.GetFile(strCommand)
  fList.Pattern = "*"
  If lvw(1).ListItems.Count > 0 Then lvw(1).ListItems(1).Selected = False
  
  For Each itm In lvw(1).ListItems
    If itm.Key = filCom.Path Then itm.Selected = True:  CrlClick: Exit For
  Next
End If

'if a playlist was passed [must make indView = 2]
If Mid(strCommand, 1, 1) = "/" Then  ' see if it is a list files that has been passed
  If bStart Then frmSplash.lblStatus = "Loading Playlist - " & Mid(strCommand, 4): DoEvents: 'Debug.Print Timer() - tm & " " & frmSplash.lblStatus
  'If Mid(strCommand, 3, 1) = "0" Then  'see if mode has been passed (0 for no mode - thus mesg reqd)
    'If Mid(strCommand, 2, 1) = "o" Then mesg = "open "
    'If Mid(strCommand, 2, 1) = "e" Then mesg = "add " 'enqueue
  'Else
    'ind = Mid(strCommand, 3, 1)
    'ChangeMode
  'End If

  'open / enqueue code
  strList = Mid(strCommand, 4)
  If Mid(strCommand, 2, 1) = "o" Then ClearLvw 2
  lstLoad
  
  
  IndView = 2
Else
  IndView = getINI("Appearance", "List View", 1)  'Explorer
End If

conNavigator.Height = Me.ScaleHeight - conNavigator.Top
conViewer.Height = conNavigator.Height

'initialize search pane
If bStart Then frmSplash.lblStatus = "Initialising Search Pane": DoEvents: 'Debug.Print Timer() - tm & " " & frmSplash.lblStatus
  cmbSrchUnit.AddItem "KB": cmbSrchUnit.AddItem "MB": cmbSrchUnit.ListIndex = 0
  cmbSrchSize.AddItem "more than": cmbSrchSize.AddItem "less than": cmbSrchSize.ListIndex = 0
'search's lookin combo
  cmbSearchIn.ComboItems.Add , "My Computer", "My Computer", imTvw.ListImages("mycomp").Index
  cmbSearchIn.ComboItems.Add , "Hard Drives", "Local Hard Drives", 2, , 2

  Dim strHard As String, drv As Drive, sDrv As String
  For Each drv In fso.Drives
    'If Drive.DriveLetter <> "A" Then
      If drv.IsReady Then sDrv = drv.VolumeName Else sDrv = ""
      cmbSearchIn.ComboItems.Add , drv.DriveLetter & ":\", sDrv & " (" & drv.DriveLetter & ":)", imTvw.ListImages(drv.DriveType).Index, , 2
      'get list of local hard drives
      If drv.DriveType = 2 Then strHard = strHard & drv.DriveLetter & ":; "
    'End If
  Next
  If Len(strHard) <> 0 Then strHard = Mid(strHard, 1, Len(strHard) - 2)
  cmbSearchIn.ComboItems("Hard Drives").Text = "Local Hard Drives (" & strHard & ")"  'so as not to do 'for each drive' again
  cmbSearchIn.ComboItems.Add , "Browse", "Browse...", imTvw.ListImages("current").Index, , 0
  cmbSearchIn.ComboItems(1).Selected = True

'search folder history
  Dim strS() As String, cnt As Long
  cIni.Section = "Search Folders": cIni.EnumerateCurrentSection strS, cnt
  For i = 1 To cnt
    cmbSearchIn.ComboItems.Add , strS(i), strS(i), imTvw.ListImages("folder").Index, , 2
  Next

If bStart Then frmSplash.lblStatus = "Preparing to Load Main Window": DoEvents: 'Debug.Print Timer() - tm & " " & frmSplash.lblStatus

'initialise html's path
browser.Navigate (apPath & "blank.htm") ' "about:blank")
'browser.Document.body.innerhtml = "<html><body><font color=409040><center> <h1>There is no file to view.</h1> </font></center></body></html>"

Me.WindowState = getINI("Appearance", "Window State", Me.WindowState)

Me.setCaption

If bStart Then frmSplash.isIvyLoading = False
End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
If Button = 2 And conViewer.Visible = False Then PopupMenu mnuImage
End Sub

Private Sub Form_Resize()
On Error Resume Next

If Me.WindowState = vbNormal Then
  If Me.Top < 0 Then Me.Top = 0
  If Me.Left < 0 Then Me.Left = 0
End If

If Me.WindowState = vbMinimized Then Exit Sub

putINI "Appearance", "Window State", Me.WindowState

'ensure navigator has min ht 1800 & form has min width
If mnuHorz.Checked Then
  If Me.ScaleHeight - conNavigator.Top < 1030 Then Me.Height = conNavigator.Top + 1045 + (Me.Height - Me.ScaleHeight): Exit Sub
Else
  If Me.ScaleHeight < conNavigator.Top + 1800 Then Me.Height = conNavigator.Top + 1815 + (Me.Height - Me.ScaleHeight): Exit Sub
End If
If Me.Width < 3000 Then Me.Width = 3000

conNavigator.Height = Me.ScaleHeight - conNavigator.Top

vlin.Height = Me.ScaleHeight - vlin.Top
hhlin.Width = Me.ScaleWidth

If mnuHorz.Checked Then
  hhsiz
Else
  Vsiz
  conViewer.Height = Me.ScaleHeight - conViewer.Top
  
  If RollStatus = 1 Then
    hlin.Top = Me.ScaleHeight - hlin.Height - lvw(2).Top
    If mnuStatusBar.Checked Then hlin.Top = stbr.Top - hlin.Height
  End If
  
  If RollStatus = 0 Then  'to resize if middle & lvw is gonna go out of navigator
    If hlin.Top > stbr.Top - 300 Then hlin.Top = stbr.Top - 1000
  End If
  
  Hsiz 'is causing startup problems
End If

If ind = 2 Then imgClick
If ind = 3 Then ani_Center

'stuff to do after form is visibly loaded
'stuff to do after form is visibly loaded
If Me.Tag = "loading" Then
  Me.Tag = ""

  'If ind = 0 Then ind = 3: ChangeMode
  If False = getINI("Preferences", "Show StatusBar", True) Then mnuStatusBar_Click
  If True = getINI("Preferences", "Show Toolbar", True) Then mnuToolbarView_Click Else TBar_Resiz
  If True = getINI("Preferences", "Split Horizontally", False) Then mnuHorz_Click Else Roll
  conNavigator.Visible = True
  conViewer.Visible = True
  
  mnuView_Click CInt(IndView)
  
  'finally unload splash screen
  If "True" = getINI("Application Start", "Splash", "True") Then Unload frmSplash
End If

End Sub
Private Sub Form_Unload(Cancel As Integer)
mnuExit_Click
End Sub

Private Sub hs_Change()
img.Left = 0 - hs * 1000
If mnuFullScreen.Checked Then img.Left = img.Left - vlin.Left
End Sub

Private Sub img_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
If Button = 2 Then
  If mnuRZ.Visible = True And Shift = 1 Then PopupMenu mnuRZ Else PopupMenu mnuImage
End If
End Sub

Public Sub CrlClick(Optional crlMode As Byte, Optional sFile As String)
'this code used to be under file_click & later lvw_click & lastly lvwClick
On Error Resume Next
'If ind = 0 Then Exit Sub

'needed for sticky mode
If crlMode = 0 Then crlMode = ind
If fso.FileExists(sFile) = False Then
  If lvw(IndView).SelectedItem Is Nothing Then Exit Sub
  If lvw(IndView).SelectedItem.Key = "" Then MsgBox "No Key Found", vbCritical
  sFile = lvw(IndView).SelectedItem.Key
End If

CurFile = sFile

If fso.FileExists(sFile) = False Then
  If IndView <> 2 And sFile = lvw(IndView).SelectedItem.Key Then lvw(IndView).ListItems.Remove (lvw(IndView).SelectedItem.Index)
  Exit Sub
End If

extnn = LCase(fso.GetExtensionName(sFile))
If extnn = "" Then
  If viDefExtn <> 0 Then crlMode = viDefExtn 'default mode
Else
  crlMode = extns.QueryExtn(extnn)
  If crlMode = 1 Then
    If ind <> 1 Then browser.Navigate "": crlMode = 1 'Html
  ElseIf crlMode = 2 Then
    If ind <> 2 Then crlMode = 2   'Image
  ElseIf crlMode = 3 Then
    If ind <> 3 Then
      ani.FileName = "": crlMode = 3  'Media
      If Tmr.Enabled = True Then Tmr.Enabled = False
    End If
  ElseIf crlMode = 4 Then
    If ind <> 4 Then crlMode = 4   'Text
  ElseIf crlMode = -1 Then 'ask user to set type (for hitherto unheard of extension)
    If viDefExtn = 0 Then 'same as previous
      crlMode = ind
    ElseIf viDefExtn > 0 Then
      crlMode = viDefExtn
    ElseIf viDefExtn = -1 Then 'prompt user (new type)
      putINI "Preferences", "Options Tab Index", 5
      Load frmOptions
      frmOptions.tvw.Tag = extnn
      frmOptions.Show vbModal
      crlMode = conViewer.Tag
      conViewer.Tag = ""
    End If
  ElseIf crlMode = 0 Then 'load some blank file in IE
    ind = 1
    ChangeMode
    CurFile = sFile
    Me.setCaption
    browser.Navigate (apPath & "none.htm")
    Exit Sub
  End If
End If

DoEvents

'switch mode if necessary
If crlMode <> ind Then ind = crlMode: ChangeMode

'if mode has changed and ani is playing, then stop
If ind <> 3 And ani.PlayState = mpPlaying Then ani.Stop

CurFile = sFile
Me.setCaption

Select Case ind 'mnuMode(crlMode).Tag
Case 1 '"IE"
  browser.Navigate (sFile)
Case 2 '"Image"
If mnuImage.Tag > 6 Then
  If ind <> 1 Then browser.Visible = True: img.Visible = False
  If mnuImage.Tag = 7 Then
    browser.Navigate sFile
  End If
  browser.Visible = True
Else
  On Error GoTo ErrImg
  Set img.Picture = LoadPicture(sFile)
ErrImgRet:
  imgClick
End If
Case 3 '"Media"
  If viChangeHorz And mnuHorz.Checked = False And InStr(extns.Item("Audio").Extensions, extnn) <> 0 Then
    If False = getINI("Preferences", "Notified Audio Horz Switch", False) Then
      Dim m As VbMsgBoxResult
      m = MsgBox("IViewer Recommends switching to Horizontal view for playing of Audio files." & vbCrLf & _
        "Would you like for it to keep doing so automatically? You may change this option as " & vbCrLf & _
        "'Switch to Horizontal to play Audio' in Options/General of the Settings Dialog." & vbCrLf & vbCrLf & _
        "Click 'View Horizontally' in View menu to alternate the positions of Viewer & Filelists", vbYesNo + vbQuestion, "Auto Switch Horizontal")
        If m = vbNo Then
          viChangeHorz = False
          putINI "Preferences", "Switch Horizontal for Audio", False
        End If
        putINI "Preferences", "Notified Audio Horz Switch", True 'One time only
    End If
    If mnuOptionsShowLyrics.Enabled And mnuOptionsShowLyrics.Checked And rtb.Visible = True Then Else mnuHorz_Click
      'even if guy says no the first time, let him see what horizontal is like
  End If
  If mnuMediaPause.Checked Then mnuMediaPause.Checked = False
  If ani.ShowStatusBar = False Then ani.ShowStatusBar = True
  If ani.ShowTracker = False Then ani.ShowTracker = True
  ani.DisplaySize = mnuMedia.Tag
  ani.FileName = sFile
  ani.Play
  stbr.Panels(1) = "Rate: " & ani.Rate
  stbr.Panels(1).ToolTipText = stbr.Panels(1).Text
  y = ani.Rate * 100
Case 4 '"Text"
  rtb.Tag = ""
  rtb.LoadFile sFile, rtfText
  If rtb.Text = "" Then rtb.Text = "[no text in file]"
End Select

Exit Sub

ErrImg:
If fso.FileExists(apPath & "noimg.gif") And img.Visible = True Then Set img.Picture = LoadPicture(apPath & "noimg.gif"): GoTo ErrImgRet
End Sub

Public Sub lvwLoad()
On Error Resume Next

Set fldr = fso.GetFolder(fList.Path)
ClearLvw 1
If fldr.IsRootFolder Then lvw(1).Tag = fldr Else lvw(1).Tag = fldr & "\"

'no need to clear duplicate entries in history SINCE WE GOT SETHISTORYFOLDER
SetHistoryFolder fldr.Path

'see if there are no files to show
If fList.ListCount = 0 Then
  If fList.Pattern = "*.*" Or fList.Pattern = "*" Then Exit Sub
  'cmbFilter.ListIndex = 0
  'cmbFilter_Click
  'Exit Sub
  
Else
'getting the names into the FileList
Dim i As Integer
  For i = 0 To fList.ListCount - 1
    fList.ListIndex = i
    AddItem 1, lvw(1).Tag & fList.List(i)
  Next i
  'lvw(1).ListItems(1).Selected = True
End If

lvw(1).Refresh
putINI "Application Start", "Explore Path", fList.Path
End Sub

Private Sub ClearLvw(Index As Integer)
bolHasMedia(Index) = False
If lvw(Index).ColumnHeaders(5).Width > 0 Then
  lvw(Index).ColumnHeaders(5).Tag = lvw(Index).ColumnHeaders(5).Width
  lvw(Index).ColumnHeaders(5).Width = 0
End If
'LockWindowUpdate lvw(Index).hWnd
lvw(Index).ListItems.Clear
'LockWindowUpdate 0
End Sub

Public Function AddItem(Index As Integer, strFile As String) As ListItem
'chk and see if file already exists
'problem detected 22 Dec 05 when loading all from time library
'causes error if already present

On Error Resume Next
Set AddItem = lvw(Index).ListItems(LCase(strFile))
If Not AddItem Is Nothing Then Exit Function

Dim strIcon As String, fil As File

strIcon = GetFilIcon(strFile)
'filename,size, type, date modi,time, [folder,] bytes

If fso.FileExists(strFile) = False Then
  Set AddItem = lvw(Index).ListItems.Add(lvw(Index).ListItems.Count + 1, _
    fso.GetParentFolderName(strFile), fso.GetFileName(strFile), _
    icoLarge.ListImages(strIcon).Index, icoSmall.ListImages(strIcon).Index)
  If Index <> 1 Then AddItem.SubItems(5) = fso.GetParentFolderName(strFile)
  AddItem.SubItems(6) = 0
  Exit Function
End If

Set fil = fso.GetFile(strFile)

Set AddItem = lvw(Index).ListItems.Add(lvw(Index).ListItems.Count + 1, fil.Path, _
  fil.Name, icoLarge.ListImages(strIcon).Index, icoSmall.ListImages(strIcon).Index)

AddItem.SubItems(1) = StrSiz(fil.Size)
If viLvwExtension Then AddItem.SubItems(2) = fso.GetExtensionName(fil.Path) Else AddItem.SubItems(2) = fil.Type
If viShowTime = True Then AddItem.SubItems(3) = fil.DateLastModified Else AddItem.SubItems(3) = Format(fil.DateLastModified, "dd mmm yy")

If Index = 1 Then
  AddItem.SubItems(5) = fil.Size
Else
  AddItem.SubItems(5) = fil.ParentFolder
  AddItem.SubItems(6) = fil.Size
End If

'if any media is added, make time visible
Dim ext As String, tim As String
Static tl As New cInifile
If tl.Path = "" Then tl.Path = apPath & "Time Library.ini": tl.Section = "Time Library"

ext = LCase(fso.GetExtensionName(strFile))
If ext <> "" And InStr(extns.Item("Audio").Extensions & ";" & extns.Item("Video").Extensions, ext) <> 0 Then
  tl.Key = fil.Path
  tim = tl.Value  'getINI("Time Library", fil.Path, "")
  If bolHasMedia(Index) = False Then
    bolHasMedia(Index) = True
    lvw(Index).ColumnHeaders(5).Width = lvw(Index).ColumnHeaders(5).Tag
    If Index = 1 Then lvwSizCol lvw(1), 1 Else lvwSizCol lvw(Index), 6
  End If
  AddItem.SubItems(4) = tim
End If

End Function

Public Sub CrlNext() '(Optional isMedia As Boolean)
If lvw(IndView).SelectedItem Is Nothing Then ani.Stop: Exit Sub
If mnuShuffle.Checked Then
  lvw(IndView).SelectedItem.Selected = False
  Randomize
  If viReorderAsShuffle Then 'move a random file from ahead of selection to nextposition
    If lvw(IndView).SelectedItem.Index = lvw(IndView).ListItems.Count Then 'call next subfolder when were adding that option
      lvw(IndView).ListItems(1).Selected = True
      CrlClick
      'lvw(IndView).SelectedItem.EnsureVisible
    Else
      Swap lvw(IndView).SelectedItem.Index + 1, lvw(IndView).SelectedItem.Index + Rnd * (lvw(IndView).ListItems.Count - lvw(IndView).SelectedItem.Index), IndView
      lvw(IndView).SelectedItem.Selected = False
      'lvw(IndView).ListItems(lvw(IndView).SelectedItem.Index + 1).Selected = True
      Set lvw(IndView).SelectedItem = lvw(IndView).ListItems(lvw(IndView).SelectedItem.Index + 1)
      lvw(IndView).SelectedItem.EnsureVisible
      CrlClick
    End If
  Else
    lvw(IndView).ListItems(1 + Rnd * (lvw(IndView).ListItems.Count - 1)).Selected = True
    lvw(IndView).SelectedItem.EnsureVisible
  End If
Else
  If lvw(IndView).SelectedItem.Index = lvw(IndView).ListItems.Count Then
    lvw(IndView).SelectedItem.Selected = False
    lvw(IndView).ListItems.Item(1).Selected = True
  Else: lvw(IndView).SelectedItem.Selected = False
    lvw(IndView).ListItems.Item(lvw(IndView).SelectedItem.Index + 1).Selected = True
  End If
  lvw(IndView).SelectedItem.EnsureVisible
End If

CrlClick
End Sub

Private Sub mnuFaveNode_Click()
AddFolderNode tvw.SelectedItem.Tag, 10
End Sub

Private Sub mnuListPlayNext_Click()
If lvw(IndView).ListItems.Count = 0 Then Exit Sub

If mnuShuffle.Checked Then
  Dim m As VbMsgBoxResult
  m = MsgBox("To do this, you will have to turn  off 'Shuffle & Play'" & vbCrLf & "Do you wish to do so?", vbQuestion + vbYesNo, "Play Selection Next")
  If m = vbNo Then Exit Sub Else mnuShuffle.Checked = False
End If
  
Dim itm As ListItem, i As Integer, itms() As String
ReDim itms(0)
For i = lvw(IndView).ListItems.Count To 1 Step -1
  If lvw(IndView).ListItems(i).Selected And lvw(IndView).ListItems(i).Key <> CurFile Then
    ReDim Preserve itms(UBound(itms) + 1)
    itms(UBound(itms)) = lvw(IndView).ListItems(i).Key
  End If
Next

Dim nItm As ListItem, iTo As ListItem, j As Integer, tmp As String
lvw(IndView).Sorted = False
On Error Resume Next
Set iTo = lvw(IndView).ListItems(CurFile)
For i = 1 To UBound(itms)
  Set itm = lvw(IndView).ListItems(itms(i))
  tmp = itm.Key
  itm.Key = ""
  If iTo Is Nothing Then
    Set nItm = lvw(IndView).ListItems.Add(1, tmp, itm.Text, itm.Icon, itm.SmallIcon)
  Else
    Set nItm = lvw(IndView).ListItems.Add(iTo.Index + 1, tmp, itm.Text, itm.Icon, itm.SmallIcon)
  End If
  For j = 1 To lvw(IndView).ColumnHeaders.Count - 1
    nItm.SubItems(j) = itm.SubItems(j)
  Next
  lvw(IndView).ListItems.Remove itm.Index
Next

lvw(IndView).MultiSelect = False
If iTo Is Nothing Then lvw(IndView).ListItems(1).Selected = True Else iTo.Selected = True
lvw(IndView).MultiSelect = True
End Sub

Private Sub mnuListRename_Click()
lvw(IndView).StartLabelEdit
End Sub

Private Sub lvw_AfterLabelEdit(Index As Integer, Cancel As Integer, NewString As String)
If Cancel = 0 Then
  Dim result As Long, fileop As SHFILEOPSTRUCT, pos As Single, pst As MPPlayStateConstants
  With fileop
    .hWnd = Me.hWnd
    .wFunc = &H4 'FO_RENAME
    .pFrom = lvw(Index).SelectedItem.Key
    .pTo = AddSlash(fso.GetParentFolderName(.pFrom)) & NewString
    .fFlags = &H100 Or &H80 'FOF_SIMPLEPROGRESS or FOF_FILESONLY
    If ani.FileName = .pFrom Then pos = ani.CurrentPosition: pst = ani.PlayState: ani.FileName = ""
  End With
  
  result = SHFileOperation(fileop)
  Me.Tag = "ren"
  If result = 0 Then
    lvw(Index).SelectedItem.Text = NewString
    lvw(Index).SelectedItem.Key = fileop.pTo
    If pos <> 0 Then
      ani.AutoStart = False
      ani.FileName = fileop.pTo
      ani.AutoStart = True
      If ani.ShowStatusBar = False Then ani.ShowStatusBar = True
      If ani.ShowTracker = False Then ani.ShowTracker = True
      ani.DisplaySize = mnuMedia.Tag
      ani.CurrentPosition = pos
      If pst = mpPlaying Then ani.Play Else ani.Stop
    End If
  End If
  
  If result <> 0 Then
    MsgBox "Error while renaming " & vbCrLf & Err.Description ' Operation failed
  Else
    If fileop.fAnyOperationsAborted <> 0 Then MsgBox "Operation Failed"
  End If
End If
End Sub

Private Sub lvw_ColumnClick(Index As Integer, ByVal ColumnHeader As MSComctlLib.ColumnHeader)
Dim Order As Integer
Static lastSorted(3) As Integer, lastAsc(3) As Boolean
If lastSorted(Index) = ColumnHeader.Index And lastAsc(Index) = False Then Order = 4: lastAsc(Index) = True Else Order = 3: lastAsc(Index) = False

SortColumn lvw(Index), ColumnHeader, Order
lastSorted(Index) = ColumnHeader.Index
End Sub

Private Sub SortColumn(cLvw As ListView, ByVal ColumnHeader As MSComctlLib.ColumnHeader, Order As Integer)
Dim sType As Integer, Indx As Integer

Indx = ColumnHeader.Index 'default

If ColumnHeader.Index = 2 Or ColumnHeader.Index = cLvw.ColumnHeaders.Count Then
  sType = 1  'Size
  Indx = cLvw.ColumnHeaders.Count 'last column if its for size
ElseIf ColumnHeader.Text = "Date Modified" Then
  sType = 2 'Dates
Else
  sType = 0 'Name
End If

  SortLvwColumn cLvw, Indx, sType, Order
End Sub

Private Sub lvw_DblClick(Index As Integer)
CrlClick
crlSiz
End Sub

Private Sub lvw_ItemClick(Index As Integer, ByVal Item As MSComctlLib.ListItem)
crlSiz
lvw_MouseUp Index, 1, 0, 0, 0
If Index = 2 Then 'Playlist - in case file has been restored/removable media inserted
  If Item.Icon = 1 And fso.FileExists(Item.Key) Then
    Dim strIcon As String
    strIcon = GetFilIcon(Item.Key)
    Item.Icon = icoLarge.ListImages(strIcon).Index: Item.SmallIcon = icoSmall.ListImages(strIcon).Index
  End If
  If fso.FileExists(Item.Key) = False And Item.Icon <> 1 Then Item.Icon = 1: Item.SmallIcon = 1
End If
End Sub

Private Sub lvw_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
If KeyCode = 93 Then PopupMenu menuFileList 'right click
If Index <> 2 Then 'Search or Explorer
  If KeyCode = vbKeyInsert And Shift = 0 Then mnuListAdd_Click 'Adds selected to lvw(2)
  If KeyCode = vbKeyDelete Then mnuListDelete_Click 'Delete
  If KeyCode = vbKeyF2 Then mnuListRename_Click
Else 'Playlist
  If Shift = 1 Then
    If KeyCode = vbKeyPageUp Then mnuPlaylistMoveUp_Click: KeyCode = 0
    If KeyCode = vbKeyPageDown Then mnuPlaylistMoveDown_Click: KeyCode = 0
  End If
  If KeyCode = vbKeyDelete Then
    Dim i As Integer
    i = 1
Ctr:
    Do Until i = lvw(2).ListItems.Count + 1
      If Shift = 0 Then
        If lvw(2).ListItems(i).Selected Then lvw(2).ListItems.Remove (lvw(2).ListItems(i).Index): GoTo Ctr
      Else
        If lvw(2).ListItems(i).Selected Then Else lvw(2).ListItems.Remove (lvw(2).ListItems(i).Index): GoTo Ctr
      End If
      i = i + 1
    Loop
  End If
End If

If lvw(Index).SelectedItem Is Nothing Then Exit Sub
If InStr(1, extns.Item("Audio").Extensions & ";" & extns.Item("Video").Extensions, LCase(fso.GetExtensionName(lvw(Index).SelectedItem.Text)), vbTextCompare) <> 0 Then
  If KeyCode = vbKeyReturn Then
    If Me.Tag = "ren" Then Me.Tag = "" Else CrlClick 'lvw_MouseUp Index, 1, Shift, 0, 0
  End If
End If

End Sub

Private Sub lvw_KeyPress(Index As Integer, KeyAscii As Integer)
If KeyAscii = 1 Then 'Ctrl + A
  For Each itm In lvw(Index).ListItems
    itm.Selected = True
  Next
  crlSiz
End If
If Index = 1 And KeyAscii = vbKeyBack Then TBar_ButtonClick TBar.Buttons("up")
End Sub

Private Sub lvw_MouseDown(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
If Button = 2 Then
  If Shift = 0 Then PopupMenu menuFileList
  If Shift = 1 Then mnuCall_Click
  Button = 0
End If
End Sub

Private Sub lvw_MouseUp(Index As Integer, Button As Integer, Shift As Integer, x As Single, y As Single)
If Button = 1 Then
  Button = 0
  On Error Resume Next
  If lvw(Index).ListItems.Count = 0 Then Exit Sub
  
  Dim c As Integer, i As Integer
  'check if multiple files are selected & exit sub
  c = 0
  For i = 1 To lvw(Index).ListItems.Count
  If lvw(Index).ListItems(i).Selected Then c = c + 1: If c > 1 Then Exit Sub
  Next i
  
  If InStr(extns.Item("Audio").Extensions & ";" & extns.Item("Video").Extensions, LCase(fso.GetExtensionName(lvw(Index).SelectedItem.Text))) <> 0 Then
    If ani.FileName <> lvw(Index).SelectedItem.Key Then
      'CrlClick
      crlSiz
    Else
      Me.setCaption
    End If
  Else
    If CurFile <> lvw(Index).SelectedItem.Key And InStr(extns.Item("Audio").Extensions & ";" & extns.Item("Video").Extensions, LCase(fso.GetExtensionName(lvw(Index).SelectedItem.Text))) = 0 Then CrlClick: crlSiz
  End If
End If
End Sub

Private Sub lvw_OLEDragDrop(Index As Integer, Data As MSComctlLib.DataObject, Effect As Long, Button As Integer, Shift As Integer, x As Single, y As Single)
On Error Resume Next
If Index = 2 Then 'Playlist
  Dim i As Integer
  For i = 1 To Data.Files.Count
    If fso.FileExists(Data.Files(i)) Then
      AddItem 2, Data.Files(i)
    End If
  Next
ElseIf Index = 1 Then
  'this code must be identical with the one in conViewer_DragDrop
  If fso.FileExists(Data.Files(1)) Then
    LoadFol fso.GetParentFolderName(Data.Files(1)) 'loadfol is used in mnuFileOpen
    lvw(1).ListItems(Data.Files(1)).Selected = True
    CrlClick , Data.Files(1)
  ElseIf fso.FolderExists(Data.Files(1)) Then
    LoadFol Data.Files(1)
  End If
End If
End Sub

Private Sub mnuF6_Click()
If ani.Visible Then
  If y > 10 Then y = y - 10: ani.Rate = y / 100: stbr.Panels(1) = "Rate: " & ani.Rate
  y = ani.Rate * 100
ElseIf img.Visible Then
  If mnuImage.Tag <> 1 Then Exit Sub
  z = z - 1
  If z < -20 Then z = -20
  If z = 0 Then z = z - 2
  imgClick
End If
End Sub

Private Sub mnuF6c_Click()
If ani.Visible = False Then Exit Sub
If y > 1 Then y = y - 1: ani.Rate = y / 100: stbr.Panels(1) = "Rate: " & ani.Rate
y = ani.Rate * 100
End Sub

Private Sub mnuF7_Click()
If ani.Visible = True Then
  If y < 1000 Then y = y + 10: ani.Rate = y / 100: stbr.Panels(1) = "Rate: " & ani.Rate
  y = ani.Rate * 100
ElseIf img.Visible = True Then
  If mnuImage.Tag <> 1 Then Exit Sub
  z = z + 1
  If z > zMax Then z = zMax: Exit Sub
  If z = -1 Then z = 1
  imgClick
End If
End Sub

Private Sub mnuF7c_Click()
If ani.Visible = False Then Exit Sub
If y < 1000 Then y = y + 1: ani.Rate = y / 100: stbr.Panels(1) = "Rate: " & ani.Rate
y = ani.Rate * 100
End Sub

Private Sub mnuF8_Click()
If ani.Visible = True Then y = 100: ani.Rate = y / 100: stbr.Panels(1) = "Rate: " & ani.Rate: y = ani.Rate * 100
If img.Visible = True And mnuImage.Tag = 1 Then z = 1: imgClick
End Sub

Private Sub mnuFaveF_Click(Index As Integer)
On Error Resume Next
LoadFaves
If fso.FolderExists(Mid(mnuFaveF(Index).Caption, 1 + InStr(1, mnuFaveF(Index).Caption, " ", vbTextCompare))) = True Then
  fList.Path = Mid(mnuFaveF(Index).Caption, 1 + InStr(1, mnuFaveF(Index).Caption, " ", vbTextCompare))
  tvw_OpenPath fList.Path
  lvwLoad
Else
  MsgBox "Folder Not Found", vbInformation, "Folder Unavailable"
End If
End Sub

Private Sub mnuFaveFolder_Click()
Dim Index As Integer, i As Integer
Index = mnuFaveF.Count
For i = 1 To Index - 1
  If cmbHistory.Text = Mid(mnuFaveF(i).Caption, InStr(1, mnuFaveF(i).Caption, " ", vbTextCompare)) Then Exit For
Next
If i <> Index Then Exit Sub
If sh1.Visible = False Then sh1.Visible = True
Load mnuFaveF(Index)
mnuFaveF(Index).Visible = True
mnuFaveF(Index).Caption = "&" & Index & " " & cmbHistory.Text

'save favorite folders
cIni.Section = "Favorite Folders": cIni.DeleteSection
For i = 1 To mnuFaveF.Count - 1
  putINI "Favorite Folders", Mid(mnuFaveF(i).Caption, 1 + InStr(1, mnuFaveF(i).Caption, " ", vbTextCompare)), CStr(i)
Next
End Sub

Private Sub mnuFaveOrganize_Click()
putINI "Preferences", "Options Tab Index", 2
mnuSettings_Click
End Sub

Private Sub mnuFaveP_Click(Index As Integer)
strList = Mid(mnuFaveP(Index).Caption, 1 + InStr(1, mnuFaveP(Index).Caption, " ", vbTextCompare))
ClearLvw 2
lstLoad
End Sub

Private Sub mnuFavePlaylist_Click()
Dim Index As Integer, i As Integer
Index = mnuFaveP.Count
For i = 1 To Index - 1
  If strList = Mid(mnuFaveF(i).Caption, InStr(1, mnuFaveF(i).Caption, " ", vbTextCompare)) Then Exit For
Next
If i <> Index Then Exit Sub
If sh2.Visible = False Then sh2.Visible = True
Load mnuFaveP(Index)
mnuFaveP(Index).Visible = True
mnuFaveP(Index).Caption = "&" & Index & " " & strList

'save fave playlists
cIni.Section = "Favorite Files": cIni.DeleteSection
For i = 1 To mnuFaveP.Count - 1
  putINI "Favorite Files", Mid(mnuFaveP(i).Caption, 1 + InStr(1, mnuFaveP(i).Caption, " ", vbTextCompare)), CStr(i)
Next

End Sub

Private Sub mnuFileExplore_Click()
On Error Resume Next
Shell "explorer.exe /e," & fso.GetParentFolderName(lvw(IndView).SelectedItem.Key), vbMaximizedFocus
End Sub

Private Sub mnuFileLaunch_Click()
'change "Open" to "Explore" to bring up file explorer
If lvw(IndView).SelectedItem Is Nothing Then Exit Sub
ShellExecute 0, "Open", CurFile, "", "C:\", 1
End Sub

Private Sub mnuFileNotepad_Click()
Shell "notepad.exe " & CurFile, vbMaximizedFocus
End Sub

Private Sub mnuFileOpen_Click()
On Error Resume Next

Dim fn As String, multi As Variant
Dim strTit As String, strPls As String
Dim i As Integer, strExts As String

If IndView = 2 Then
  strTit = "Select File(s)/Playlist(s) to add to Playlist"
  strPls = "Playlist Files (*.m3u;*.lst)" & Chr(0) & "*.m3u;*.lst" & Chr(0)
  multi = OpenSaveFlag.OFN_ALLOWMULTISELECT Or OpenSaveFlag.OFN_EXPLORER
Else
  strTit = "Select File to Open"
End If

strExts = "All Media Files (Audio & Video)" & Chr(0) & extns("Audio").Extensions & extns("Video").Extensions
For i = 2 To extns.Count
  strExts = strExts & Chr(0)
  strExts = strExts & extns(i).ExtName & "(" & extns(i).Extensions & ")" & Chr(0) & extns(i).Extensions
Next

fn = ShowFileOpenSave(Me, True, strTit, _
  getINI("Preferences", "Open Dialog Folder", apPath), _
  "All files(*.*)" & Chr(0) & "*.*" & Chr(0) & _
  strPls & strExts, , , multi Or OpenSaveFlag.OFN_HIDEREADONLY)

If fn = "" Then Exit Sub

putINI "Preferences", "Open Dialog Folder", fso.GetParentFolderName(fn)
If multi <> 0 Then
  'add array to playlist
  Dim fool As String, fils As Variant
  If InStr(fn, vbNullChar) > 0 Then
    fool = Mid(fn, 1, InStr(fn, vbNullChar) - 1)
    fn = Mid(fn, InStr(fn, vbNullChar) + 1)
  End If
    fils = Split(fn, vbNullChar, , vbBinaryCompare)
  
  Dim strFil As String
  For i = LBound(fils) To UBound(fils)
    If fils(i) = "" Then Exit For
    If fool <> "" Then
      strFil = fool & "\" & CStr(fils(i))
    Else
      strFil = CStr(fils(i))
    End If
    
    If InStr(strPls, LCase(fso.GetExtensionName(strFil))) <> 0 Then
      strList = strFil
      lstLoad
    Else
      AddItem 2, strFil
    End If
  Next
Else
  'similar to code in lvw_OLEDragDrop & conViewer_OLEDragDrop
  LoadFol fso.GetParentFolderName(fn)
  lvw(1).ListItems(fn).Selected = True
  CrlClick , fn
End If

Exit Sub

errr:
If fList.ListCount = 0 Then
  fList.Pattern = "*"
End If

End Sub

Sub LoadFol(fol As String)
  'load File list (Explorer)
  fList.Path = fol
  If fList.ListCount = 0 Then cmbFilter.ListIndex = 0
  SetHistoryFolder fol
  
  lvwLoad
  
  If IndView <> 1 Then mnuView_Click 1
  
  If Not lvw(1).SelectedItem Is Nothing Then lvw(1).SelectedItem.Selected = False
End Sub

Private Sub mnuHelpAbout_Click()
Load frmSplash
frmSplash.lblStatus = "IViewer is absolutely FREE!!  �2001 No Rights Reserved."

frmSplash.Show vbModal

'"Warning: The author is in no way responsible for " & _
  "damage resulting from the use of this program."
End Sub

Private Sub mnuHelpTips_Click()
If fso.FileExists(apPath & "tips.txt") = False Then
  MsgBox "The file 'tips.txt' is not in the program folder." & vbCrLf & "  ('" & apPath & "')" & vbCrLf & _
    "To view Tips, kindly replace the file.", vbCritical, "File Not Found"
  'MsgBox "Sorry, could not load tips because 'tips.txt'" & vbCrLf & _
  "could not be found in the program directory.", vbCritical, "File Not Found"
Else
  Load frmTips: frmTips.Show vbModal
End If
End Sub

Private Sub mnuHorz_Click()
mnuHorz.Checked = Not mnuHorz.Checked

If mnuHorz.Checked Then TBar.Buttons("goh").Image = imTBar.ListImages("gov").Key Else TBar.Buttons("goh").Image = imTBar.ListImages("goh").Key

If mnuHorz.Checked = True Then
  vlin.Visible = False: hhlin.Visible = True
    'nav,view,hhlin
  conViewer.ZOrder 0
  conNavigator.ZOrder 0
  hhlin.ZOrder 0
Else
  vlin.Visible = True: hhlin.Visible = False
  cmbFilter.Left = 0
  lvw(1).Left = 0
    'view,nav,vlin
  conNavigator.ZOrder 0
  conViewer.ZOrder 0
  vlin.ZOrder 0
End If

hhsiz

mnuView_Click CInt(IndView)

putINI "Preferences", "Split Horizontally", mnuHorz.Checked
End Sub

Private Sub mnuImg_Click(Index As Integer)
Dim i As Integer
For i = 1 To 7: mnuImg(i).Checked = False: Next i
mnuImg(Index).Checked = True
mnuImage.Tag = Index
If Index = 1 Then
  mnuRZ.Caption = "&Magnification": mnuRZ.Visible = True
  TBar.Buttons("inc").Visible = True: TBar.Buttons("dec").Visible = True: TBar.Buttons("reset").Visible = True
    TBar.Buttons("inc").Tag = "Size +": TBar.Buttons("inc").ToolTipText = "Increase Size"
    TBar.Buttons("dec").Tag = "Size -": TBar.Buttons("dec").ToolTipText = "Decrease Size"
    TBar.Buttons("reset").Tag = "Reset": TBar.Buttons("reset").ToolTipText = "Reset Size"
  If TBar.Buttons(1).Caption <> "" Then
    TBar.Buttons("inc").Caption = "Size +": TBar.Buttons("dec").Caption = "Size -": TBar.Buttons("reset").Caption = "Reset"
  End If
  TBar.Buttons("inc").Image = imTBar.ListImages("inc").Index
  TBar.Buttons("dec").Image = imTBar.ListImages("dec").Index
  TBar.Buttons("reset").Image = imTBar.ListImages("reset").Index
  TBar.Refresh
Else
  mnuRZ.Visible = False
  TBar.Buttons("inc").Visible = False: TBar.Buttons("dec").Visible = False: TBar.Buttons("reset").Visible = False
End If

ChangeMode: mnuImage.Visible = True: mnuFullScreen.Visible = True 'coz of browser also being an option
If Index > 6 Then
  browser.Visible = True: img.Visible = False: stbr.Panels(1) = ""
  mnuFullScreen.Enabled = False
Else
  img.Visible = True: browser.Visible = False
  stbr.Panels(1) = "Zoom:": mnuFullScreen.Enabled = True
End If

CrlClick
End Sub

Private Sub imgClick()
stbr.Panels(1) = ""  'so we can load zoom at end if not done during this sub

'if fullscreen then load new img in frmfscr & exit sub

Dim r As Single, zo As String, mh As Integer, mw As Integer 'mh & mw are max ht + wd resp. zo is zoom

r = img.Picture.Height / img.Picture.Width
If r >= 1 Then zMax = Int(65536 / img.Picture.Height) Else zMax = Int(65536 / img.Picture.Width)

mh = conViewer.Height: mw = conViewer.Width
hs.Visible = False: vs.Visible = False: iv.Visible = False

'zMax = 3
TBar.Buttons("inc").ToolTipText = "Increase Size upto " & zMax & ":1"


If mh = 0 Or mw = 0 Or fso.FileExists(CurFile) = False Or img.Picture = 0 Then Exit Sub

If mnuFullScreen.Checked Then
  Load frmFScr
  If mnuImg(6).Checked = True Then
    frmFScr.loadTiled lvw(IndView).SelectedItem.Key
  Else
    frmFScr.loadImg lvw(IndView).SelectedItem.Key
  End If
  frmFScr.Show
  Exit Sub
End If

img.Visible = False

Select Case mnuImg(mnuImage.Tag).Caption

Case "&Variable"
  If z >= 0 Then img.Width = img.Picture.Width * z / 1: img.Height = img.Picture.Height * z / 1: stbr.Panels(1) = "1:" & z
  If z < 0 Then img.Width = img.Picture.Width / -z / 1: img.Height = img.Picture.Height / -z / 1: stbr.Panels(1) = -z & ":1"
  If img.Width > mw + 500 Then mh = iv.Top
  If img.Height > mh + 500 Then mw = iv.Left
  If img.Width > mw + 500 Then mh = iv.Top

Case "Fit &Height"
  img.Height = mh: img.Top = 0
  img.Width = img.Height / r
  If img.Width > mw + 500 Then
    mh = iv.Top
    hs.Visible = True
    img.Height = mh: img.Top = 0
    img.Width = img.Height / r
  End If

Case "Fit &Width"
  img.Width = mw: img.Left = 0
  img.Height = img.Width * r
  If img.Height > mh + 500 Then
    mw = iv.Left
    vs.Visible = True
    img.Width = mw: img.Left = 0
    img.Height = img.Width * r
  End If

Case "&Fit Both"
  If r > mh / mw Then
    img.Height = mh
    img.Width = img.Height / r
    zo = img.Width / img.Picture.Width
  Else
    img.Width = mw
    img.Height = img.Width * r
  End If

Case "&Stretch Fit"
  img.Left = 0: img.Top = 0
  img.Width = mw: img.Height = mh
  stbr.Panels(1) = "H: " & Format(img.Height / img.Picture.Height, "##.###") & _
    " W: " & Format(img.Width / img.Picture.Width, "##.###")

Case "&Tiled"

  Dim x As Integer, y As Integer ', D As Integer
  Dim PatternHeight As Integer, PatternWidth As Integer
  
  conViewer.ScaleMode = 3
  conViewer.Cls
  img.Stretch = False
  PatternHeight = img.Height       'hard-coded value
  PatternWidth = img.Width        'hard-coded value
  img.Visible = False
  conViewer.Picture = LoadPicture(CurFile)
  
  For x = 0 To conViewer.Width Step PatternWidth
    For y = 0 To conViewer.Height Step PatternHeight
      BitBlt conViewer.hdc, x, y, PatternWidth, PatternHeight, conViewer.hdc, 0, 0, &HCC0020  '=SRCCOPY)
    Next y
  Next x
  img.Stretch = True
  conViewer.ScaleMode = 1
  stbr.Panels(1) = ""
  Exit Sub

Case Else
  'MsgBox mnuImg(mnuImage.Tag).Caption
End Select

'scrollbars & centering
If img.Width > mw + 500 Then
  hs.Visible = True
  hs.max = Int(img.Width / 1000) - Int(mw / 1000)
  hs.Value = Int(hs.max / 2): hs_Change
Else
  img.Left = (mw - img.Width) / 2
End If

If img.Height > mh + 500 Then
  vs.Visible = True
  vs.max = Int(img.Height / 1000) - Int(mh / 1000)
  vs.Value = Int(vs.max / 2): vs_Change
Else
  img.Top = (mh - img.Height) / 2
End If

If hs.Visible And vs.Visible = False Then hs.Width = conViewer.Width
If vs.Visible And hs.Visible = False Then vs.Height = conViewer.Height
If vs.Visible And hs.Visible Then vs.Height = iv.Top: hs.Width = iv.Left: iv.Visible = True

img.Visible = True

'zoom in status bar

If stbr.Panels(1) = "" Then
  zo = img.Height / img.Picture.Height
  stbr.Panels(1) = "Zoom: " & Format(zo, "##.####")
End If
End Sub

Private Sub mnuCall_Click()
  If ind = 2 Then Call PopupMenu(mnuImage)
  If ind = 3 Then Call PopupMenu(mnuMedia)
  If ind = 4 Then Call PopupMenu(mnuText)
End Sub

Private Sub mnuClear_Click()
ani.FileName = ""
DoEvents
If ani.Visible Then ani_Center
DoEvents
If img.Visible = True Then
  If fso.FileExists(apPath & "noimg.gif") = True Then
    img.Picture = LoadPicture(apPath & "noimg.gif")
    zMax = Int(32768 / img.Picture.Height)
    imgClick
  Else: Set img = LoadPicture("")
  End If
End If
CurFile = ""
DoEvents
rtb.LoadFile "": rtb.Text = ""
DoEvents
browser.Navigate (apPath & "blank.htm")
'browser.Document.body.innerhtml = "<html><body><font color=409040><center> <h1>There is no file to view.</h1> </font></center></body></html>"
DoEvents
Me.Caption = apComp & " " & apProd & " - "
End Sub


Private Sub mnuExit_Click()
On Error Resume Next

Dim mn As Long
mn = getINI("Application Start", "Life Timer", 0)
mn = mn + DateDiff("n", timStart, Now)
putINI "Application Start", "Life Timer", CStr(mn)

'lvw & lst view, visiblity & width options
Dim dList 'As ListView
Dim i As Integer, sw As String, sv As String
For Each dList In lvw
  For i = 1 To dList.ColumnHeaders.Count - 1
  sv = "lvw" & dList.Index & " visible " & dList.ColumnHeaders(i).Text
  sw = "lvw" & dList.Index & " width " & dList.ColumnHeaders(i).Text
  If dList.ColumnHeaders(i).Width = 0 Then
    putINI "File List Columns", sv, False
    putINI "File List Columns", sw, dList.ColumnHeaders(i).Tag
  Else
    putINI "File List Columns", sv, True
    putINI "File List Columns", sw, dList.ColumnHeaders(i).Width
  End If
  Next i
Next

'save default filelist
If True = getINI("Application Start", "Remember Filelist", True) Then 'srirams idea to not load filelists
  Dim tim As String, tmpTim As String
  Set ts = fso.CreateTextFile(apPath & "iviewer.lst", True)
  If bolHasMedia(2) = True Then ts.WriteLine "#EXTM3U"
    For Each itm In lvw(2).ListItems
      If itm.SubItems(4) = "" Then GoTo Edn
      tim = itm.SubItems(4)
      tmpTim = Val(Mid(tim, 1, Len(tim) - 3)) * 60 + Val(Mid(tim, Len(tim) - 1, 2))
      ts.WriteLine "#EXTINF:" & tmpTim & "," & fso.GetBaseName(itm.Text)
Edn:
      ts.WriteLine itm.Key
      
    Next
    ts.Close
End If

If ani.PlayState <> mpClosed Or ani.PlayState <> mpStopped Then ani.Stop
DoEvents

End
End Sub

Public Sub mnuFullScreen_Click()
On Error Resume Next
mnuFullScreen.Checked = Not mnuFullScreen.Checked
If mnuFullScreen.Checked Then
  Load frmFScr
  frmFScr.Show
  If mnuImg(6).Checked = True Then
    frmFScr.loadTiled lvw(IndView).SelectedItem.Key
  Else
    frmFScr.loadImg lvw(IndView).SelectedItem.Key
  End If
Else
  Unload frmFScr
End If
End Sub

Private Sub mnuJump_Click()
Load frmJump
frmJump.DoJump lvw(IndView)
frmJump.Show vbModal

End Sub

Private Sub mnuJumpFolder_Click()
Load frmJumpFolder
frmJumpFolder.Show vbModal
End Sub

Private Sub mnuListAdd_Click()
Dim indCH As Integer, lstitm As ListItem
  indCH = lvw(2).ColumnHeaders.Count - 1
On Error Resume Next
For Each itm In lvw(IndView).ListItems
  If itm.Selected = True Then  'mustnt already be there
  If IsError(lvw(2).ListItems(itm.Key)) = True Then 'doesnt already exist
    Set lstitm = lvw(2).ListItems.Add(lvw(2).ListItems.Count + 1, itm.Key, itm.Text, itm.Icon, itm.SmallIcon)
    lstitm.SubItems(1) = itm.SubItems(1)
    lstitm.SubItems(2) = itm.SubItems(2)
    lstitm.SubItems(3) = itm.SubItems(3)
    lstitm.SubItems(4) = itm.SubItems(4)
    If IndView = 1 Then
      lstitm.SubItems(5) = lvw(1).Tag   'add fol
      lstitm.SubItems(6) = itm.SubItems(5) 'add bytes
    Else
      lstitm.SubItems(5) = itm.SubItems(5) 'add fol
      lstitm.SubItems(6) = itm.SubItems(6) 'add bytes
    End If
  End If
  End If
Next
End Sub

Private Sub mnuListDelete_Click()

If viTrashOpt = 0 Then Exit Sub
'im not sure this is working properly 6:05 AM 7th May 2003
On Error GoTo sortErr:
Dim m As VbMsgBoxResult, rf As Integer, strMsg As String, i As Integer
Dim dFiles() As String

ReDim dFiles(0)
'see how many items are selected
For Each itm In lvw(1).ListItems
  If itm.Selected = True Then
    ReDim Preserve dFiles(UBound(dFiles) + 1)
    dFiles(UBound(dFiles)) = itm.Key
  End If
Next

If UBound(dFiles) = 1 Then
  strMsg = vbCrLf & dFiles(1) & vbCrLf
ElseIf UBound(dFiles) > 1 Then
  strMsg = " the selected " & UBound(dFiles) & " files "
Else
  Exit Sub
End If

'rf = lvw(1).SelectedItem.Index

If viTrashOpt = 1 Then 'Delete Without Recovery
  If True = getINI("Trash", "Trash Confirm", True) Then
    m = MsgBox("Sure you wanna delete" & strMsg, vbYesNo + vbQuestion)
    If m = vbNo Then Exit Sub
  End If
  mnuClear_Click
  For i = 1 To UBound(dFiles)
    fso.DeleteFile (dFiles(i))
    lvw(1).ListItems.Remove (lvw(1).ListItems(dFiles(i)).Index)
  Next
ElseIf viTrashOpt = 2 Then 'Recycle Bin
  If True = getINI("Trash", "Trash Confirm", True) Then
    m = MsgBox("Sure you wanna delete" & strMsg & "to Windows Recycle Bin", vbYesNo + vbQuestion)
    If m = vbNo Then Exit Sub
  End If
  mnuClear_Click
  Dim FileDelOp As SHFILEOPSTRUCT
  FileDelOp.wFunc = &H3 'FO_DELETE
  FileDelOp.fFlags = &H40 Or &H10 'FOF_ALLOWUNDO or FOF_NOCONFIRMATION
  For i = 1 To UBound(dFiles)
    FileDelOp.pFrom = dFiles(i)
    SHFileOperation FileDelOp 'itll ask for a confirm every time
    lvw(1).ListItems.Remove (lvw(1).ListItems(dFiles(i)).Index)
  Next
ElseIf viTrashOpt = 3 Then 'Trash Can
  If fso.FolderExists(viTrashLoc) = False Then fso.CreateFolder (viTrashLoc)
  If True = getINI("Trash", "Trash Confirm", True) Then
    m = MsgBox("Sure you wanna move" & strMsg & "to Trash Can" & vbCrLf & " (" & viTrashLoc & ")", vbYesNo + vbQuestion)
    If m = vbNo Then Exit Sub
  End If
  mnuClear_Click
  For i = 1 To UBound(dFiles)
    fso.MoveFile dFiles(i), viTrashLoc & "\" & fso.GetFileName(dFiles(i))
    lvw(1).ListItems.Remove (lvw(1).ListItems(dFiles(i)).Index)
  Next
Else
End If

  'If lvw(1).ListItems.Count = 0 Then Exit Sub  'if deleting last item
  'If rf = lvw(1).ListItems.Count + 1 Then lvw(1).ListItems(rf - 1).Selected = True Else lvw(1).ListItems(rf).Selected = True 'if item is at end of list
  'CrlClick

Exit Sub

sortErr:
If Err = 58 Then
  m = MsgBox("File already exists in Trash Can." & vbCrLf & "Open Trash Can to view file?", vbYesNo)
  If m = vbYes Then Shell "explorer.exe /e," & getINI("Trash", "Trash Location", apPath & "trash"), vbMaximizedFocus
Else
  MsgBox "Error # " & Err & " " & Err.Description
End If
End Sub

Sub lstLoad()
On Error Resume Next
If fso.FileExists(strList) = False Then Exit Sub

Dim lin As String
Set ts = fso.OpenTextFile(strList, ForReading)

Dim mesg As Integer, tmpTim As String
  tmpTim = ""

Dim j As Integer, a$

Do While Not ts.AtEndOfStream
  lin = ts.ReadLine
  
  If fso.FileExists(lin) Then
    Set fil = fso.GetFile(lin)
  ElseIf Mid(lin, 1, 8) = "#EXTINF:" Then    'if m3u & time is given
    j = InStr(lin, ",")
    If j <> 0 Then tmpTim = Mid(lin, 9, j - 9):  GoTo lp Else tmpTim = ""
  ElseIf lin = "#EXTM3U" Then
    GoTo lp
  Else
    If mesg = 1 Or mesg = 3 Then GoTo Iff
    Msg.Message = lin
    Msg.Show vbModal
    mesg = Msg.iMsg
    If mesg = 0 Then Exit Sub
  End If

Iff:
  If fso.FileExists(lin) = False Then
    If mesg = 3 Or mesg = 4 Then GoTo lp
    AddItem 2, lin
    GoTo lp
  End If
  
  Set itm = AddItem(2, lin)
  If tmpTim <> "" Then
    If Int(tmpTim Mod 60) < 10 Then a$ = ":0" Else a$ = ":"
    itm.SubItems(4) = Int(tmpTim / 60) & a$ & Int(tmpTim Mod 60)
    tmpTim = ""
  End If

lp:
Loop
ts.Close
If Msg.iMsg <> -1 Then Unload Msg
End Sub

Private Sub mnuListReturn_Click()
'first see if it is in playlist
If fso.FileExists(CurFile) = False Then MsgBox "File is no longer available", vbExclamation: Exit Sub

Dim itm As ListItem
On Error Resume Next
Set itm = lvw(2).ListItems(CurFile)
If itm Is Nothing Then
  If IndView <> 1 Then mnuLvwView_Click 1
  fList.Path = fso.GetParentFolderName(CurFile)
  tvw_OpenPath fList.Path
  lvwLoad
  Set itm = Nothing
  Set itm = lvw(1).ListItems(CurFile)
  If itm Is Nothing Then
    'set filter to all & try again
    cmbFilter.ListIndex = 0
    Set itm = lvw(1).ListItems(CurFile)
    If itm Is Nothing Then MsgBox "Couldnt find file. - unknown error", vbCritical: Exit Sub
    If Not lvw(1).SelectedItem Is Nothing Then lvw(1).SelectedItem.Selected = False
    lvw(1).ListItems(CurFile).Selected = True
  Else
    If Not lvw(1).SelectedItem Is Nothing Then lvw(1).SelectedItem.Selected = False
    lvw(1).ListItems(CurFile).Selected = True
  End If
  lvw(2).ListItems(CurFile).EnsureVisible
Else
  If IndView <> 2 Then mnuView_Click 2
  If Not lvw(2).SelectedItem Is Nothing Then lvw(2).SelectedItem.Selected = False
  lvw(2).ListItems(CurFile).Selected = True
  lvw(2).ListItems(CurFile).EnsureVisible
End If
End Sub

Private Sub mnuLvwArrange_Click(Index As Integer)
  SortColumn lvw(IndView), lvw(IndView).ColumnHeaders(Index), 3
End Sub

Private Sub mnuLvwColumn_Click(Index As Integer)
On Error Resume Next
mnuLvwColumn(Index).Checked = Not mnuLvwColumn(Index).Checked

If mnuLvwColumn(Index).Checked Then
  lvw(IndView).ColumnHeaders(Index).Width = lvw(IndView).ColumnHeaders(Index).Tag
Else
  lvw(IndView).ColumnHeaders(Index).Tag = lvw(IndView).ColumnHeaders(Index).Width
  lvw(IndView).ColumnHeaders(Index).Width = 0
End If
End Sub

Private Sub mnuLvwShuffle_Click()
Dim max As Integer, tot As Integer, n As Integer
tot = lvw(IndView).ListItems.Count - 1
max = 1.5 * tot
Randomize
For n = 1 To max
  Swap CInt(Rnd * tot) + 1, CInt(Rnd * tot) + 1, IndView
Next
End Sub

Private Sub mnuLvwView_Click(Index As Integer)
lvw(1).View = Index
lvw(2).View = Index
lvw(3).View = Index

mnuLvwView(0).Checked = False: mnuLvwView(1).Checked = False
  mnuLvwView(2).Checked = False: mnuLvwView(3).Checked = False
mnuLvwView(Index).Checked = True

putINI "File List", "View", lvw(1).View
End Sub

Private Sub mnuMed_Click(Index As Integer)
ani.DisplaySize = Index
Dim i As Integer
For i = 0 To 7
mnuMed(i).Checked = False
Next i
mnuMed(Index).Checked = True
mnuMedia.Tag = Index
ani_Center
putINI "Preferences", "Media Size", ani.DisplaySize
End Sub

'media
Private Sub mnuMediaMute_Click()
If ani.FileName = "" Then Exit Sub
If ani.Mute = True Then mnuMediaMute.Checked = False: ani.Mute = False Else mnuMediaMute.Checked = True: ani.Mute = True
End Sub

Private Sub mnuMediaPause_Click()
If ani.FileName = "" Then Exit Sub
If ani.PlayState = mpPaused Or ani.PlayState = mpStopped Then mnuMediaPause.Checked = False: ani.Play Else mnuMediaPause.Checked = True: ani.Pause
End Sub

Private Sub mnuMediaRepeat_Click()
mnuMediaRepeat.Checked = Not mnuMediaRepeat.Checked
If mnuMediaRepeat.Checked Then TBar.Buttons("repeat").Value = tbrPressed Else TBar.Buttons("repeat").Value = tbrUnpressed
End Sub

Private Sub mnuMediaReplay_Click()
If fso.FileExists(ani.FileName) Then ani.FileName = ani.FileName Else CrlClick
'If fso.FileExists(ani.FileName) Then ani.Play
End Sub

Private Sub mnuMediaStop_Click()
If ani.FileName = "" Then Exit Sub
If ani.PlayState <> mpStopped Then ani.Stop ': ani.CurrentPosition = 0
End Sub

Private Sub mnuMediaVolDec_Click()
If ani.Volume < -3200 Then ani.Volume = -3300 Else ani.Volume = ani.Volume - 100
End Sub

Private Sub mnuMediaVolInc_Click()
If ani.Volume > -100 Then ani.Volume = 0 Else ani.Volume = ani.Volume + 100
End Sub

Private Sub mnuMedMoveBack_Click()
On Error Resume Next
Dim bP As Boolean
If ani.PlayState = mpPlaying Then bP = True: ani.Pause
If ani.PlayState = mpPaused Then ani.CurrentPosition = ani.CurrentPosition - MStep
If bP Then ani.Play
End Sub

Private Sub mnuMedMoveFwd_Click()
On Error Resume Next
If ani.PlayState = mpPlaying Or ani.PlayState = mpPaused Then ani.CurrentPosition = ani.CurrentPosition + MStep
End Sub

Private Sub mnuMedMoveStep_Click()
Dim ip As String
ip = InputBox("What step should it move in (seconds)." & vbCrLf & "(For MoveFwd & MoveBack)", "Fwd/Back Step", MStep)
If IsNumeric(ip) Then
  MStep = Val(ip)
  putINI "Preferences", "Media Move Step", CStr(MStep)
End If
End Sub

Private Sub mnuMedMoveTo_Click()
Dim ip As String
ip = InputBox("What position should it move to (seconds)." & vbCrLf & "Just say +/- 5 if it should go fwd/back 5 seconds.", "Jump to position:", ani.CurrentPosition)
If IsNumeric(ip) Then
  If Mid(ip, 1, 1) = "+" Or Mid(ip, 1, 1) = "-" Then ani.CurrentPosition = ani.CurrentPosition + Val(ip) Else ani.CurrentPosition = ip
ElseIf InStr(ip, ":") <> 0 Then
  Dim nt() As String
  nt = Split(ip, ":")
  ani.CurrentPosition = 60 * Val(nt(0)) + Val(nt(1))
End If
End Sub

Private Sub mnuNext_Click()
  CrlNext
End Sub

Private Sub mnuOptExtns_Click()
putINI "Preferences", "Options Tab Index", 5
frmOptions.Show vbModal
End Sub

Private Sub mnuOptionsClipboard_Click()
Clipboard.Clear

Dim lMax As Integer
If IndView <> 2 Then
  For Each itm In lvw(IndView).ListItems
  If Len(itm.Text) > lMax Then lMax = Len(itm)
  Next
  lMax = Int(lMax / 8) + 1
      Dim cnt As Integer
  For Each itm In lvw(IndView).ListItems
  Clipboard.SetText (Clipboard.GetText & itm)
    If lvw(IndView).View = lvwReport Then
      cnt = Int(Len(itm) / 8)
      Do Until cnt = lMax
      Clipboard.SetText (Clipboard.GetText & Chr(9))
      cnt = cnt + 1
      Loop
      Clipboard.SetText (Clipboard.GetText & itm.SubItems(1))
      Clipboard.SetText (Clipboard.GetText & Chr(9) & itm.SubItems(2))
    End If
  Clipboard.SetText (Clipboard.GetText & vbCrLf)
  Next
Else
  Dim i As Integer
  For i = 1 To lvw(2).ListItems.Count
    If lvw(2).View = lvwReport Then
      Clipboard.SetText (Clipboard.GetText & lvw(2).ListItems(i).ListSubItems(3).Text & lvw(2).ListItems(i).Text)
    Else
      Clipboard.SetText (Clipboard.GetText & lvw(2).ListItems(i).Text)
    End If
  Clipboard.SetText (Clipboard.GetText & vbCrLf)
  Next i
End If

MsgBox "Copied file information to Clipboard successfully.", vbInformation, "File Information Copy Completion"
End Sub

Private Sub mnuOptionsGenerate_Click()
Dim m As VbMsgBoxResult
m = MsgBox("Proceed only if files in current playlist/searchlist are sorted by folder." & vbCrLf & _
  "Do this either by clicking on the column 'In Folder' or" & vbCrLf & _
  "by right-clicking & selecting 'Arrange Icons/by Folder'" & vbCrLf & _
  "Do you still wish to proceed? ", vbYesNo + vbQuestion, "Generate Playlist Confirmation")

If m = vbNo Then Exit Sub

Clipboard.Clear

Dim strFold As String, strGen As String, strTemp As String, ln As Integer
'see if we're generating from playlist or searchlist

For Each itm In lvw(IndView).ListItems
  If itm.SubItems(3) <> strFold Then
    strGen = strGen & vbCrLf & itm.SubItems(3) & vbCrLf
      ln = Len(itm.SubItems(3))
      Do Until Len(strTemp) = ln
        strTemp = strTemp & "="
      Loop
      strGen = strGen & strTemp & vbCrLf & vbCrLf
      strTemp = ""
    strFold = itm.SubItems(3)
  End If
  strGen = strGen & itm.Text & vbCrLf
Next

Clipboard.Clear
Clipboard.SetText strGen

MsgBox "Playlist generated and copied successfully to clipboard.", vbInformation, "Playlist Generation."
End Sub

Private Sub mnuOptionsLyricsTrain_Click()
mnuOptionsLyricsTrain.Checked = Not mnuOptionsLyricsTrain.Checked
If mnuOptionsLyricsTrain.Checked And IndView <> 2 Then
  'warn about shifting to playlist...
  Dim m As VbMsgBoxResult
  m = MsgBox("Clear Playlist & shift?", vbOKCancel, "Lyrics Trainer")
  If m = vbCancel Then mnuOptionsLyricsTrain.Checked = False: Exit Sub
  lvw(2).ListItems.Clear
  lvw(2).Tag = IndView
  For Each itm In lvw(IndView).ListItems
    AddItem 2, itm.Key
  Next
  If Not lvw(IndView).SelectedItem Is Nothing Then
    On Error Resume Next
    lvw(2).ListItems(lvw(IndView).SelectedItem.Key).Selected = True
  End If
  lvw(IndView).ListItems.Clear
End If
'If Not mnuOptionsLyricsTrain.Checked Then
 
End Sub

Private Sub mnuOptionsRunLyriks_Click()
Shell GetSetting(apComp, "Lyriks", "Lyriks EXE File", ""), vbMaximizedFocus
End Sub

Private Sub mnuOptionsShowLyrics_Click()
mnuOptionsShowLyrics.Checked = Not mnuOptionsShowLyrics.Checked
putINI "Preferences", "Show Lyrics", mnuOptionsShowLyrics.Checked
ani_NewStream
'move all current files to playlist (if iInd= 1 or 3)
End Sub

Private Sub mnuOptionsTLib_Click()
frmTLib.Show
End Sub

Private Sub mnuPlaylistCrop_Click()
  If IndView <> 2 Then Exit Sub
  lvw_KeyDown 2, vbKeyDelete, 1
End Sub

Private Sub mnuPlaylistLoad_Click()
Dim fn As String
fn = ShowFileOpenSave(Me, True, "Select a Playlist to Load", _
  getINI("Preferences", "Open Dialog Folder", apPath), _
  "Playlist files (*.lst;*.m3u)" & Chr(0) & "*.lst;*.m3u" & Chr(0) & _
  "All files(*.*)" & Chr(0) & "*.*", , , OpenSaveFlag.OFN_HIDEREADONLY)

If fn = "" Then Exit Sub

putINI "Preferences", "Open Dialog Folder", fso.GetParentFolderName(fn)

'appending is done by adding playlist to playlist
ClearLvw 2

strList = fn
lstLoad

If mnuView(2).Checked = False Then mnuView_Click 2
End Sub

Private Sub mnuPlaylistMoveDown_Click()
If IndView <> 2 Then Exit Sub

Dim r As Integer
r = lvw(2).ListItems.Count - 1
Do Until r = 0
  If lvw(2).ListItems(r).Selected = True And lvw(2).ListItems(r + 1).Selected = False Then
    Swap r, r + 1, 2
    lvw(2).ListItems(r).Selected = False
    lvw(2).ListItems(r + 1).Selected = True
  End If
r = r - 1
Loop
End Sub

Private Sub mnuPlaylistMoveUp_Click()
If IndView <> 2 Then Exit Sub

Dim r As Integer
r = 2

Do Until r > lvw(2).ListItems.Count
  If lvw(2).ListItems(r).Selected = True And lvw(2).ListItems(r - 1).Selected = False Then
    Swap r, r - 1, 2
    lvw(2).ListItems(r).Selected = False
    lvw(2).ListItems(r - 1).Selected = True
  End If
r = r + 1
Loop
End Sub

Sub Swap(j As Integer, k As Integer, Index As Byte)
'maybe its possible to set virtual item and delete original & add at other side of one to be swapped with
If k = j Then Exit Sub
Dim i As Integer, tmp As String, tmpKey As String

'swap texts
tmp = lvw(Index).ListItems(j).Text
lvw(Index).ListItems(j).Text = lvw(Index).ListItems(k).Text
lvw(Index).ListItems(k).Text = tmp

'swap subitems
For i = 1 To lvw(Index).ColumnHeaders.Count - 1
  tmp = lvw(Index).ListItems(j).SubItems(i)
  lvw(Index).ListItems(j).SubItems(i) = lvw(Index).ListItems(k).SubItems(i)
  lvw(Index).ListItems(k).SubItems(i) = tmp
Next i

'swap icons
tmp = lvw(Index).ListItems(j).Icon
  lvw(Index).ListItems(j).Icon = lvw(Index).ListItems(k).Icon
  lvw(Index).ListItems(k).Icon = CInt(tmp)
tmp = lvw(Index).ListItems(j).SmallIcon
  lvw(Index).ListItems(j).SmallIcon = lvw(Index).ListItems(k).SmallIcon
  lvw(Index).ListItems(k).SmallIcon = CInt(tmp)

'swap keys
tmp = lvw(Index).ListItems(j).Key
  tmpKey = lvw(Index).ListItems(k).Key: lvw(Index).ListItems(k).Key = "temporary key"
lvw(Index).ListItems(j).Key = tmpKey
lvw(Index).ListItems(k).Key = tmp

End Sub

Private Sub mnuPlaylistNew_Click()
ClearLvw 2: strList = ""
strList = ""
End Sub

Private Sub mnuPlaylistRemove_Click()
  If IndView <> 1 Then lvw_KeyDown Val(ind), vbKeyDelete, 0
End Sub

Private Sub mnuPlaylistRemoveMissing_Click()
Dim i As Integer, strIcon As String
i = 1

Do Until i > lvw(2).ListItems.Count
  If fso.FileExists(lvw(2).ListItems(i).Key) = False Then
    lvw(2).ListItems.Remove (i)
  Else
    If lvw(2).ListItems(i).Icon = 1 Then
      strIcon = GetFilIcon(lvw(2).ListItems(i).Key)
      lvw(2).ListItems(i).Icon = icoLarge.ListImages(strIcon).Index
      lvw(2).ListItems(i).SmallIcon = icoSmall.ListImages(strIcon).Index
    End If
    i = i + 1
  End If
Loop

End Sub

Private Sub mnuPlaylistSave_Click()

'MsgBox "Check mnuPlaylistSave_Click"

On Error Resume Next
Dim lstFil As File, filt As String, slst As String, fn As String
If fso.FileExists(strList) Then Set lstFil = fso.GetFile(strList)

Dim m As VbMsgBoxResult, fNam As String
If fso.FileExists(strList) Then

  m = MsgBox("Do you want to overwrite '" & lstFil.Path & "'?", vbYesNoCancel + vbQuestion)
  If m = vbCancel Then Exit Sub
  If m = vbNo Then GoTo Sav
  fNam = lstFil.Path
Else
Sav:
  If bolHasMedia(IndView) = True Then filt = "M3U files (*.m3u)" & Chr(0) & "*.m3u" & Chr(0) & "All files (*.*)" & Chr(0) & "*.*" Else filt = "List files" & Chr(0) & "*.lst" & Chr(0) & "All files (*.*)" & Chr(0) & "*.*"
  If fso.FileExists(strList) Then slst = strList Else slst = apPath
  
  fn = ShowFileOpenSave(Me, False, "Save Playlist", slst, filt) 'OpenSaveFlag.OFN_HIDEREADONLY
  If fn <> "" Then fNam = fn Else Exit Sub
  
End If

Dim itm As ListItem, tim As String, tmpTim As String
Set ts = fso.CreateTextFile(fNam, True)
strList = fNam
      'from mnuexit since adding time support (m3u reading)
'If ind = 3 And lst(3).ListItems.Count > 0 Then ts.WriteLine "#EXTM3U"
For Each itm In lvw(2).ListItems
  If itm.SubItems(4) <> "" Then
    tim = itm.SubItems(4)
    If tim = "" Then GoTo Edn
    tmpTim = Val(Mid(tim, 1, Len(tim) - 3)) * 60 + Val(Mid(tim, Len(tim) - 1, 2))
    ts.WriteLine "#EXTINF:" & tmpTim & "," & fso.GetBaseName(itm.Text)
Edn:
  End If
  ts.WriteLine itm.Key
Next

ts.Close
End Sub

Private Sub mnuRefresh_Click()
On Error Resume Next

If IndView <> 1 Then
  lvw(IndView).Refresh
Else
  If Me.ActiveControl.Name = "tvw" Then 'refresh treeview
    Tvw_Refresh
    
    'this was in Tvw_Refresh but moved here & to part of form_load after loading history to avoid loading trouble
    If fso.FolderExists(cmbHistory.Text) Then fList.Path = cmbHistory.Text Else SetHistoryFolder fList.Path
    tvw_OpenPath cmbHistory.Text: lvwLoad
  End If
  
  Dim rf As Integer
  rf = lvw(1).SelectedItem.Index
  fList.Refresh
  lvwLoad
  'If lvw(1).ListItems.Count = 0 Then Exit Sub
  'If lvw(1).ListItems.Count < rf Then lvw(1).ListItems.Item(lvw(1).ListItems.Count).Selected = True Else lvw(1).ListItems.Item(lvw(1).ListItems.Count).Selected = True: lvw(1).ListItems.Item(rf).Selected = True
    'must unselect just like win exp
  If lvw(1).SelectedItem Is Nothing Then Else lvw(1).SelectedItem.Selected = False
End If
End Sub

Private Sub mnuSettings_Click()
  frmOptions.Show vbModal
End Sub

Private Sub mnuShuffle_Click()
mnuShuffle.Checked = Not mnuShuffle.Checked
If mnuShuffle.Checked And viReorderAsShuffle Then 'move current to top
  Swap 1, lvw(IndView).SelectedItem.Index, IndView
  lvw(IndView).SelectedItem.Selected = False
  lvw(IndView).ListItems(1).Selected = True
  Set lvw(IndView).SelectedItem = lvw(IndView).ListItems(1)
End If
End Sub

Private Sub mnuTextSearch_Click()
Load frmSearch
frmSearch.Show
frmSearch.txt.Text = rtb.SelText
End Sub

Public Sub doTBarCaption() 'public to call from options dlg
Dim bolCap As Boolean
If True = getINI("Preferences", "Toolbar Caption", "True") Then bolCap = True

Dim i As Integer
For i = 1 To TBar.Buttons.Count
If bolCap Then TBar.Buttons(i).Caption = TBar.Buttons(i).Tag Else TBar.Buttons(i).Caption = ""
Next

DoEvents
If TBar.Visible Then TBar_Resiz
End Sub

Private Sub mnuToolbarView_Click()
mnuToolbarView.Checked = Not mnuToolbarView.Checked
If mnuToolbarView.Checked = True Then TBar.Visible = True Else TBar.Visible = False
DoEvents
TBar_Resiz
putINI "Preferences", "Show Toolbar", mnuToolbarView.Checked
End Sub

Public Sub TrashChk(Locn As String)
If InStr(Locn, "\") = 0 Then Locn = apPath & Locn 'locn is folder name in appath
If fso.FolderExists(Locn) Then Exit Sub

Dim m As VbMsgBoxResult
m = MsgBox(Locn & vbCrLf & "could not be found." & vbCrLf & "Do you want it to be created?", vbYesNo + vbQuestion)

If m = vbYes Then
  fso.CreateFolder (Locn)
  If fso.FolderExists(Locn) Then
    MsgBox "'" & viTrashLoc & "' created successfully."
  Else
    MsgBox viTrashLoc & " doesnt seem to be a valid folder name. Click ok to revert to the last saved location."
    viTrashLoc = getINI("Trash", "Trash Location", apPath & "Trash")
  End If
End If
End Sub


Private Sub mnuTxtSize_Click()
Dim ip As String
ip = InputBox("Enter Text Size (must be between 6 and 72)", , rtb.Font.Size)
If Val(ip) < 4 Or Val(ip) > 144 Then Exit Sub
rtb.Font.Size = Val(ip)
mnuTxtSize.Caption = mnuTxtSize.Tag & " (" & rtb.Font.Size & ")"
putINI "Text", "Font.Size", rtb.Font.Size
End Sub

Public Sub mnuView_Click(Index As Integer)
'doLvwView

Dim bLvw As Boolean, bLst As Boolean, bSrch As Boolean

Dim i As Integer
For i = 1 To 3
  If i = Index Then
    mnuView(i).Checked = True
    TBar.Buttons(i).Value = tbrPressed
      IndView = i
  Else
    mnuView(i).Checked = False
    TBar.Buttons(i).Value = tbrUnpressed
  End If
Next

putINI "Appearance", "List View", CStr(IndView)

If Index = 3 Then ' mnuViewSearch.Checked = True Then
  bSrch = True
ElseIf Index = 2 Then '
  bLst = True
Else
  bLvw = True
End If

'Filelist
cmbHistory.Visible = bLvw
tvw.Visible = bLvw
lvw(1).Visible = bLvw
TBar.Buttons("up").Visible = bLvw
cmbFilter.Visible = bLvw
If mnuHorz.Checked = False Then Roll

vvlin.Visible = False: hlin.Visible = False
If bLvw = True Then
  If mnuHorz.Checked = True Then vvlin.Visible = bLvw Else hlin.Visible = bLvw
End If

'Playlist
'For i = 7 To 8: TBar.Buttons(i).Visible = bLst: Next
lvw(2).Visible = bLst
mnuPlaylistMoveUp.Visible = bLst
mnuPlaylistMoveDown.Visible = bLst
mnuPlaylistRemove.Visible = bLst
mnuPlaylistRemoveMissing.Visible = bLst
mnuPlaylistCrop.Visible = bLst

'off only if in Playlist Mode
mnuListAdd.Visible = Not bLst
mnuListRename.Visible = Not bLst
mnuListDelete.Visible = Not bLst

'Search
conSearch.Visible = bSrch

If bSrch Then cmbSearch.SetFocus Else lvw(IndView).SetFocus

'Column Visible menu & Sortby Column menu

For i = 1 To 6
  If i < lvw(IndView).ColumnHeaders.Count Then
    mnuLvwColumn(i).Visible = True
    If lvw(IndView).ColumnHeaders(i).Width = 0 Then mnuLvwColumn(i).Checked = False Else mnuLvwColumn(i).Checked = True
    mnuLvwArrange(i).Visible = True

  Else
    mnuLvwColumn(i).Visible = False
    mnuLvwArrange(i).Visible = False
  End If
Next

For i = 1 To lvw(IndView).ColumnHeaders.Count - 1
  mnuLvwColumn(i).Caption = lvw(IndView).ColumnHeaders(i).Text
  mnuLvwArrange(i).Caption = "by " & lvw(IndView).ColumnHeaders(i).Text
Next

End Sub

Private Sub mnuViewSlideShow_Click()
mnuViewSlideShow.Checked = Not mnuViewSlideShow.Checked
Tmr.Enabled = mnuViewSlideShow.Checked
If mnuViewSlideShow.Checked Then TBar.Buttons("slides").Value = tbrPressed Else TBar.Buttons("slides").Value = tbrUnpressed
End Sub

Private Sub TBar_ButtonClick(ByVal Button As MSComctlLib.Button)
'If Button.Index > cMod - 1 And Button.Index < cMod + 4 Then mnuMode_Click (Button.Index - cMod + 1): Exit Sub
Dim Inde As Integer

Select Case Button.Key

Case "explore"
  mnuView_Click 1
Case "play"
  mnuView_Click 2
Case "search"
  mnuView_Click 3
Case "open"
  mnuFileOpen_Click
Case "list"
  mnuPlaylistLoad_Click
Case "save"
  mnuPlaylistSave_Click
Case "goh"
  mnuHorz_Click
Case "up"
  If fso.FolderExists(lvw(1).Tag) Then Else MsgBox "Sorry, the folder " & lvw(1).Tag & " couldnt be found.": Exit Sub
  Set fldr = fso.GetFolder(lvw(1).Tag)
  DoEvents
  If fldr.IsRootFolder Then Exit Sub
  tvw_OpenPath fldr.ParentFolder
  fList.Path = fldr
  lvwLoad
Case "next"
  CrlNext
Case "repeat" 'checkbutton
  mnuMediaRepeat_Click
Case "slides" 'checkbutton
  mnuViewSlideShow_Click
Case "inc"
  mnuF7_Click
Case "dec"
  mnuF6_Click
Case "reset"
  mnuF8_Click
Case "image"
  Inde = mnuImage.Tag + 1: If Inde > mnuImg.Count Then Inde = 1
  mnuImg_Click Inde
Case "icon"
  Inde = lvw(1).View + 1: If Inde > 3 Then Inde = 0
  mnuLvwView_Click Inde
Case Else
  MsgBox Button.Key
End Select
End Sub

Private Sub TBar_ButtonDropDown(ByVal Button As MSComctlLib.Button)
If Button.Tag = "Views" Then
  PopupMenu menuLvwView, , TBar.Left + Button.Left, TBar.Top + Button.Top + Button.Height
ElseIf Button.Tag = "Image" Then
  PopupMenu mnuImage, , TBar.Left + Button.Left, TBar.Top + Button.Top + Button.Height
End If
End Sub

Private Sub tmr_Timer()
On Error Resume Next
If mnuViewSlideShow.Checked = True Then CrlNext
End Sub

Private Sub tmrAni_Timer()
ani_Center
tmrAni.Enabled = False
  Dim sep$
  If Int(ani.Duration Mod 60) < 10 Then sep$ = ":0" Else sep$ = ":"
  sep$ = Int(ani.Duration / 60) & sep$ & Int(ani.Duration Mod 60)
  If lvw(IndView).SelectedItem.SubItems(4) <> sep$ Then 'will blink otherwise
    lvw(IndView).SelectedItem.SubItems(4) = sep$
  End If

Static tl As New cInifile
If tl.Path = "" Then tl.Path = apPath & "Time Library.ini": tl.Section = "Time Library"
tl.Key = CurFile: tl.Value = sep$
  'putINI "Time Library", CurFile, sep$
End Sub

Private Sub hu_Click()
If RollStatus = -1 Then
  RollStatus = 0
Else
  If RollStatus = 0 Then hlin.Tag = hlin.Top
  RollStatus = -1:
End If
Roll
End Sub

Private Sub hd_Click()
If RollStatus = 1 Then
  RollStatus = 0
Else
  If RollStatus = 0 Then hlin.Tag = hlin.Top
  RollStatus = 1
End If
Roll
End Sub

Private Sub Roll() ' u=down 5=up
If RollStatus = -1 Then 'roll up
  tvw.Visible = False
  cmbHistory.Visible = False
  lvw(1).Visible = True
  hlin.Top = 0
  hu.ToolTipText = "Restore": hd.ToolTipText = "Roll Down"
  hu.Caption = "u": hd.Caption = "uu"
ElseIf RollStatus = 0 Then 'middle
  tvw.Visible = True
  lvw(1).Visible = True
  cmbHistory.Visible = True
  If Val(hlin.Tag) <> 0 Then
    If hlin.Tag > stbr.Top - 300 Then hlin.Tag = stbr.Top - 1000
    hlin.Top = hlin.Tag: hlin.Tag = 0
  End If
  If hlin.Top > conNavigator.ScaleHeight - 800 Then hlin.Top = conNavigator.ScaleHeight - 800
  hu.ToolTipText = "Roll Up": hd.ToolTipText = "Roll Down"
  hu.Caption = "5": hd.Caption = "u"
ElseIf RollStatus = 1 Then 'roll down
  tvw.Visible = True
  lvw(1).Visible = False
  cmbHistory.Visible = True
  hlin.Top = 0
  If mnuStatusBar.Checked Then hlin.Top = stbr.Top - hlin.Height Else hlin.Top = conNavigator.Height - hlin.Height
  hu.ToolTipText = "Roll Up": hd.ToolTipText = "Restore"
  hu.Caption = "55": hd.Caption = "5"
End If

putINI "Appearance", "hlin.pos", CStr(RollStatus)


Hsiz
End Sub

Private Sub hlin_Resize()
hu.Width = hlin.Width / 4
hd.Left = hu.Width * 3
hd.Width = hu.Width
End Sub

Private Sub hlin_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
hlin.BackColor = &H80000010: hMoving = True
End Sub

Private Sub hlin_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
Dim hpos As Single
If hMoving Then
  hpos = y + hlin.Top
  If hpos < cmbHistory.Top + 1000 Then hpos = cmbHistory.Top + 1000
  If hpos > stbr.Top - stbr.Height - 500 Then hpos = stbr.Top - stbr.Height - 500
  hlin.Top = hpos
  hlin.Tag = 0: RollStatus = 0: Roll
End If
End Sub

Private Sub hlin_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
hlin.BackColor = &H8000000F: hMoving = False

End Sub

Sub Hsiz()
If RollStatus <> -1 Then tvw.Height = hlin.Top - tvw.Top
  cmbFilter.Top = hlin.Top + hlin.Height
  lvw(1).Top = cmbFilter.Top + cmbFilter.Height
  If RollStatus <> 1 Then
    'this following line crashes when lst.top is gr8er than stbrs.top for obvious reasons
    If mnuStatusBar.Checked Then lvw(1).Height = stbr.Top - lvw(1).Top Else lvw(1).Height = conNavigator.ScaleHeight - lvw(1).Top
  End If
If RollStatus = 0 Then putINI "Appearance", "hlin.top", hlin.Top
End Sub

Private Sub ani_PlayStateChange(ByVal OldState As Long, ByVal NewState As Long)
If ani.PlayState = mpPlaying And tmrRev.Enabled = True Then
  tmrRev.Enabled = False
  mnuListPlayRev.Checked = False
End If
End Sub

Private Sub tmrRev_Timer()
ani.CurrentPosition = ani.CurrentPosition - 5
End Sub

Private Sub mnuListPlayRev_Click()
mnuListPlayRev.Checked = Not mnuListPlayRev.Checked
tmrRev.Enabled = mnuListPlayRev.Checked
If Not tmrRev.Enabled Then ani.DisplayMode = mpTime: ani.Play Else ani.DisplayMode = mpFrames: ani.Pause
End Sub

Private Sub tvw_Expand(ByVal Node As MSComctlLib.Node)
If Node.Image = 7 Then Exit Sub
tvw_Load fso.GetFolder(Node.Key), Node
End Sub

Private Sub tvw_KeyUp(KeyCode As Integer, Shift As Integer)
If tvw.SelectedItem Is Nothing Then Exit Sub
If tvw.SelectedItem.Key = fList.Path Then Exit Sub
If fso.DriveExists(tvw.SelectedItem.Key) = True Then
  If fso.Drives(tvw.SelectedItem.Key).IsReady = True Then _
    fList.Path = tvw.SelectedItem.Key
Else
  fList.Path = tvw.SelectedItem.Key
End If
lvwLoad
End Sub

Private Sub tvw_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
If tvw.SelectedItem Is Nothing Then Exit Sub
If tvw.SelectedItem.Image = 7 Then Exit Sub
If tvw.SelectedItem.Key = fList.Path Then Exit Sub
If fso.DriveExists(tvw.SelectedItem.Key) = True Then
  If fso.Drives(tvw.SelectedItem.Key).IsReady = True Then _
    fList.Path = tvw.SelectedItem.Key
Else
  fList.Path = tvw.SelectedItem.Key
End If
lvwLoad
End Sub

Private Sub tvw_NodeClick(ByVal Node As MSComctlLib.Node)
'NOT WORKING TOO PROEPRLY CHECK BEFORE FINALISING
Dim m As VbMsgBoxResult, nod As Node

If tvw.SelectedItem.Image = 7 Then lvw(1).ListItems.Clear: Exit Sub

If fso.DriveExists(Node.Key) Then
  Do
  If fso.Drives(Node.Key).IsReady = True Then Exit Do
    Do Until Node.Children = 0
      Set nod = Node.Child
        tvw.Nodes.Remove nod
    Loop
    m = MsgBox("The Drive is not ready", vbRetryCancel + vbCritical)
    If m = vbCancel Then Exit Sub
  Loop
  Node.Text = driveName(fso.GetDrive(Node.Key))
ElseIf fso.FolderExists(Node.Key) = False Then
  tvw.Nodes.Remove (Node.Index)
  Exit Sub
End If

Set fldr = fso.GetFolder(Node.Key)
If Node.Tag <> "expanded" Then tvw_Load fldr, Node: Exit Sub

Dim intg As Integer

'first clear a sub folder if it doesnt exist
If Node.Children > 0 Then
  Set nod = Node.Child
  Do
    If fso.FolderExists(nod.Key) = False Then
      intg = nod.Index
      Set nod = nod.Next
      tvw.Nodes.Remove (intg)
    Else
      Set nod = nod.Next
    End If
  Loop Until nod Is Nothing
End If


'now go folder by folder & see if u get an error then add new node coz itll error
'only if there aint no subnode which is only if the folder hasnt already been added
Dim fol As Folder

On Error Resume Next
If fldr.SubFolders.Count > 0 Then
  For Each fol In fldr.SubFolders
    Set nod = Nothing
    Set nod = tvw.Nodes(fol.Path)
    If nod Is Nothing Then  'then add folder
      tvw.Nodes.Add Node, tvwChild, fol.Path, fol.Name, "folder", "current"
      If fol.SubFolders.Count > 1 Then tvw.Nodes.Add fol.Path, tvwChild, fol.Path & " "
    End If
  Next
End If
End Sub

Private Sub tvw_Load(fldr As Folder, Node As MSComctlLib.Node)
On Error Resume Next
If Node.Tag = "expanded" Then Exit Sub
If Node.Children > 0 Then tvw.Nodes.Remove (fldr.Path & " ")
Node.Sorted = True
Dim fol As Folder
  For Each fol In fldr.SubFolders
  If LCase(fol.Name) = "recycled" And fldr.IsRootFolder Then GoTo Lop
  tvw.Nodes.Add Node, tvwChild, fol.Path, fol.Name, "folder", "current"
  tvw.Nodes(fol.Path).Sorted = True
  If fol.SubFolders.Count > 0 Then tvw.Nodes.Add tvw.Nodes(fol.Path), tvwChild, fol.Path & " "
Lop:
  Next
Node.Tag = "expanded"
End Sub

Public Function tvw_OpenPath(fl As String) As Node
Dim fol As Folder
If fso.FolderExists(fl) Then Set fol = fso.GetFolder(fl) Else Exit Function

If fol.IsRootFolder Then tvw.Nodes(fol.Path).Selected = True: Exit Function

If tvw.Nodes(fol.Drive & "\").Expanded = False Then tvw_Load fso.GetFolder(UCase(fol.Drive) & "\"), tvw.Nodes(UCase(fol.Drive) & "\")
If Len(fl) > 3 Then tvw.Nodes(UCase(fol.Drive) & "\").Expanded = True

Dim nod As Node
On Error Resume Next
Set nod = tvw.Nodes(fol.ParentFolder)
If nod Is Nothing Then
  Set nod = tvw_OpenPath(fol.ParentFolder)
  tvw_Load fol, tvw.Nodes(fl)
  nod.Selected = True
End If

nod.Expanded = True
Set tvw_OpenPath = nod
'tvw.Nodes(nodPath).Expanded = True

Exit Function
SorTrr:
  MsgBox Err.Number & " " & Err.Description, vbCritical
  'GoTo SrtRet:


End Function

Sub old_tvw_OpenPath(nodPath As String)
On Error Resume Next
If fso.FolderExists(nodPath) Then Set fldr = fso.GetFolder(nodPath) Else Exit Sub
If fldr.IsRootFolder Then tvw.Nodes(fldr.Path).Selected = True: Exit Sub

If tvw.Nodes(fldr.Drive & "\").Expanded = False Then tvw_Load fso.GetFolder(UCase(fldr.Drive) & "\"), tvw.Nodes(UCase(fldr.Drive) & "\")

If Len(nodPath) > 3 Then tvw.Nodes(UCase(fldr.Drive) & "\").Expanded = True

Dim i As Integer
For i = 4 To Len(nodPath)
  If Mid(nodPath, i, 1) = "\" Then
    If tvw.Nodes(Mid(nodPath, 1, i - 1)).Expanded = False Then
      tvw_Load fso.GetFolder(Mid(nodPath, 1, i)), tvw.Nodes(Mid(nodPath, 1, i - 1))
SrtRet:
      If Len(tvw.Nodes(Mid(nodPath, 1, i - 1)).Key) <> Len(nodPath) Then tvw.Nodes(Mid(nodPath, 1, i - 1)).Expanded = True
    End If
  End If
Next

tvw_Load fso.GetFolder(nodPath), tvw.Nodes(nodPath)
'tvw.Nodes(nodPath).Expanded = True
tvw.Nodes(nodPath).Selected = True

Exit Sub
SorTrr:
  MsgBox Err.Number & " " & Err.Description, vbCritical
  GoTo SrtRet:
End Sub

Private Sub Tvw_Refresh()
LockWindowUpdate tvw.hWnd
tvw.Nodes.Clear: 'cmbHistory.ComboItems.Clear
tvw.Sorted = False
LockWindowUpdate 0


If True = True Then 'showMycomp as boolean
  tvw.Nodes.Add , 4, "mycomp", "My Computer", 7
  Dim drv As Drive
  For Each drv In fso.Drives
    tvw.Nodes.Add "mycomp", 4, drv.DriveLetter & ":\", driveName(drv), imTvw.ListImages(drv.DriveType).Index
    If drv.IsReady Then
      Set fldr = fso.GetFolder(CStr(drv.Path) & "\")
      If fldr.SubFolders.Count > 1 Then tvw.Nodes.Add fldr.Path, tvwChild, fldr.Path & " "
    End If
  Next
End If

'9 dtop  10 faves   11 docs
Dim sKey() As String, cnt As Long, i As Long
'Dim strFol As String, strName As String
'Dim fol As Folder
cIni.Section = "Explorer Nodes"
cIni.EnumerateCurrentSection sKey, cnt

For i = 1 To cnt
  cIni.Key = sKey(i)
  'strFol = skey(i)
  AddFolderNode sKey(i), CInt(cIni.Value)
  'strName = StrReverse(strFol)
  'strName = Left(strName, InStr(strName, "\") - 1)
  'strName = StrReverse(strName)
    'Set fol = fso.GetFolder(strFol)
    'tvw.Nodes.Add , 4, strFol, strName, CInt(cIni.Value)
    'If fol.SubFolders.Count > 0 Then tvw.Nodes.Add tvw.Nodes(strFol), tvwChild, fol.Path & " "
Next

End Sub

Sub AddFolderNode(strFol As String, ind As Integer)
Dim fol As Folder
If fso.FolderExists(strFol) Then
  Set fol = fso.GetFolder(strFol)
  tvw.Nodes.Add , 4, strFol, fol.Name, ind
  If fol.SubFolders.Count > 0 Then tvw.Nodes.Add tvw.Nodes(strFol), tvwChild, fol.Path & " "
Else
  tvw.Nodes.Add , 4, strFol, fol.Name, 8
End If
End Sub

Function driveName(drv As Drive) As String
If drv.IsReady Then
  driveName = drv.VolumeName & " (" & drv.DriveLetter & ":)"
Else
  Dim vn As String
  If drv.DriveType = CDRom Then vn = "CD Drive "
  If drv.DriveType = Removable Then vn = "3� Floppy "
  driveName = vn & "(" & drv.DriveLetter & ":)"
End If
End Function

Private Sub txtSrchSize_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyReturn And cmdSearchGo.Caption = "Search" Then cmdSearchGo_Click
End Sub

Private Sub vlin_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
vlin.BackColor = &H80000010: vMoving = True
End Sub
Private Sub vlin_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
Dim vpos As Single, bCl As Boolean
If vMoving Then
  vpos = x + vlin.Left
  If vlin.Left = 0 And x > 0 Then vpos = 600: bCl = True
  If vpos < 500 Then vpos = 0: bCl = True
  If vpos > Me.ScaleWidth - 500 Then vpos = Me.ScaleWidth - vlin.Width: bCl = True
  If vlin.Left = Me.ScaleWidth - vlin.Width Then vpos = vlin.Left - 600: bCl = True
  vlin.Left = vpos
  If bCl Then
    'stop resizing
    vMoving = False
    vlin.BackColor = Me.BackColor
    Vsiz
  End If
End If

Exit Sub
'Dim vpos As Single
If vMoving Then
  vpos = x + vlin.Left
  If vpos > Me.Width / 2 Then vpos = Me.Width / 2
  If vpos < 1000 Then vpos = 1000
  vlin.Left = vpos
End If
End Sub
Private Sub vlin_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
vlin.BackColor = &H8000000F: vMoving = False
Vsiz
End Sub

Private Sub vvlin_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
vvlin.BackColor = &H80000010: vvMoving = True
ApplyTransient "Drag to resize widths of Folder Tree & Files List"
End Sub

Private Sub vvlin_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
Dim vpos As Single, bCl As Boolean
If vvMoving Then
  vpos = x + vvlin.Left
  If vvlin.Left = 0 And x > 0 Then vpos = 600: bCl = True
  If vpos < 500 Then vpos = 0: bCl = True
  If vpos > conNavigator.ScaleWidth - 500 Then vpos = conNavigator.ScaleWidth - vvlin.Width: bCl = True
  If vvlin.Left = conNavigator.ScaleWidth - vvlin.Width Then vpos = vvlin.Left - 600: bCl = True
  vvlin.Left = vpos
  If bCl Then
    'stop resizing
    vvMoving = False
    vvlin.BackColor = Me.BackColor
  End If
  vVsiz
End If

Exit Sub
'Dim vpos As Single
If vvMoving Then
  vpos = x + vvlin.Left
  If vpos > Me.Width / 2 Then vpos = Me.Width / 2
  If vpos < 1000 Then vpos = 1000
  vvlin.Left = vpos
  vVsiz
End If
End Sub

Private Sub vvlin_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
vvlin.BackColor = &H8000000F: vvMoving = False: ApplyTransient
End Sub

Private Sub hhsiz()
conViewer.Visible = False: conNavigator.Visible = False
vvlin.Visible = False: hlin.Visible = False

If mnuHorz.Checked Then
  If (TBar.Visible And hhlin.Top = TBar.Height) Or (TBar.Visible = False And hhlin.Top = 0) Then Else conViewer.Height = hhlin.Top - conViewer.Top
  conViewer.Left = 0

  conNavigator.Top = hhlin.Top + hhlin.Height
  conNavigator.Width = Me.ScaleWidth

  If mnuView(1).Checked = True Then vvlin.Visible = True
Else
  conViewer.Left = vlin.Left + vlin.Width
  conViewer.Height = Me.ScaleHeight - conViewer.Top
  
  conNavigator.Width = vlin.Left
  conNavigator.Top = conViewer.Top
  
  If IndView <> 3 Then hlin.Visible = True
End If

conViewer.Width = Me.ScaleWidth - conViewer.Left
conNavigator.Height = Me.ScaleHeight - conNavigator.Top
conNavigator_Resiz
'hhlin.Top = hhlin.Top - 500
If mnuHorz.Checked = False And hlin.Top > stbr.Top - 500 Then RollStatus = 1:  Roll

If Not tvw.SelectedItem Is Nothing Then tvw.SelectedItem.EnsureVisible 'esp when goin from horz to vertical
If Not lvw(1).SelectedItem Is Nothing Then lvw(1).SelectedItem.EnsureVisible
If Not lvw(2).SelectedItem Is Nothing Then lvw(2).SelectedItem.EnsureVisible
If Not lvw(3).SelectedItem Is Nothing Then lvw(3).SelectedItem.EnsureVisible

conViewer.Visible = True: conNavigator.Visible = True
If ani.Visible = True Then ani_Center

putINI "Appearance", "hhlin", hhlin.Top
End Sub

Sub Vsiz()
On Error Resume Next

If vlin.Visible = False Then Exit Sub

If vlin.Left > 300 Then
  conNavigator.Width = vlin.Left
  conNavigator_Resiz
End If
conViewer.Left = vlin.Left + vlin.Width
conViewer.Width = Me.ScaleWidth - conViewer.Left

putINI "Appearance", "vlin", vlin.Left
End Sub

Sub vVsiz()
If vvlin.Left > conNavigator.Width Then vvlin.Left = (conNavigator.Width - vvlin.Width) / 2
cmbHistory.Width = vvlin.Left

tvw.Width = vvlin.Left
lvw(1).Left = vvlin.Left + vvlin.Width: cmbFilter.Left = lvw(1).Left
lvw(1).Width = conNavigator.ScaleWidth - lvw(1).Left: cmbFilter.Width = lvw(1).Width
cmbFilter.Top = 0: lvw(1).Top = cmbFilter.Height
If mnuStatusBar.Checked Then lvw(1).Height = stbr.Top - lvw(1).Top Else lvw(1).Height = conNavigator.ScaleHeight - lvw(1).Top
tvw.Height = lvw(1).Height - 75

putINI "Appearance", "vvlin", vvlin.Left
End Sub


Private Sub hhlin_MouseDown(Button As Integer, Shift As Integer, x As Single, y As Single)
hhlin.BackColor = &H80000010: hhMoving = True
End Sub
Private Sub hhlin_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)

Dim hpos As Single, bCl As Boolean, hh As Single
If hhMoving Then
  hpos = y + hhlin.Top
  If TBar.Visible Then hh = TBar.Height
  If hhlin.Top = hh And y > 0 Then hpos = hh + 600: bCl = True
  If hpos < hh + 500 Then
    hpos = hh: bCl = True
  ElseIf ani.Visible And hpos < hh + 1080 Then
    hpos = hh + 1080
  End If
  If hpos > Me.ScaleHeight - 1000 Then hpos = Me.ScaleHeight - hhlin.Height: bCl = True
  If hhlin.Top = Me.ScaleHeight - hhlin.Height Then hpos = hhlin.Top - 1000: bCl = True
  hhlin.Top = hpos
  If bCl Then
    'stop resizing
    hhMoving = False
    hhlin.BackColor = Me.BackColor
    hhsiz
  End If
End If

Exit Sub
'Dim hpos As Single
If hhMoving Then
  hpos = y + hhlin.Top
  
    If hpos > Me.ScaleHeight - 2120 Then hpos = Me.ScaleHeight - 2120
  If IndView = 3 Then
    If hpos < conViewer.Top + 1080 Then hpos = conViewer.Top + 1080
  Else
    If hpos < conViewer.Top + 1000 Then hpos = conViewer.Top + 1000
  End If
  hhlin.Top = hpos
End If
End Sub
Private Sub hhlin_MouseUp(Button As Integer, Shift As Integer, x As Single, y As Single)
hhlin.BackColor = &H8000000F: hhMoving = False
hhsiz
End Sub

Private Sub vs_Change()
On Error Resume Next
img.Top = -vs * 1000: Exit Sub
End Sub

Private Sub mnuStatusBar_Click()
mnuStatusBar.Checked = Not mnuStatusBar.Checked
stbr.Visible = mnuStatusBar.Checked
If mnuStatusBar.Checked Then conSearch.Height = stbr.Top Else conSearch.Height = conNavigator.Height
conSearch_Resiz
lvw(2).Height = conSearch.Height
If mnuHorz.Checked = True Then
  If mnuStatusBar.Checked Then lvw(1).Height = conNavigator.ScaleHeight - stbr.Height - lvw(1).Top Else lvw(1).Height = conNavigator.ScaleHeight - lvw(1).Top
  tvw.Height = lvw(1).Height - 75
Else
  If mnuStatusBar.Checked Then lvw(1).Height = conNavigator.ScaleHeight - lvw(1).Top - stbr.Height Else lvw(1).Height = conNavigator.ScaleHeight - lvw(1).Top
  If RollStatus = 1 Then hlin.Top = Me.ScaleHeight - hlin.Height - lvw(1).Top: If mnuStatusBar.Checked Then hlin.Top = hlin.Top - stbr.Height
  If RollStatus = 1 And IndView = 1 Then Roll
End If
putINI "Preferences", "Show StatusBar", mnuStatusBar.Checked
End Sub

Private Sub crlSiz()
On Error Resume Next
Dim sz As Long: Dim it As Integer: sz = 0: it = 0
For Each itm In lvw(IndView).ListItems
  If itm.Selected And fso.FileExists(itm.Key) Then _
    Set fil = fso.GetFile(itm.Key): _
    sz = sz + fil.Size: it = it + 1
Next
If it > 1 Then
  stbr.Panels(2) = StrSiz(sz) & "  " & it & " of " & lvw(IndView).ListItems.Count & " files"
Else
  stbr.Panels(2) = lvw(IndView).SelectedItem.SubItems(1) & "  [1 file  of " & lvw(IndView).ListItems.Count & "]"
End If
End Sub

Private Sub TBar_Resiz()
If hhlin.Visible Then
  If hhlin.Top < TBar.Height And TBar.Visible Then
    hhlin.Top = TBar.Height + hhlin.Top
  ElseIf hhlin.Top = TBar.Height Then
    hhlin.Top = 0
  End If
  hhsiz
End If

If mnuHorz.Checked Then
  If TBar.Visible = True Then conViewer.Top = TBar.Height Else conViewer.Top = 0
  conViewer.Height = hhlin.Top - conViewer.Top
Else
  If TBar.Visible = True Then conNavigator.Top = TBar.Height Else conNavigator.Top = 0
  conViewer.Top = conNavigator.Top
  conViewer.Height = Me.ScaleHeight - conViewer.Top
End If

conNavigator.Height = Me.ScaleHeight - conNavigator.Top
conNavigator_Resiz

If TBar.Visible Then vlin.Top = TBar.Height Else vlin.Top = 0
vlin.Height = Me.ScaleHeight - vlin.Top
End Sub

Public Function GetFilIcon(strFile As String) As String
On Error Resume Next
  Dim extn As String
  'get valid icon key
  If fso.FileExists(strFile) = False Then GetFilIcon = "IVYnotfile": Exit Function
  extn = LCase(fso.GetExtensionName(strFile))
    'if no extn, return default
    If extn = "" Then GetFilIcon = "IVYnofile": Exit Function
  If extn = "ico" Or extn = "exe" Or extn = "lnk" Then extn = LCase(strFile)
  If IsNumeric(extn) Then extn = "Numeric" & extn
  'check if already exists
  Dim li As ListImage
  Set li = icoSmall.ListImages(extn)
  If Not li Is Nothing Then GetFilIcon = li.Key: Exit Function
  
  Dim hImgSmall As Long, hImgLarge As Long     ' The handle to the system image list

  ' Get the system icons associated with the file
  hImgSmall& = SHGetFileInfo(strFile, 0&, shinfo, Len(shinfo), BASIC_SHGFI_FLAGS Or &H1)
  hImgLarge& = SHGetFileInfo(strFile, 0&, shinfo, Len(shinfo), BASIC_SHGFI_FLAGS Or &H0)

  ' clear the pictureboxes before receiving the icons.
  picSmall.Picture = LoadPicture():    picLarge.Picture = LoadPicture()

  ' Draw the associated icons into the picture boxes
  ImageList_Draw hImgSmall&, shinfo.iIcon, picSmall.hdc, 0, 0, &H1
  ImageList_Draw hImgLarge&, shinfo.iIcon, picLarge.hdc, 0, 0, &H1

  icoSmall.ListImages.Add icoSmall.ListImages.Count + 1, extn, picSmall.Image
  icoLarge.ListImages.Add icoLarge.ListImages.Count + 1, extn, picLarge.Image
  
  GetFilIcon = extn
End Function

Public Sub AddHistoryFolder(Path As String)
'first see if it already exists
Dim Indx As Integer, cmbItm As ComboItem  'index where to add new path / remove item to add at start (in case of sort by time)

putINI "Folder History", Path, Format(Now, "yyyymmddhhMMss")

For Each cmbItm In cmbHistory.ComboItems
  If cmbItm.Key = LCase(Path) Then
    If viViewHistoryByTime Then cmbHistory.ComboItems.Remove cmbItm.Index: Exit For Else Exit Sub
  End If
Next


'then add it to lstSort
If viViewHistoryByTime = False Then lstSort.AddItem LCase(Path)

Dim i As Integer
If viViewHistoryByTime = True Then 'add as first item
  Indx = 1: GoTo Nxt
Else  'see where it should be added
  If cmbHistory.ComboItems.Count = 0 Then Indx = 1: GoTo Nxt
  For i = 1 To lstSort.ListCount
    If i = lstSort.ListCount Then Indx = i: GoTo Nxt 'if it is to be added as last item
    If cmbHistory.ComboItems(i).Key <> lstSort.List(i - 1) Then Indx = i: GoTo Nxt
  Next
End If
  Exit Sub  'path we're supposed to have just added coudnt be found in lstSize so disqualified

Nxt:
  If fso.DriveExists(Path) Then
    cmbHistory.ComboItems.Add Indx, LCase(Path), Path, 2
  ElseIf fso.FolderExists(Path) Then
    cmbHistory.ComboItems.Add Indx, LCase(Path), Path, 6
  Else
    cmbHistory.ComboItems.Add Indx, LCase(Path), Path, 8
  End If

End Sub


Public Sub SetHistoryFolder(Path As String)
AddHistoryFolder Path

cmbHistory.ComboItems(LCase(Path)).Selected = True

tvw_OpenPath Path
End Sub

Public Sub loadRtbFont(tbox As Control)
tbox.Font.Name = getINI("Text", "Font.Name", tbox.Font.Name)
tbox.Font.Size = getINI("Text", "Font.Size", tbox.Font.Size)
tbox.Font.Bold = getINI("Text", "Font.Bold", False)
tbox.Font.Italic = getINI("Text", "Font.Italic", False)
tbox.Font.Underline = getINI("Text", "Font.Underline", False)
tbox.Font.Strikethrough = getINI("Text", "Font.Strikethrough", False)
End Sub

Public Sub LoadFaves()
Dim i As Integer, cnt As Long, strS() As String

For i = 1 To mnuFaveF.Count - 1: Unload mnuFaveF(i): Next
cIni.Section = "Favorite Folders": cIni.EnumerateCurrentSection strS, cnt
For i = 1 To cnt
  Load mnuFaveF(i)
  mnuFaveF(i).Visible = True
  cIni.Key = strS(i)
  mnuFaveF(i).Caption = "&" & cIni.Value & " " & cIni.Key
Next
If mnuFaveF.Count > 1 Then sh1.Visible = True Else sh1.Visible = False

For i = 1 To mnuFaveP.Count - 1: Unload mnuFaveP(i): Next
cIni.Section = "Favorite Files": cIni.EnumerateCurrentSection strS, cnt
For i = 1 To cnt
  Load mnuFaveP(i)
  mnuFaveP(i).Visible = True
  cIni.Key = strS(i)
  mnuFaveP(i).Caption = "&" & cIni.Value & " " & cIni.Key
Next
If mnuFaveP.Count > 1 Then sh2.Visible = True Else sh2.Visible = False

End Sub

Public Sub loadExtensions()
Dim i As Integer, strS() As String, cnt As Long

cmbFilter.Clear
cmbSearch.Clear

'Filter Items
cmbFilter.AddItem "All Files (*.*)"
cmbFilter.AddItem "Media Files (" & extns.Item("Audio").Extensions & ";" & extns.Item("Video").Extensions & ")"
For i = 2 To extns.Count
  cmbFilter.AddItem extns(i).ExtName & " Files (" & extns(i).Extensions & ")"
Next

'add default to search before filing rest
For i = 0 To cmbFilter.ListCount - 1
  cmbSearch.AddItem cmbFilter.List(i)
Next

cIni.Section = "Filter History": cIni.EnumerateCurrentSection strS, cnt
For i = 1 To cnt
  cmbFilter.AddItem strS(i)
Next
cmbFilter.ListIndex = 0

'Search Items
cIni.Section = "Search History": cIni.EnumerateCurrentSection strS, cnt
For i = 1 To cnt
  cmbSearch.AddItem strS(i)
Next
cmbSearch.ListIndex = 0

End Sub

Private Sub ApplyTransient(Optional txt As String, Optional Keep As Boolean = True, Optional RevertIn As Integer)
Static strOrig As String

If txt = "" Then 'Revert
  stbr.Panels(1).Visible = True
  stbr.Panels(2).Alignment = sbrLeft
  stbr.Font.Bold = False
  stbr.Panels(2) = strOrig
Else
  stbr.Panels(1).Visible = False
  stbr.Panels(2).Alignment = sbrCenter
  stbr.Font.Bold = True
  If Keep = True Then strOrig = stbr.Panels(2)
  stbr.Panels(2) = txt
End If

If RevertIn > 0 Then
  tmrTransient.Interval = RevertIn * 1000
  tmrTransient.Enabled = True
Else
  tmrTransient.Enabled = False
End If
End Sub



Private Sub tmrTransient_Timer()
ApplyTransient
tmrTransient.Enabled = False
End Sub


Public Sub setCaption()
'form's caption
If viTitle = 1 Or CurFile = "" Then
  Me.Caption = apComp & " " & apProd & " - " & Chr(34) & CurFile & Chr(34) & " "
ElseIf viTitle = 2 Then
  Me.Caption = CurFile & " "
ElseIf viTitle = 3 Then
  Me.Caption = fso.GetFileName(CurFile) & " "
End If

App.Title = "IViewer (" & fso.GetFileName(CurFile) & ")"
End Sub

Public Sub LoadHistory()
Dim strS() As String, cnt As Long, pt As String, i As Long
cIni.Section = "Folder History"
cIni.EnumerateCurrentSection strS, cnt

lstSort.Clear
cmbHistory.ComboItems.Clear

For i = 1 To cnt
  If viViewHistoryByTime Then
    cIni.Key = strS(i)
    lstSort.AddItem cIni.Value & "#" & cIni.Key
  Else
    lstSort.AddItem strS(i)
  End If
Next i


For i = 0 To cnt - 1
  If viViewHistoryByTime Then pt = Mid(lstSort.List(i), InStr(lstSort.List(i), "#") + 1) Else pt = lstSort.List(i)
  If fso.DriveExists(pt) Then
    cmbHistory.ComboItems.Add , LCase(pt), pt, 2
  ElseIf fso.FolderExists(pt) Then
    cmbHistory.ComboItems.Add , LCase(pt), pt, 6
  Else
    cmbHistory.ComboItems.Add , LCase(pt), pt, 8
  End If
Next
End Sub

