HelpScribble project file.
13
...
0
0




FALSE

D:\imu\VB\IViewer\help
1
BrowseButtons()
0
FALSE

FALSE
TRUE
16777215
0
16711680
8388736
255
FALSE
FALSE
FALSE
1
FALSE
FALSE
Contents
%s Contents
Index
%s Index
Previous
Next
FALSE

10
10
Scribble10
Introduction




Writing



FALSE
20
{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Arial;}{\f1\fnil Arial;}}
{\colortbl ;\red0\green0\blue255;\red0\green128\blue0;\red128\green0\blue0;}
\viewkind4\uc1\pard\cf1\b\fs32 
\par 
\par Contents
\par 
\par \cf2\b0\strike\fs20 Features\cf3\strike0\{linkID=5000>new\}\{linkID=100\}
\par \tab\cf2\strike Built-in Explorer\cf3\strike0\{linkID=110\}\cf0\tab 
\par 
\par 
\par \cf2\strike Menu\cf3\strike0\{linkID=500\}
\par \cf0\f1\tab\cf2\strike File Menu\cf3\strike0\{linkID=510\}
\par \cf0\f0\tab\cf2\strike View Menu\cf3\strike0\{linkID=520\}\cf0\f1 
\par \f0\tab\cf2\strike File List Menu\cf3\strike0\{linkID=530\}\cf0\f1 
\par 
\par 
\par \cf2\strike\f0 Sys Requirements\cf3\strike0\{linkID=5000\}\cf0\f1 
\par 
\par 
\par }
15
Scribble15
<new topic>




Writing



FALSE
6
{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil Arial;}}
{\colortbl ;\red0\green0\blue255;}
\viewkind4\uc1\pard\cf1\b\f0\fs32 <new topic>\cf0\b0\fs20 
\par 
\par 
\par }
20
Scribble20
<new topic>




Writing



FALSE
4
{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil Arial;}}
\viewkind4\uc1\pard\f0\fs20 
\par 
\par }
100
Scribble100
Features




Writing



FALSE
14
{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Arial;}{\f1\fnil Arial;}{\f2\fnil\fcharset2 Symbol;}}
{\colortbl ;\red0\green0\blue255;}
\viewkind4\uc1\pard\cf1\b\fs32 Features\cf0\b0\f1\fs20 
\par 
\par \f0 IViewer is a smart file viewer built around four basic modes
\par 
\par \pard{\pntext\f2\'B7\tab}{\*\pn\pnlvlblt\pnf2\pnindent0{\pntxtb\'B7}}\fi-200\li200\tx200 Images (includes option of tiling)
\par {\pntext\f2\'B7\tab}Text - using the RichTxt32.ocx file
\par {\pntext\f2\'B7\tab}Media (built on the WMP6 MSDXM.ocx)
\par {\pntext\f2\'B7\tab}HTML (built around the web browser control)\f1 
\par {\pntext\f2\'B7\tab}\pard 
\par 
\par 
\par }
110
Scribble110
Built in Explorer




Writing



FALSE
15
{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Arial;}{\f1\fnil Arial;}}
{\colortbl ;\red0\green0\blue255;}
\viewkind4\uc1\pard\cf1\b\fs32 Built in Explorer\cf0\b0\f1\fs20 
\par 
\par 
\par 
\par 
\par \f0 This also has the option of adding root nodes. 
\par 
\par 
\par As well as the option of filtering out files from it
\par \f1 
\par 
\par \f0 Also, it remembers folders viewed\f1 
\par }
500
Scribble500
Menu




Writing



FALSE
6
{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Arial;}{\f1\fnil Arial;}}
{\colortbl ;\red0\green0\blue255;}
\viewkind4\uc1\pard\cf1\b\fs32 Menu\cf0\b0\f1\fs20 
\par 
\par 
\par }
510
Scribble510
File Menu




Writing



FALSE
8
{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Arial;}{\f1\fnil Arial;}}
{\colortbl ;\red0\green0\blue255;\red128\green0\blue0;}
\viewkind4\uc1\pard\cf1\b\fs32 File Menu
\par \pard\li540 
\par \pard\cf0\b0\f1\fs20 
\par \pard\li540 
\par \pard\cf2\{bmc mnuFile.\f0 bmp\f1\}\cf0 
\par }
520
Scribble520
View Menu




Writing



FALSE
6
{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Arial;}{\f1\fnil Arial;}}
{\colortbl ;\red0\green0\blue255;\red128\green0\blue0;}
\viewkind4\uc1\pard\cf1\b\fs32 View Menu\cf0\b0\f1\fs20 
\par 
\par \cf2\{bmc mnuView.\f0 bmp\f1\}\cf0 
\par }
530
Scribble530
File List




Writing



FALSE
6
{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Arial;}{\f1\fnil Arial;}}
{\colortbl ;\red0\green0\blue255;\red128\green0\blue0;}
\viewkind4\uc1\pard\cf1\b\fs32 File List\cf0\b0\f1\fs20 
\par 
\par \cf2\{bmc mnuList.bmp\}\cf0 
\par }
5000
Scribble5000
System Requirements




Writing



FALSE
6
{\rtf1\ansi\ansicpg1252\deff0\deflang1033{\fonttbl{\f0\fnil\fcharset0 Arial;}{\f1\fnil Arial;}}
{\colortbl ;\red0\green0\blue255;}
\viewkind4\uc1\pard\cf1\b\fs32 System Requirements\f1 
\par \cf0\b0\fs20 
\par \f0 
\par }
2
menu="Contents",(176,72,211,818),0,,,0
new="",(512,0,511,1023),0,,,0
0
0
0
7
*InternetLink
8388608
Verdana
0
20
0
B...
0
0
28
-28
0
0
*ParagraphTitle
-16777208
Arial
0
11
1
B...
0
0
0
0
0
0
*PopupLink
-16777208
Arial
0
8
1
....
0
0
0
0
0
0
*PopupTopicTitle
16711680
Arial
0
10
1
B...
0
0
0
0
0
0
*TopicText
-16777208
Arial
0
10
1
....
0
0
0
0
0
0
*TopicTitle
16711680
Arial
0
16
1
B...
0
0
0
0
0
0
ImuTitle
-16777208
Arial
0
10
1
....
0
0
0
0
0
0
