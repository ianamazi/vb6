VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmJumpFolder 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Jump to Folder..."
   ClientHeight    =   3615
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6255
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3615
   ScaleWidth      =   6255
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtFilter 
      Height          =   375
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   4935
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   5160
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   120
      Width           =   855
   End
   Begin MSComctlLib.ListView lvwJump 
      Height          =   2895
      Left            =   120
      TabIndex        =   2
      Top             =   600
      Width           =   6015
      _ExtentX        =   10610
      _ExtentY        =   5106
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      HoverSelection  =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Name"
         Object.Width           =   4410
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Location"
         Object.Width           =   5292
      EndProperty
   End
End
Attribute VB_Name = "frmJumpFolder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
lvwJump.SmallIcons = Vdplayer.imTvw

lvwJump.ListItems.Clear

'first add from history
Dim cb As ComboItem, itm As ListItem
On Error Resume Next
For Each cb In Vdplayer.cmbHistory.ComboItems
  If fso.FolderExists(cb.Key) Then
    If InStr(cb.Text, "\") <> 0 Then tx = StrReverse(Mid(StrReverse(cb.Text), 1, InStr(StrReverse(cb.Text), "\") - 1)) _
      Else tx = cb.Text
    Set itm = lvwJump.ListItems.Add(lvwJump.ListItems.Count + 1, LCase(cb.Key), tx, , cb.Image)
    itm.SubItems(1) = cb.Key
  End If
Next

Dim nod As Node
For Each nod In Vdplayer.tvw.Nodes
  If fso.FolderExists(nod.Key) Then
    If nod.Text <> "" Then  'for the blank children we have since thats how we indicate
                            'sub folders without actually loading them
      Set itm = lvwJump.ListItems.Add(lvwJump.ListItems.Count + 1, LCase(nod.Key), nod.Text, , nod.Image)
    End If
    If itm.Key = LCase(nod.Key) Then itm.SubItems(1) = nod.Key
  End If
Next

End Sub

Private Sub cmdCancel_Click()
Unload Me
End Sub

Private Sub lvwJump_DblClick()
  Vdplayer.tvw_OpenPath lvwJump.SelectedItem.Key
  Vdplayer.fList.path = lvwJump.SelectedItem.Key
  Vdplayer.lvwLoad
  Vdplayer.tvw.SelectedItem.EnsureVisible
    Unload Me
    'lvwCrl.SetFocus
End Sub

Private Sub lvwJump_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyReturn Then lvwJump_DblClick
End Sub

Private Sub txtFilter_Change()
lvwJump.ListItems.Clear

Dim str As String
str = LCase(txtFilter)

lvwJump.ListItems.Clear
Dim nod As Node, itm As ListItem
For Each nod In Vdplayer.tvw.Nodes
  'MsgBox InStr(itm.Text, txtFilter)
  If InStr(LCase(nod.Text), str) > 0 Then
    Set itm = lvwJump.ListItems.Add(lvwJump.ListItems.Count + 1, nod.Key, nod.Text, , nod.Image)
    itm.SubItems(1) = nod.Key
  End If
Next

End Sub


Private Sub txtFilter_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyUp Or KeyCode = vbKeyDown Then lvwJump.SetFocus
End Sub

Private Sub txtFilter_KeyPress(KeyAscii As Integer)
If KeyAscii = vbKeyUp Then
  If lvwJump.SelectedItem.Index > 1 Then lvwJump.ListItems(lvwJump.SelectedItem.Index - 1).Selected = True
ElseIf KeyAscii = vbKeyDown Then
  If lvwJump.SelectedItem.Index < lvwJump.ListItems.Count Then lvwJump.ListItems(lvwJump.SelectedItem.Index + 1).Selected = True
End If
End Sub

