VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSearch 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "Search for Text"
   ClientHeight    =   3765
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4695
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmSearchText.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3765
   ScaleWidth      =   4695
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin MSComctlLib.ListView lvw 
      Height          =   2295
      Left            =   120
      TabIndex        =   5
      Top             =   1320
      Width           =   4455
      _ExtentX        =   7858
      _ExtentY        =   4048
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   2
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Text            =   "Loc (Char)"
         Object.Width           =   2540
      EndProperty
      BeginProperty ColumnHeader(2) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         SubItemIndex    =   1
         Text            =   "Line"
         Object.Width           =   4586
      EndProperty
   End
   Begin VB.Frame Frame1 
      Caption         =   "Find Text"
      Height          =   1215
      Left            =   165
      TabIndex        =   6
      Top             =   0
      Width           =   4335
      Begin VB.CommandButton cmdFind 
         Caption         =   "&Find"
         Default         =   -1  'True
         Height          =   375
         Left            =   3000
         TabIndex        =   1
         Top             =   360
         Width           =   1095
      End
      Begin VB.CheckBox chk 
         Caption         =   "&Match Case"
         Height          =   240
         Left            =   3000
         TabIndex        =   4
         Top             =   840
         Width           =   1215
      End
      Begin VB.OptionButton opt 
         Caption         =   "&Down"
         Height          =   255
         Index           =   2
         Left            =   1560
         TabIndex        =   3
         Top             =   840
         Width           =   735
      End
      Begin VB.OptionButton opt 
         Caption         =   "&All"
         Height          =   255
         Index           =   1
         Left            =   840
         TabIndex        =   2
         Top             =   840
         Value           =   -1  'True
         Width           =   735
      End
      Begin VB.TextBox txt 
         Height          =   360
         Left            =   240
         TabIndex        =   0
         Top             =   360
         Width           =   2655
      End
   End
End
Attribute VB_Name = "frmSearch"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'always on top
Private Declare Function SetWindowPos& Lib "user32" (ByVal hWnd As Long, ByVal hWndInsertAfter As Long, ByVal X As Long, ByVal Y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long)

Private Sub cmdFind_Click()
Dim pos As Long, lpos As Long, itm As ListItem

With Vdplayer.rtb

lvw.ListItems.Clear
  lvw.Tag = Len(txt)
pos = -1

Do
  pos = .Find(txt, pos + 1)
    'must add code for case sensitivity
    'and for "Whole Word Only"
      'Cant chk down only can we
  If pos = -1 Then Exit Do
  
  lpos = .GetLineFromChar(pos)
  Set itm = lvw.ListItems.Add(lvw.ListItems.Count + 1, , "Lin:" & 1 + lpos)
  
  itm.ToolTipText = "(Char:" & pos & ")"
  itm.Tag = pos
  
  Dim beg As Long, ln As Long, tmp As String, r1 As Long, r2 As Long
    'r is for each type of way the linw can end
    'must see if theres more than crlf & fullstop
  tmp = StrReverse(Mid(.Text, 1, pos))
  r1 = 0: r2 = 0
  r1 = InStr(tmp, " .")
  r2 = InStr(tmp, Chr(13))
  If r1 = 0 And r2 = 0 Then
    beg = 1
  ElseIf r1 < r2 And r1 <> 0 Then
    beg = pos - r1 + 1
  Else
    beg = pos - r2 + 2
  End If
  
  tmp = Mid(.Text, pos)
  r1 = 0: r2 = 0
  r1 = InStr(tmp, ". ")
  r2 = InStr(tmp, Chr(10))
  If r1 = -1 And r2 = -1 Then
    ln = Len(Mid(.Text, pos))
  ElseIf r1 < r2 And r1 <> 0 Then
    ln = r1 + pos - beg
  Else
    ln = r2 + pos - beg
  End If
  
  
  itm.SubItems(1) = Mid(.Text, beg, ln)
  itm.ListSubItems(1).ToolTipText = itm.SubItems(1)
  
Loop

Set itm = Nothing

End With
End Sub

Private Sub Form_Resize()
If Me.Width < 2100 Then Me.Width = 2100: Exit Sub
Frame1.Left = (Me.ScaleWidth - Frame1.Width) / 2
lvw.Width = Me.ScaleWidth - 2 * lvw.Left
If Me.ScaleHeight > lvw.Top + 1000 Then
  lvw.Height = Me.ScaleHeight - lvw.Top - lvw.Left
  lvw.ColumnHeaders(2).Width = lvw.Width - lvw.ColumnHeaders(1).Width - 300
End If
End Sub

Private Sub lvw_ItemClick(ByVal Item As MSComctlLib.ListItem)
Vdplayer.rtb.SelStart = Item.Tag
Vdplayer.rtb.SelLength = lvw.Tag
End Sub

Sub Old_Find()
Dim strSearch As String, strIn As String, inSt As Long
Dim Lposn As Long, Rposn As Long

If txt = "" Then MsgBox "Must enter text to search for.", vbExclamation, "Search String?": txt.SetFocus: Exit Sub

With Vdplayer.rtb
If opt(2).Value = True Then
  Rposn = .SelStart + .SelLength
Else
  If LCase(.SelText) = LCase(txt) Then
    Rposn = .SelStart + .SelLength
  Else
    Rposn = 1
  End If
End If
Lposn = Rposn - 2

If chk.Value = 1 Then
  strSearch = txt.Text
  strIn = .Text
Else
  strSearch = LCase(txt.Text)
  strIn = LCase(.Text)
End If

'Dim r As Long
'r = .Find(strSearch)
'If r <> -1 Then
  '.SelStart = r
  '.SelLength = Len(strSearch)
'Else
  'MsgBox "Text not found", vbExclamation
'End If
inSt = InStr(Mid(strIn, Rposn), CStr(strSearch))
If inSt = 0 Then inSt = InStr(strIn, strSearch): Lposn = -1
If inSt = 0 Then MsgBox "Text not found", vbExclamation: Exit Sub

.SelStart = Lposn + inSt
.SelLength = Len(txt)

End With
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyEscape Then Unload Me
End Sub

Private Sub Form_Load()
'Public Const HWND_TOPMOST = -1
'Public Const HWND_NOTOPMOST = -2
'Public Const HWND_TOP = 0

SetWindowPos Me.hWnd, -1, 0, 0, 0, 0, 3  'makes it stay on top
txt = Vdplayer.rtb.SelText

Me.Height = getINI("Text", "Search.Height", Me.Height)
Me.Width = getINI("Text", "Search.Width", Me.Width)
Me.Top = getINI("Text", "Search.Top", (Screen.Height - Me.Height) / 2)
Me.Left = getINI("Text", "Search.Left", (Screen.Width - Me.Width) / 2)

End Sub

Private Sub Form_Unload(Cancel As Integer)
  putINI "Text", "Search.Height", Me.Height
  putINI "Text", "Search.Width", Me.Width
  putINI "Text", "Search.Top", Me.Top
  putINI "Text", "Search.Left", Me.Left
End Sub

