VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmSplash 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   3930
   ClientLeft      =   255
   ClientTop       =   1410
   ClientWidth     =   6720
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   ForeColor       =   &H00000000&
   Icon            =   "frmSplash.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmSplash.frx":000C
   ScaleHeight     =   262
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   448
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList ImLink 
      Left            =   5640
      Top             =   1200
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   12632256
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   16777215
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmSplash.frx":4CE7
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.Label lblLink 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "www.cselian.com/software"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   -1  'True
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   270
      Left            =   1800
      TabIndex        =   2
      Top             =   3600
      Width           =   3120
   End
   Begin VB.Label lblVer 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "v 3.0.375"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00404040&
      Height          =   315
      Left            =   2760
      TabIndex        =   1
      Top             =   2040
      Width           =   1200
   End
   Begin VB.Label lblStatus 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   600
      Left            =   240
      TabIndex        =   0
      Top             =   2880
      Width           =   6210
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmSplash"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public isIvyLoading As Boolean

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyEscape And isIvyLoading = True Then End
If isIvyLoading = False And (KeyCode = vbKeyEscape Or KeyCode = vbKeyReturn) Then Unload Me
End Sub

Private Sub Form_Load()
  Me.BackColor = 15265783
  Me.MouseIcon = ImLink.ListImages(1).ExtractIcon
  lblVer.Caption = "v " & App.Major & "." & App.Minor & "." & App.Revision
End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Button = 2 And isIvyLoading = False Then Unload Me: Exit Sub
If (X > 30 And X < 420 And Y > 6 And Y < 56) Then
  ShellExecute 0, "open", "http://www.angelfire.com/rock3/ivy", vbNullString, "C:\", 0
ElseIf (X > 192 And X < 430 And Y > 154 And Y < 176) Then
  ShellExecute 0&, vbNullString, "mailto:ianamazi@yahoo.com", vbNullString, vbNullString, vbHide
Else
  If isIvyLoading = False Then Unload Me
End If
End Sub

Private Sub Form_MouseMove(Button As Integer, Shift As Integer, X As Single, Y As Single)
If (X > 30 And X < 420 And Y > 6 And Y < 56) _
  Or (X > 192 And X < 430 And Y > 154 And Y < 176) Then _
    Me.MousePointer = 99 Else Me.MousePointer = 0
End Sub

Private Sub lblStatus_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
  If isIvyLoading = False Then Unload Me
End Sub

Private Sub lblVer_Click()
'show version history HOW??
End Sub
