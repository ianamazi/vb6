Attribute VB_Name = "modMain"
Option Explicit

Public apComp As String, apProd As String, apPath As String
Public fso As New FileSystemObject
Public cIni As New cInifile


Public Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long

Public Declare Function ShellExecute Lib "Shell32.dll" Alias "ShellExecuteA" _
(ByVal hWnd As Long, ByVal lpOperation As String, _
ByVal lpFile As String, ByVal lpParameters As String, _
ByVal lpDirectory As String, ByVal nShowCmd As Long) As Long 'for launching

Public Declare Function BitBlt Lib "gdi32" (ByVal hDestDC As Integer, ByVal x As Integer, _
   ByVal y As Integer, ByVal nWidth As Integer, ByVal nHeight As Integer, ByVal _
   hSrcDC As Integer, ByVal XSrc As Integer, ByVal YSrc As Integer, ByVal dwRop _
   As Long) As Integer

'for renaming (added on 27th Dec 03) - see lvw_AfterLabelEdit in vdplayer for rest of code
'from D:\imufol\vb Code\snippets other\Files, total API manipulation with visual dialogs.FIL
Public Declare Function SHFileOperation Lib "Shell32.dll" Alias "SHFileOperationA" _
(lpFileOp As SHFILEOPSTRUCT) As Long
Public Type SHFILEOPSTRUCT
     hWnd As Long
     wFunc As Long
     pFrom As String
     pTo As String
     fFlags As Long
     fAnyOperationsAborted As Long
     hNameMappings As Long
     lpszProgressTitle As String
End Type
'add to above declarations to include recycle bin deleting (added 13th July 2004 from MSDN)
Public Declare Function GetTempFileName Lib "kernel32" _
        Alias "GetTempFileNameA" (ByVal lpszPath As String, _
        ByVal lpPrefixString As String, ByVal wUnique As Long, _
        ByVal lpTempFileName As String) As Long


'TO GET MY DOCUMENTS AND DESKTOP ICONS IN frmOptions
Declare Function SHGetSpecialFolderPath Lib "Shell32.dll" Alias "SHGetSpecialFolderPathA" _
  (ByVal hwndOwner As Long, ByVal lpszPath As String, ByVal nFolder As Long, ByVal fCreate As Long) As Long

Public extns As New Extens

Public Function doMinutes(i As Long) As String
If i > 525600 Then doMinutes = Int(i / 525600) & " year(s), ": i = i - 525600 * Int(i / 525600)
If i > 1440 Then doMinutes = doMinutes & Int(i / 1440) & " days, ": i = i - 1440 * Int(i / 1440)
If i > 60 Then doMinutes = doMinutes & Int(i / 60) & " hours, ": i = i - 60 * Int(i / 60)
If i > 0 Then doMinutes = doMinutes & i & " minutes."
End Function

Public Function StrSiz(Siz As Long)
If Siz >= 1048576 Then StrSiz = Int(Siz / 10485.76) / 100 & "MB": Exit Function
If Siz >= 1024 Then StrSiz = Int(Siz / 10.24) / 100 & "KB": Exit Function
StrSiz = Siz & "b"
End Function

Public Function AddSlash(fol As String) As String
If Right(fol, 1) = "\" Then
  AddSlash = fol
Else
  AddSlash = fol & "\"
End If
End Function

Public Function getINI(Section As String, Key As String, Optional Default As String) As String
With cIni
  .Section = Section
  .Key = Key
  If IsMissing(Default) Then .Default = "" Else .Default = Default
  getINI = .Value
End With
End Function

Public Sub putINI(Section As String, Key As String, Value As String)
With cIni
  .Section = Section
  .Key = Key
  .Value = Value
End With
End Sub

Sub Main()

apComp = App.CompanyName: apProd = App.ProductName: apPath = App.Path

'Dim tm As Single
'tm = Timer()
If Mid(apPath, Len(apPath) - 1) <> "\" Then apPath = apPath & "\"

cIni.Path = apPath & "IViewer.ini"

'Load frmSplash
  'frmSplash.lblStatus = "IViewer is absolutely FREE!!  �2001 No Rights Reserved."
  'frmSplash.Show vbModal: Exit Sub
'frmExtns.Show vbModal: Exit Sub
'frmOptions.Show vbModal: Exit Sub
'frmTLib.Show: Exit Sub

If "True" = getINI("Application Start", "Splash", "True") Then
  Load frmSplash
  frmSplash.WindowState = 0
  frmSplash.Left = (Screen.Width - frmSplash.Width) / 2: frmSplash.Top = (Screen.Height - frmSplash.Height) / 2
  frmSplash.Show
End If

Load Vdplayer
Vdplayer.Show

'Debug.Print Timer() - tm: Debug.Print
'MsgBox "Start Took " & Timer() - tm & " seconds"

If True = getINI("Application Start", "Tips", "True") Then
  If fso.FileExists(apPath & "tips.txt") = False Then MsgBox "The file 'tips.txt' is not in the program folder." & vbCrLf & _
    "  ('" & apPath & "')" & vbCrLf & "To view Tips, kindly replace the file.", vbCritical, "File Not Found" Else frmTips.Show
End If
End Sub



Public Sub SortLvwColumn(ByVal lvw As MSComctlLib.ListView, ColumnIndex As Integer, SortType As Integer, SortOrder As Integer)
    Dim x As Integer, y As Integer
    'On Error GoTo ErrHandler
    

Select Case SortType
        
'*** Alphanumeric sort
Case 0 'sortAlphanumeric
  DoSort lvw, SortOrder, ColumnIndex - 1
            
'*** Numeric Sort
Case 1 'sortNumeric
  Dim strMax As String, strNew As String
            
    'Find the longest (whole) number string length in the column

    If ColumnIndex > 1 Then
        For x = 1 To lvw.ListItems.Count
            If Len(lvw.ListItems(x).ListSubItems(ColumnIndex - 1)) <> 0 Then 'ignores 0 length strings
                If Len(CStr(Int(lvw.ListItems(x).ListSubItems(ColumnIndex - 1)))) > Len(strMax) Then
                    strMax = CStr(Int(lvw.ListItems(x).SubItems(ColumnIndex - 1)))
                End If
            End If
        Next
    Else
        For x = 1 To lvw.ListItems.Count
            If Len(lvw.ListItems(x)) <> 0 Then
                If Len(CStr(Int(lvw.ListItems(x)))) > Len(strMax) Then
                    strMax = CStr(Int(lvw.ListItems(x)))
                End If
            End If
        Next
    End If
            
      'hide the control - speeds up the sort
      lvw.Visible = False
            
    If ColumnIndex > 1 Then
        For x = 1 To lvw.ListItems.Count
            If Len(lvw.ListItems(x).ListSubItems(ColumnIndex - 1)) = 0 Then
                lvw.ListItems(x).ListSubItems(ColumnIndex - 1) = "0" 'make 0 length strings = To "0"
            ElseIf Len(CStr(Int(lvw.ListItems(x).ListSubItems(ColumnIndex - 1)))) < Len(strMax) Then
                'prefix all numbers with 0's as required
                strNew = lvw.ListItems(x).ListSubItems(ColumnIndex - 1)

                For y = 1 To Len(strMax) - Len(CStr(Int(lvw.ListItems(x).ListSubItems(ColumnIndex - 1))))
                    strNew = "0" & strNew
                Next
                lvw.ListItems(x).ListSubItems(ColumnIndex - 1) = strNew
            End If
        Next
    Else
        For x = 1 To lvw.ListItems.Count


            If Len(lvw.ListItems(x).Text) = 0 Then
                lvw.ListItems(x).Text = "0" 'make 0 length strings = To "0"
            ElseIf Len(CStr(Int(lvw.ListItems(x)))) < Len(strMax) Then
                'prefix all numbers with 0's as required
                
                strNew = lvw.ListItems(x).Text


                For y = 1 To Len(strMax) - Len(CStr(Int(lvw.ListItems(x))))
                    strNew = "0" & strNew
                Next
                lvw.ListItems(x).Text = strNew
            End If
        Next
    End If
            
                DoSort lvw, SortOrder, ColumnIndex - 1
                
    If ColumnIndex > 1 Then
        'Remove preceding 0's
        For x = 1 To lvw.ListItems.Count
            lvw.ListItems(x).ListSubItems(ColumnIndex - 1) = CDbl(lvw.ListItems(x).ListSubItems(ColumnIndex - 1))
            If lvw.ListItems(x).ListSubItems(ColumnIndex - 1) = 0 Then lvw.ListItems(x).ListSubItems(ColumnIndex - 1) = ""
        Next
    Else
        'Remove preceding 0's
        For x = 1 To lvw.ListItems.Count
            lvw.ListItems(x).Text = CDbl(lvw.ListItems(x).Text)
            If lvw.ListItems(x).Text = 0 Then lvw.ListItems(x).Text = ""
        Next
    End If
    lvw.Visible = True
                
'*** Date Sort
Case 2 'sortDate
    lvw.Visible = False

    If ColumnIndex > 1 Then
        'Convert dates to format that can be sorted alphanumerically
        For x = 1 To lvw.ListItems.Count
            lvw.ListItems(x).ListSubItems(ColumnIndex - 1) = Format(lvw.ListItems(x).ListSubItems(ColumnIndex - 1), "YYYY MM DD hh:mm:ss")
        Next

        DoSort lvw, SortOrder, ColumnIndex - 1

        'Convert dates back to General Date format
        For x = 1 To lvw.ListItems.Count
            lvw.ListItems(x).ListSubItems(ColumnIndex - 1) = Format(lvw.ListItems(x).ListSubItems(ColumnIndex - 1), "General Date")
        Next
    Else
        'Convert dates to format that can be sorted alphanumerically
        For x = 1 To lvw.ListItems.Count
            lvw.ListItems(x).Text = Format(lvw.ListItems(x).Text, "YYYY MM DD hh:mm:ss")
        Next
            
        DoSort lvw, SortOrder, ColumnIndex - 1
                
        'Convert dates back to General Date format
        For x = 1 To lvw.ListItems.Count
            lvw.ListItems(x).Text = Format(lvw.ListItems(x).Text, "General Date")
        Next
                
    End If
            
    lvw.Visible = True

End Select

End Sub


Private Sub DoSort(ByVal lvw As ListView, SortOrder As Integer, SortKey As Integer)
    If SortOrder = 3 Then 'sortAscending
        lvw.SortOrder = lvwAscending
    ElseIf SortOrder = 4 Then 'sortDescending
        lvw.SortOrder = lvwDescending
    End If
    lvw.SortKey = SortKey
    lvw.Sorted = True
End Sub
