Attribute VB_Name = "modLvw_Indent"
Option Explicit

Private Type LVITEM
   mask As Long
   iItem As Long
   iSubItem As Long
   state As Long
   stateMask As Long
   pszText As String
   cchTextMax As Long
   iImage As Long
   lParam As Long
   iIndent As Long
End Type

Private Const LVIF_INDENT As Long = &H10
Private Const LVIF_TEXT As Long = &H1
'Private Const LVM_FIRST As Long = &H1000
'Private Const LVM_SETITEM As Long = (LVM_FIRST + 6)

Private Declare Function SendMessage Lib "user32" _
   Alias "SendMessageA" _
  (ByVal hWnd As Long, _
   ByVal wMsg As Long, _
   ByVal wParam As Long, _
   lParam As Any) As Long

' LVM_SETIMAGELIST wParam value
Public Const LVSIL_STATE = 2

' Listview item state image index values
Public Enum LVItemStates
  lvisNoButton = 0
  lvisCollapsed = 1
  lvisExpanded = 2
End Enum

' LVITEM mask
Public Const LVIF_STATE = &H8
#If (WIN32_IE >= &H300) Then
Public Const LVIF_INDENT = &H10
#End If

' LVITEM state, stateMask
Public Const LVIS_FOCUSED = &H1
Public Const LVIS_SELECTED = &H2
Public Const LVIS_STATEIMAGEMASK = &HF000

' messages
Public Const LVM_FIRST = &H1000
Public Const LVM_SETIMAGELIST = (LVM_FIRST + 3)
Public Const LVM_GETITEMCOUNT = (LVM_FIRST + 4)
Public Const LVM_GETITEM = (LVM_FIRST + 5)
Public Const LVM_SETITEM = (LVM_FIRST + 6)
Public Const LVM_GETNEXTITEM = (LVM_FIRST + 12)
Public Const LVM_HITTEST = (LVM_FIRST + 18)
Public Const LVM_ENSUREVISIBLE = (LVM_FIRST + 19)
Public Const LVM_SETCOLUMNWIDTH = (LVM_FIRST + 30)
Public Const LVM_SETITEMSTATE = (LVM_FIRST + 43)
#If (WIN32_IE >= &H300) Then
Public Const LVM_SETEXTENDEDLISTVIEWSTYLE = (LVM_FIRST + 54)
#End If



' Sets the state and indent vaues of the specified listview item

Public Function setIndent(hwndLV As Long, iItem As Long, iIndent As Long, dwState As LVItemStates) As Boolean
  Dim lvi As LVITEM

  lvi.mask = LVIF_STATE Or LVIF_INDENT
  lvi.iItem = iItem - 1
  lvi.state = INDEXTOSTATEIMAGEMASK(dwState)
  lvi.stateMask = LVIS_STATEIMAGEMASK
  lvi.iIndent = iIndent

  Call SendMessage(hwndLV, LVM_SETITEM, 0&, lvi)


End Function

' Returns the state and indent vaues of the specified listview item

Public Function getIndent(hwndLV As Long, iItem As Long, iIndent As Long) As LVItemStates
  Dim lvi As LVITEM
  
  lvi.mask = LVIF_STATE Or LVIF_INDENT
  lvi.iItem = iItem
  lvi.stateMask = LVIS_STATEIMAGEMASK
  
  If ListView_GetItem(hwndLV, lvi) Then
    iIndent = lvi.iIndent
    Listview_GetItemStateEx = STATEIMAGEMASKTOINDEX(lvi.state And LVIS_STATEIMAGEMASK)
  End If
  
End Function

'Public Sub setIndent(hwndLV As Long, itm As ListItem, nIndent As Long)
'If nIndent < 1 Then Exit Sub
' Dim lv As LVITEM
'
''if nIndent indicates that indentation
''is requested nItem is the item to indent (last added item)
'    With lv
'      .mask = LVIF_INDENT
'      .iItem = itm.Index - 1   '0-based
'      .iIndent = nIndent
'    End With
'
'    Call SendMessage(hwndLV, LVM_SETITEM, 0&, lv)
'End Sub
'
'
'Public Function getIndent(itm As ListItem) As Integer
' Dim lv As LVITEM
' lv.iItem = itm
' getIndent = lv.iIndent
'End Function



' =========================================================
' imagelist macros

' Returns the one-based index of the specifed state image mask, shifted
' left twelve bits. A common control utility macro.

' Prepares the index of a state image so that a tree view control or list
' view control can use the index to retrieve the state image for an item.

Public Function INDEXTOSTATEIMAGEMASK(iIndex As Long) As Long
' #define INDEXTOSTATEIMAGEMASK(i) ((i) << 12)
  INDEXTOSTATEIMAGEMASK = iIndex * (2 ^ 12)
End Function

' Returns the state image index from the one-based index state image mask.
' The inverse of INDEXTOSTATEIMAGEMASK.

' A user-defined function (not in Commctrl.h)

Public Function STATEIMAGEMASKTOINDEX(iState As Long) As Long
  STATEIMAGEMASKTOINDEX = iState / (2 ^ 12)
End Function

Public Function ListView_SetImageList(hWnd As Long, himl As Long, iImageList As Long) As Long
  ListView_SetImageList = SendMessage(hWnd, LVM_SETIMAGELIST, ByVal iImageList, ByVal himl)
End Function

