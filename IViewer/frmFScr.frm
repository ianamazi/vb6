VERSION 5.00
Begin VB.Form frmFScr 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4680
   ForeColor       =   &H00000000&
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Label lblName 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008080&
      Height          =   195
      Left            =   2355
      TabIndex        =   0
      Top             =   120
      Visible         =   0   'False
      Width           =   570
   End
   Begin VB.Image img 
      Height          =   1650
      Left            =   720
      Stretch         =   -1  'True
      Top             =   600
      Width           =   3315
   End
End
Attribute VB_Name = "frmFScr"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim bolCtrl As Boolean

Public Sub loadTiled(strPic As String)
  Dim X As Integer, Y As Integer ', D As Integer
  Dim PatternHeight As Integer, PatternWidth As Integer
  
  Me.ScaleMode = 3
  Me.Cls
  img.Visible = False
  img.Picture = LoadPicture(strPic)
  img.Stretch = False
  PatternHeight = img.Height       'hard-coded value
  PatternWidth = img.Width        'hard-coded value
  Me.Picture = LoadPicture(strPic)
  
  For X = 0 To Me.Width Step PatternWidth
    For Y = 0 To Me.Height Step PatternHeight
      BitBlt Me.hdc, X, Y, PatternWidth, PatternHeight, Me.hdc, 0, 0, &HCC0020  '=SRCCOPY)
    Next Y
  Next X
  img.Stretch = True
  Me.ScaleMode = 1
  'stbr.Panels(1) = ""
  'Exit Sub
End Sub

Public Sub loadImg(strPic As String)
Dim r As Single
'make img invisible to relad & resize
img.Visible = False

If fso.FileExists(strPic) = False Then
  strPic = "noimg.gif"
  If fso.FileExists("noimg.gif") = False Then Exit Sub
End If

Set img.Picture = LoadPicture(strPic)

r = img.Picture.Height / img.Picture.Width

If r > Me.ScaleHeight / Me.ScaleWidth Then
  img.Height = Me.ScaleHeight
  img.Width = img.Height / r
  'zo = img.Width / img.Picture.Width
Else
  img.Width = Me.ScaleWidth
  img.Height = img.Width * r
End If

'to center image
img.Left = 0 + (Me.ScaleWidth - img.Width) / 2
img.Top = 0 + (Me.ScaleHeight - img.Height) / 2

'now set images vis to tru
img.Visible = True
End Sub



Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF11 Or KeyCode = vbKeyEscape Then Vdplayer.mnuFullScreen_Click
If KeyCode = vbKeyControl Then bolCtrl = True
'If KeyCode = vbKeyU And bolCtrl = True Then Vdplayer.mnuAuto_Click
If (KeyCode = vbKeyT Or KeyCode = vbKeyN Or KeyCode = vbKeyRight) And bolCtrl = True Then Vdplayer.CrlNext
If KeyCode = vbKeyF4 Then PopupMenu Vdplayer.mnuImage
End Sub

Private Sub Form_KeyUp(KeyCode As Integer, Shift As Integer)
  If KeyCode = vbKeyControl Then bolCtrl = False
End Sub

Private Sub Form_Load()
  Me.BackColor = getINI("Appearance", "Full Screen Background Color", 14737632)
  
End Sub

Private Sub Form_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Button = 2 Then PopupMenu Vdplayer.mnuImage
End Sub

Private Sub Form_Resize()
lblName.Left = (Me.ScaleWidth - lblName.Width) / 2
End Sub

Private Sub img_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
If Button = 2 Then PopupMenu Vdplayer.mnuImage
End Sub
