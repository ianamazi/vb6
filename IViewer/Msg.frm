VERSION 5.00
Begin VB.Form Msg 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   1410
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6375
   BeginProperty Font 
      Name            =   "Verdana"
      Size            =   9.75
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1410
   ScaleWidth      =   6375
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton com 
      Caption         =   "&Yes"
      Height          =   330
      Index           =   2
      Left            =   1971
      TabIndex        =   1
      Tag             =   "Image"
      Top             =   840
      Width           =   1100
   End
   Begin VB.CommandButton com 
      Cancel          =   -1  'True
      Caption         =   "N&o to all"
      Default         =   -1  'True
      Height          =   330
      Index           =   3
      Left            =   3336
      TabIndex        =   2
      Tag             =   "Media"
      Top             =   840
      Width           =   1100
   End
   Begin VB.CommandButton com 
      Caption         =   "Ye&s to all"
      Height          =   330
      Index           =   1
      Left            =   609
      TabIndex        =   0
      Tag             =   "Html"
      Top             =   840
      Width           =   1100
   End
   Begin VB.CommandButton com 
      Caption         =   "&No"
      Height          =   330
      Index           =   4
      Left            =   4701
      TabIndex        =   3
      Tag             =   "Text"
      Top             =   840
      Width           =   1100
   End
   Begin VB.Label lbl 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      Height          =   240
      Left            =   420
      TabIndex        =   4
      Top             =   315
      Width           =   5535
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "Msg"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Message As String, iMsg As Integer

Private Sub com_Click(Index As Integer)
iMsg = Index
  Unload Me
End Sub

Private Sub Form_Activate()
Me.Caption = apProd
If Mid(Me.Tag, 1, 1) <> "/" Then
  lbl.Caption = "'" & Message & "' not found. Do you want to load into list anyway?"
Else
  lbl.Caption = Mid(Me.Tag, 2, Len(Message) - 1)
  Dim i As Byte
  For i = 1 To 4: com(i).Caption = com(i).Tag: Next i
End If

iMsg = 0
End Sub

Private Sub lbl_Change()
Me.Height = lbl.Top + lbl.Height + 1340
Dim i As Byte
For i = 1 To 4: com(i).Top = Me.Height - 975: Next i
Me.Top = (Screen.Height - Me.Height) / 2
End Sub

