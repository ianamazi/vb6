VERSION 5.00
Object = "{EAB22AC0-30C1-11CF-A7EB-0000C05BAE0B}#1.1#0"; "SHDOCVW.DLL"
Begin VB.Form frmTips 
   BorderStyle     =   5  'Sizable ToolWindow
   Caption         =   "IViewer Tips"
   ClientHeight    =   4665
   ClientLeft      =   60
   ClientTop       =   300
   ClientWidth     =   7380
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4665
   ScaleWidth      =   7380
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkShow 
      Caption         =   "Show Tips on Startup"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   5265
      TabIndex        =   8
      Top             =   120
      Width           =   1830
   End
   Begin VB.PictureBox pic 
      BorderStyle     =   0  'None
      Height          =   375
      Left            =   1320
      ScaleHeight     =   375
      ScaleWidth      =   4815
      TabIndex        =   5
      TabStop         =   0   'False
      Top             =   4080
      Width           =   4815
      Begin VB.CommandButton cmdNext 
         Caption         =   "&Random Tip"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3585
         TabIndex        =   4
         ToolTipText     =   "Shows Tip at random"
         Top             =   0
         Width           =   1215
      End
      Begin VB.CommandButton cmdOK 
         Cancel          =   -1  'True
         Caption         =   "&Close"
         Default         =   -1  'True
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   0
         TabIndex        =   0
         ToolTipText     =   "Closes this window"
         Top             =   0
         Width           =   1215
      End
      Begin VB.TextBox txtTip 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2100
         TabIndex        =   2
         Top             =   45
         Width           =   375
      End
      Begin VB.CommandButton cmdBef 
         Caption         =   "3"
         BeginProperty Font 
            Name            =   "Webdings"
            Size            =   8.25
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   1335
         TabIndex        =   1
         ToolTipText     =   "Previous Tip"
         Top             =   0
         Width           =   255
      End
      Begin VB.CommandButton cmdAft 
         Caption         =   "4"
         BeginProperty Font 
            Name            =   "Webdings"
            Size            =   8.25
            Charset         =   2
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3210
         TabIndex        =   3
         ToolTipText     =   "Next Tip"
         Top             =   0
         Width           =   255
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "Tip:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   1710
         TabIndex        =   7
         Top             =   90
         Width           =   270
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   "Of"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   195
         Left            =   2595
         TabIndex        =   6
         Top             =   90
         Width           =   495
      End
   End
   Begin SHDocVwCtl.WebBrowser browser 
      Height          =   2835
      Left            =   120
      TabIndex        =   10
      Top             =   480
      Width           =   3135
      ExtentX         =   5530
      ExtentY         =   5001
      ViewMode        =   0
      Offline         =   0
      Silent          =   0
      RegisterAsBrowser=   0
      RegisterAsDropTarget=   0
      AutoArrange     =   0   'False
      NoClientEdge    =   0   'False
      AlignLeft       =   0   'False
      NoWebView       =   0   'False
      HideFileNames   =   0   'False
      SingleClick     =   0   'False
      SingleSelection =   0   'False
      NoFolders       =   0   'False
      Transparent     =   0   'False
      ViewID          =   "{0057D0E0-3573-11CF-AE69-08002B2E1262}"
      Location        =   ""
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "TIPS for IViewer"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Left            =   135
      TabIndex        =   9
      Top             =   120
      Width           =   1575
   End
End
Attribute VB_Name = "frmTips"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim ts As TextStream
Dim Tips(50) As String, cnt As Integer  'margin must exceed largely

Private Sub browser_DocumentComplete(ByVal pDisp As Object, URL As Variant)
  txtTip_Change
End Sub

Private Sub chkShow_Click()
If chkShow.Value = 1 Then putINI "Application Start", "Tips", True Else putINI "Application Start", "Tips", False
End Sub

Private Sub cmdAft_Click()
If Val(txtTip) > cnt - 1 Then txtTip = 1 Else txtTip = Val(txtTip) + 1
End Sub

Private Sub cmdBef_Click()
If Val(txtTip) = 1 Then txtTip = cnt Else txtTip = Val(txtTip) - 1
End Sub

Private Sub cmdNext_Click()
  Randomize
  txtTip = Int((cnt * Rnd) + 1)
End Sub

Sub DoTip(CurrentTip As Integer)

If CurrentTip > cnt Then txtTip = Int(Rnd * cnt) + 1: Exit Sub
  'Dim htfil As TextStream
  'Set htfil = fso.CreateTextFile(apPath & "tip.htm", True)
  Dim strTip As String
  
  strTip = "<html><body>" & vbCrLf
  strTip = strTip & "<basefont face =" & Chr(34) & "Verdana" & Chr(34) & " size = -2>" & vbCrLf
  strTip = strTip & Tips(CurrentTip) & vbCrLf
  strTip = strTip & "</body></html>"
  'htfil.WriteLine "<html><body>"
  'htfil.WriteLine "<basefont face =" & Chr(34) & "Verdana" & Chr(34) & " size = -2>"
  'htfil.Write Tips(CurrentTip)
  'htfil.WriteLine "</body></html>"

  'htfil.Close
 
  'browser.Navigate (apPath & "tip.htm")
  browser.Document.body.innerhtml = strTip
End Sub

Private Sub cmdOK_Click()
Unload Me
End Sub

Private Sub Form_Load()
Dim lin As String, strTmp As String

browser.Navigate "about:blank"


Set ts = fso.OpenTextFile(apPath & "tips.txt")

cnt = 1

Do Until ts.AtEndOfStream
  
  'look for <tip> (skip blanklines in between tips)
  Do
    lin = ts.ReadLine
    If ts.AtEndOfStream Then GoTo Edn
  Loop Until lin = "<tip>"
  
  lin = ts.ReadLine
  
  Do Until lin = "</tip>"
    strTmp = strTmp & lin & vbCrLf
    lin = ts.ReadLine
  Loop
  
  Tips(cnt) = strTmp
  strTmp = ""
  cnt = cnt + 1
Loop
ts.Close
cnt = cnt - 1
Edn:
  
  If cnt = 0 Then MsgBox "No tips found": Unload Me

Me.Width = getINI("Preferences", "frmTips.Width", Me.Width)
Me.Height = getINI("Preferences", "frmTips.Height", Me.Height)

'Me.Tag = "ldg"
If True = getINI("Preferences", "frmTips.1stLoad", "True") Then _
  Label1 = "Tips' Tip: You can resize this form": _
  Label1.Font.Bold = False: putINI "Preferences", "frmTips.1stLoad", False

cmdNext_Click

Label3.Caption = "Of " & cnt

If True = getINI("Application Start", "Tips", True) Then chkShow.Value = 1
End Sub

Private Sub Form_Resize()
If Me.Width < pic.Width Then Me.Width = pic.Width + 500
If Me.Height < 3500 Then Me.Height = 3500

browser.Height = Me.ScaleHeight - pic.Height - 2 * browser.Left - browser.Top
browser.Width = Me.ScaleWidth - 2 * browser.Left

pic.Top = browser.Top + browser.Height + browser.Left
pic.Left = (Me.ScaleWidth - pic.Width) / 2

chkShow.Left = browser.Left + browser.Width - chkShow.Width

'If Me.Tag = "ldg" Then tmr.Enabled = True: Me.Tag = ""
'Me.Move (Screen.Width - Me.Width) / 2, (Screen.Height - Me.Height) / 2
End Sub

Private Sub Form_Unload(Cancel As Integer)
putINI "Preferences", "frmTips.Width", Me.Width
putINI "Preferences", "frmTips.Height", Me.Height
End Sub

Private Sub txtTip_Change()
If Me.Visible Then DoTip Val(txtTip)
End Sub

Private Sub txtTip_KeyDown(KeyCode As Integer, Shift As Integer)
If Len(txtTip) = 2 And IsNumeric(Asc(KeyCode)) = True Then txtTip.SelStart = 0: txtTip.SelLength = Len(txtTip)
End Sub
