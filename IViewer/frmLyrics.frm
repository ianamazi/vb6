VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form frmLyrics 
   Caption         =   "Form1"
   ClientHeight    =   6060
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7155
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   6060
   ScaleWidth      =   7155
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Interval        =   500
      Left            =   5760
      Top             =   1320
   End
   Begin MSComctlLib.Slider sldr 
      Height          =   255
      Left            =   3840
      TabIndex        =   5
      Top             =   120
      Width           =   3015
      _ExtentX        =   5318
      _ExtentY        =   450
      _Version        =   393216
      LargeChange     =   6
      Min             =   6
      Max             =   48
      SelectRange     =   -1  'True
      SelStart        =   6
      Value           =   6
   End
   Begin VB.TextBox txtFont 
      Height          =   405
      Left            =   600
      TabIndex        =   2
      Text            =   "Webdings"
      Top             =   30
      Width           =   2655
   End
   Begin MSComctlLib.ListView lvwFont 
      Height          =   5295
      Left            =   600
      TabIndex        =   3
      Top             =   435
      Visible         =   0   'False
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   9340
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   0   'False
      HideColumnHeaders=   -1  'True
      FullRowSelect   =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      NumItems        =   1
      BeginProperty ColumnHeader(1) {BDD1F052-858B-11D1-B16A-00C0F0283628} 
         Object.Width           =   4145
      EndProperty
   End
   Begin RichTextLib.RichTextBox rtb 
      Height          =   5655
      Left            =   30
      TabIndex        =   0
      Top             =   480
      Width           =   7095
      _ExtentX        =   12515
      _ExtentY        =   9975
      _Version        =   393217
      Enabled         =   -1  'True
      HideSelection   =   0   'False
      ScrollBars      =   2
      OLEDragMode     =   0
      OLEDropMode     =   1
      TextRTF         =   $"frmLyrics.frx":0000
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label lb 
      AutoSize        =   -1  'True
      Caption         =   "&Size"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   1
      Left            =   3360
      TabIndex        =   4
      Top             =   105
      Width           =   390
   End
   Begin VB.Label lb 
      AutoSize        =   -1  'True
      Caption         =   "&Font"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   240
      Index           =   0
      Left            =   120
      TabIndex        =   1
      Top             =   105
      Width           =   420
   End
End
Attribute VB_Name = "frmLyrics"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Type POINTAPI
    x As Long
    y As Long
End Type

Private Declare Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Private Declare Function SetCursorPos Lib "user32" (ByVal x As Long, ByVal y As Long) As Long

Dim lPt As POINTAPI, thX As Long, thY1 As Long, thY2 As Long


Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyEscape Then Me.Hide: Timer1.Enabled = True
If Shift = 2 Then 'control
  If KeyCode = vbKeyAdd And sldr.Value < 47 Then sldr.Value = sldr.Value + 2
  If KeyCode = vbKeySubtract And sldr.Value > 7 Then sldr.Value = sldr.Value - 2
End If
End Sub

Private Sub Form_Load()
thX = Screen.Width / Screen.TwipsPerPixelX - 2
thY1 = 60
thY2 = Screen.Height / Screen.TwipsPerPixelY - 100

Dim i As Long, sKey() As String, iCnt As Long
For i = 0 To Screen.FontCount - 1
  lvwFont.ListItems.Add , Screen.Fonts(i), Screen.Fonts(i)
Next
lvwFont.Sorted = True
'Set itmFont = lvwFont.ListItems(1)
lvwFont.Sorted = False

rtb.FileName = "D:\mp3\_lyrics\_singles\hindi - pehla nasha.txt"
End Sub

Private Sub Form_Resize()
If Me.WindowState = vbMinimized Then Exit Sub
If Me.Width < 5000 Then Me.Width = 5000: Exit Sub

sldr.Width = Me.ScaleWidth - sldr.Left
rtb.Height = Me.ScaleHeight - rtb.Top - 30
rtb.Width = Me.ScaleWidth - 60
End Sub

Private Sub sldr_Change()
rtb.Font.Size = sldr.Value
End Sub

Private Sub Timer1_Timer()
GetCursorPos lPt
If lPt.x > thX And lPt.y > thY1 And lPt.y < thY2 Then Me.Show: Timer1.Enabled = False
End Sub

Private Sub txtFont_GotFocus()
lvwFont.Visible = True
End Sub

Private Sub txtFont_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF4 Or KeyCode = vbKeyDown Then
  lvwFont.Visible = True
  lvwFont.SetFocus
  lvwFont.SelectedItem.EnsureVisible
  picLine.Visible = True
  If KeyCode = vbKeyDown Then SendKeys txtFont
End If
If KeyCode = vbKeyEscape Then lvwFont_LostFocus
End Sub

Private Sub txtFont_LostFocus()
txtFont = lvwFont.SelectedItem.Text
If Me.ActiveControl.Name <> "lvwFont" Then lvwFont_LostFocus
End Sub

Private Sub lvwFont_DblClick()
'FontFloatToTop ""
lvwFont_LostFocus
End Sub

Private Sub lvwFont_ItemClick(ByVal Item As MSComctlLib.ListItem)
Item.EnsureVisible
rtb.Font.Name = Item.Text
txtFont = Item.Text
'lvwFont_MouseDown 0, 0, 0, 0
End Sub

Private Sub lvwFont_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyReturn Then lvwFont_DblClick
If KeyCode = vbKeyEscape Then lvwFont_LostFocus
End Sub

Private Sub lvwFont_LostFocus()
lvwFont.Visible = False
End Sub
